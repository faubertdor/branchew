<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA.
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'minLength' => 1,
				'maxLength' => 10,
			
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if(Yii::app()->user->isGuest)
		{
			$model=new LoginForm;

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
		
				if($model->validate() && $model->login())
					$this->redirect(ContactMembres::model()->getIndexUrl());
				else 
				    $this->redirect($this->createUrl('site/login',array()));
			}
			
			//Registration begin Step 1 for website index
			$session=new CHttpSession();
			$session->open();
		
			$_SESSION['session']=$session;
			$_SESSION['InscriptionForm']=new InscriptionForm();
	
	
			if(isset($_POST['InscriptionForm']))
			{
				$_SESSION['InscriptionForm']->attributes=$_POST['InscriptionForm'];	
			
				if($_SESSION['InscriptionForm']->validate())
					$this->redirect(array('site/inscription'));
			}

			$this->render('index',array(	
											'model'=>$_SESSION['InscriptionForm'],
										));
		}
		else 
			$this->redirect(array('profil/default/index'));
	
	}
	
	
	
	/**
	 * Registration  Step 1 -> Then Registration Step 2
	 * 
	 */
	 public function actionRegister(){
	 
	 	if(Yii::app()->user->isGuest)
	 	{
			$session=new CHttpSession();
			$session->open();
		
			$_SESSION['session']=$session;
			$_SESSION['InscriptionForm']=new InscriptionForm();
	
	
		
		
			if(isset($_POST['InscriptionForm'])){
			
				$_SESSION['InscriptionForm']->attributes=$_POST['InscriptionForm'];	
			
			
				if($_SESSION['InscriptionForm']->validate())
					$this->redirect(array('site/inscription'));
			
			}
			
			
			$this->render('register',array(	
											'model'=>$_SESSION['InscriptionForm'],
										  ));
		}
		else 
			$this->redirect(array('profil/default/index'));									
	 }
	 
	 
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Merci de nous contacter. Nous répondrons à vos messages dès que possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
		
			if($model->validate() && $model->login())
				$this->redirect(ContactMembres::model()->getIndexUrl());
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	
	//Facebook login
	public function actionFblogin()
	{
		
		// Create our Application instance (replace this with your appId and secret).
		$facebook = new Facebook(array(
  		'appId'  => '387237101329550',
  		'secret' => 'febca24136aab2d4c6cd033a5aac5b3c',
		));

		// Get User ID
		$user = $facebook->getUser();

		// We may or may not have this data based on whether the user is logged in.
		//
		// If we have a $user id here, it means we know the user is logged into
		// Facebook, but we don't know if the access token is valid. An access
		// token is invalid if the user logged out of Facebook.

		if ($user) 
		{
			try
			{
					$user_info	= $facebook->api('/'.$user);
					
					
			}
			catch(FacebookApiException $e)
			{
					error_log($e);
					$user=NULL;
			}	
		}

		// Login or logout url will be needed depending on current user state.
		if ($user) 
		{
 		 	$logoutUrl = $facebook->getLogoutUrl(array(
														'next'	=> 'http://www.branchew.com/site/login.php', // URL to which to redirect the user after logging out
												));
												
			$this->render('fblogin',array('user_info'=>$user_info,'logoutUrl'=>$logoutUrl));
		} 
		else 
		{
  			$loginUrl = $facebook->getLoginUrl(array(
														'scope'		=> 'email, user_birthday, user_location, user_hometown', // Permissions to request from the user
														'redirect_uri'	=> 'http://www.branchew.com/profil/default/index.php', // URL to redirect the user to once the login/authorization process is complete.
												));
		}
	

   
	}
	
	//Facebook registration 
	
	public function Fbregistration($user_info)
	{
		//If the user is authenticated with Facebook with need to check if he or her already has account on Branchew with this email
		$membres=Membres::model()->findByAttributes(array('email'=>$user_info['email']));
		
		if($membres===null)
		{
			$membre=new Membres();
		
			$membre->password=$_SESSION['InscriptionForm']->password;
			
			
			//Une image par defaut pour tout nouveau membre
			$dir=Yii::getPathOfAlias('application.modules.profil.assets.images');
			$membre->chemin_avatar=Yii::app()->assetManager->publish($dir.'/0');
				
			
				
			
			if(isset($_POST['InscriptionFormStep2']))
			{
	
				//Recuperer les informations du formulaire
				$step2->attributes=$_POST['InscriptionFormStep2'];
				
				if($step2->validate())
				{		
					$membre->pays=$_POST['InscriptionFormStep2']['pays'];
					$membre->ville=$_POST['InscriptionFormStep2']['ville'];
					
				if($membre->save())
				{	
					$email=$_SESSION['InscriptionForm']->email;
					$password=$_SESSION['InscriptionForm']->password;
					
					//Detruire la session
					$_SESSION['session']->destroy();
				
						
					$mail= new YiiMailMessage;
					$mail->view='inscription';
					$mail->setBody(array('membre'=>$membre),'text/html');
					$mail->addTo($membre->email);
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Bienvenue sur branchew';
					Yii::app()->mail->send($mail);
					
					//Auto Login
					$identity=new UserIdentity($email,$password);
					$identity->authenticate();
					Yii::app()->user->login($identity,60*60);
					
					if(!Yii::app()->user->isGuest)
						$this->redirect(array('profil/default/invite'));
					else
						$this->redirect(array('site/followUp'));
					
				  }
						
						
			   }
		    }
		}
		else
		{
			
			
		}
		
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	//Inscription d'un membre
	public function actionInscription(){
		
		if(Yii::app()->user->isGuest){
						 
			$step2= new InscriptionFormStep2();
			$membre=new Membres();
			
			
			if(isset($_SESSION['InscriptionForm']))
			{
				$membre->nom=$_SESSION['InscriptionForm']->nom;
				$membre->prenom=$_SESSION['InscriptionForm']->prenom;
				$membre->email=$_SESSION['InscriptionForm']->email;
				$membre->password=$_SESSION['InscriptionForm']->password;
			}
			
			//Une image par defaut pour tout nouveau membre
			$dir=Yii::getPathOfAlias('application.modules.profil.assets.images');
			$membre->chemin_avatar=Yii::app()->assetManager->publish($dir.'/0');
				
			
				
			
			if(isset($_POST['InscriptionFormStep2']))
			{
	
				//Recuperer les informations du formulaire
				$step2->attributes=$_POST['InscriptionFormStep2'];
				
				if($step2->validate())
				{		
					$membre->pays=$_POST['InscriptionFormStep2']['pays'];
					$membre->ville=$_POST['InscriptionFormStep2']['ville'];
					
				if($membre->save())
				{	
					$email=$_SESSION['InscriptionForm']->email;
					$password=$_SESSION['InscriptionForm']->password;
					
					//Detruire la session
					$_SESSION['session']->destroy();
				
						
					$mail= new YiiMailMessage;
					$mail->view='inscription';
					$mail->setBody(array('membre'=>$membre),'text/html');
					$mail->addTo($membre->email);
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Bienvenue sur branchew';
					Yii::app()->mail->send($mail);
					
					//Auto Login
					$identity=new UserIdentity($email,$password);
					$identity->authenticate();
					Yii::app()->user->login($identity,60*60);
					
					if(!Yii::app()->user->isGuest)
						$this->redirect(array('profil/default/invite'));
					else
						$this->redirect(array('site/followUp'));
					
				  }
						
						
			   }
		    }
			$this->render('inscription',array('model'=>$step2));
		}
		else 
			$this->redirect(array('profil/default/update'));

	}
	
	//Activer le profil de l'utilisateur
	public function actionActivateProfile($id,$email)
	{	
	
		if(isset($id) and isset($email))
		{
			$membre=Membres::model()->findByAttributes(array('email'=>$email));
		}
		else
			throw new CHttpException(400,'Requête invalide.');
			
		 
		if($membre->password!=$id)
			throw new CHttpException(400,'Requête invalide.');
			
		$membre->actif='true';
		
		if($membre->save())
			$this->redirect(array('site/login'));
	}
	
	//Pour ajouter les villes dans le formulaire
	public function actionRequestCities()
	{
		
	
		$data=Villes::model()->findAll('b_pays_id=:parent_id',
            							array(':parent_id'=>(int)$_POST['InscriptionFormStep2']['pays']));

        $data=CHtml::listData($data,'id','ville');
		
		
		foreach($data as $key=>$value)
			 echo CHtml::tag('option', array('value'=>$key), CHtml::encode($value), true);
		
	}
	
	//Envoyer un email a l'utilisateur avec un lien pour reinitialiser son mot de passe
	public function actionRecoverPassword()
	{	
		$model=new RecoverPassword();

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		
		if(isset($_POST['RecoverPassword']))
	  	{
	  		$model->attributes=$_POST['RecoverPassword'];
	  		
	  		if($model->validate())
	  		{
				$membre=Membres::model()->findByAttributes(array('email'=>$model->email));
		
				if($membre===NULL)
					throw  new CHttpException(404,'Cette adresse email n\'existe pas!');
				else 
				{
					
					
					$mail= new YiiMailMessage;
					$mail->view='resetPassword';
					$mail->setBody(array('membre'=>$membre),'text/html');
					$mail->addTo($membre->email);
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Réinitialiser votre mot de passe Branchew';
					
					Yii::app()->mail->send($mail);
					
					Yii::app()->user->setFlash('password','Consulter votre boite de réception et cliquez sur le lien qu’on vous a envoyé pour réinitialiser votre mot de passe.');
					$this->refresh();
					$this->redirect(array('site/recoverPassword'),array());
				}
	  		}
	  		$this->render('recoverPassword',array('model'=>$model));
		}
		else
			$this->render('recoverPassword',array('model'=>$model));
		
	}
	
	//Reinitialiser le mot de passe
	public function actionResetPassword($token,$email)
	{	
	
		if(isset($token) and isset($email))
		{
			$membre=Membres::model()->findByAttributes(array('email'=>$email));
		}
		else
			throw new CHttpException(400,'Requête invalide.');
			
		 
		if($membre->password!=$token)
			throw new CHttpException(400,'Requête invalide.');
		 
			$model=new ResetPassword();
			$membreReset=Membres::model()->findByPk($membre->id);
			
		 if(isset($_POST['ResetPassword']))
		    {
				$model->attributes=$_POST['ResetPassword'];
					
			    if($model->validate())
				{
						
						$membreReset->salt=uniqid('',true);
						$membreReset->password=md5($membreReset->salt.$model->password); //hash password with md5 algorithm
				
						if($membreReset->save())
						{
							if(Yii::app()->user->isGuest)
							$this->redirect(array('site/login'));
							else 
							$this->redirect(Membres::model()->getUpdateUrl());
						}
				}
				 else 
				 	$this->render('resetPassword',array('model'=>$model));
			 }
			 else 
				$this->render('resetPassword',array('model'=>$model));
			
		
	}
	
	//followUp
	
	public function actionFollowUp()
	{
		$this->render('followUp',array());
	}
	
	//policy
	public function actionPolicy()
	{
		$this->render('policy',array());
	}
	
	//About us
	public function actionAbout()
	{
		$this->render('about',array());
	}
	
	//Stat function
	public function actionStat(){
		
		$count=Membres::model()->count('id');
		
					$membre=Membres::model()->getViewIdInfo(4);
					
					$mail= new YiiMailMessage;
					$mail->view='statMail';
					$mail->setBody(array('count'=>$count,'membre'=>$membre),'text/html');
					$mail->addTo('faubertdor@gmail.com');
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Statistique de branchew';
					Yii::app()->mail->send($mail);
					
		
		$this->render('stat',array('count'=>$count));
		
	}
	
	
	
	
}
