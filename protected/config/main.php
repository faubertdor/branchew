<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Branchew',
	'language'=>'fr',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.profil.models.*',
		'application.modules.groupes.models.*',
		'application.modules.emploi.models.*',
		'ext.yii-mail.YiiMailMessage',
		'ext.src.*',
	),

	'modules'=>array(
	
		'profil',
		'groupes',
		'emploi'
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=>false,
			'caseSensitive'=>true,
		),
		
	/**	'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),

	**/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host='.getenv('HOST').';dbname='.getenv('DATABASE'),
			'emulatePrepare' => true,
			'username' => getenv('DB_USER'),
			'password' => getenv('DB_PASSWORD'),
			'charset' => 'utf8',
			'tablePrefix'=>'b_',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        	),
        
        //Curl component for Janrain Sign in and share
        'CURL' =>array(
			'class' => 'application.extensions.curl.Curl',
     	//you can setup timeout,http_login,proxy,proxylogin,cookie, and setOPTIONS
 		),

		//Mail component
		'mail' => array(
 			'class' => 'ext.yii-mail.YiiMail',
 			'transportType' => 'php',
 			'viewPath' => 'application.views.mail',
 			'logging' => true,
 			'dryRun' => false
 		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'faubertdor@gmail.com',
	),
);
