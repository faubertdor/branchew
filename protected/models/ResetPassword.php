<?php
class ResetPassword extends CFormModel{
	
	
	public $password;
	public $password_repeat;
	public $id;
	
	public function rules(){
		
		return array(
						array('password, password_repeat','required'),
						array('password','compare')
						
								
				);
	}
	
 	public function attributeLabels()
	{
		return array(
	
					'password' => 'Mot de passe   ',
					'password_repeat'=>'Confirmer votre mot de passe '
			);
			
	}
	//Url
	
	public static function getRecoverPasswordUrl(){
		
		return Yii::app()->createUrl('/site/recoverPassword',array());
	}
	
	
	
}