<?php
class InscriptionFormStep2 extends CFormModel{
	
	public $pays;	
	public $ville;
	public $verifyCode;
	
	//Regle de validation des donnees
	public function rules(){
		
		return array(
						array('pays, ville','required'),
						array('pays,ville', 'numerical', 'integerOnly'=>true),				
						array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}
	
	public function uniqueEmail($attribute,$params){
		
		if(Membres::model()->findByAttributes(array('email'=>$this->email))!=array())
		$this->addError('uniqueEmail','Cette adresse email est deja prise');
		
		
	}
	//Les labels
	
	public function attributeLabels()
	{
		return array(

			'domaine_activite' => 'Domaine de compétence',
			'objectif' => 'Objectif et/ou spécialités',
			'pays' => 'Pays',
			'ville' => 'Ville',
			'verifyCode'=>'Code de sécurité'
		);
	}
	
}