<?php

/**
 * This is the model class for table "{{villes}}".
 *
 * The followings are the available columns in table '{{villes}}':
 * @property integer $id
 * @property integer $b_pays_id
 * @property string $ville
 */
class Villes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Villes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{villes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(

			'ville' => 'Ville',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		
	}
	
	//Recuperer un element par son code entier
	
	public static function itemByInteger($id){
		
		if($id!=NULL)
		{
			$villeArray=Yii::app()->db->createCommand()
				->select('id,ville')
				->from('b_villes')
				->where('id='.$id)
				->queryRow();
		
		
		
		if($villeArray===false)
			return false;
		else
		{	
				return $villeArray['ville'];
		}
	}
 }
 
 
  // Recuperer toutes les villes d'un pays $id
  
 	public static function citiesByCountry($id){
 	
 	if($id!=NULL)
 	{
 		
 		$citiesArray=Yii::app()->db->createCommand()
 			->select('b_villes.id villesId,b_pays_id,ville')
 			->from('b_villes')
 			->where('b_pays_id='.$id)
 			->queryAll();
 		
 		return $citiesArray;
 		
 	}
 
 }


}