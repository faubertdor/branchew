<?php

/**
 * This is the model class for table "{{super_membres}}".
 *
 * The followings are the available columns in table '{{super_membres}}':
 * @property string $id
 * @property string $b_membres_id
 */
class SuperMembres extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SuperMembres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{super_membres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
						array('b_membres_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, b_membres_id', 'safe', 'on'=>'search'),
		);
	}

	public  function isSuper($id)
	{
		$super=Yii::app()->db->createCommand()
		->select('*')
		->from('b_super_membres')
		->where('b_membres_id='.$id)
		->queryRow();
		
		if($super==false)
		return false;
		else
		return true;
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}