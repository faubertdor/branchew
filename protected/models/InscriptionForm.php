<?php
class InscriptionForm extends CFormModel{
	
	public $nom;
	public $prenom;
	public $email;
	public $password;
	public $password_repeat;
	
	//Regle de validation des donnees
	public function rules(){
		
		return array(
						array('nom,prenom,email, password,password_repeat','required'),
						array('nom,prenom','length','max'=> 40),
						array('email,password','length','max'=>40),
						array('email','uniqueEmail'),
						array('password','length','min'=>6),
						array('email','email'),
						array('password','compare')
						
		
		
		);
	}
	
	public function uniqueEmail($attribute,$params){
		
		if(Membres::model()->findByAttributes(array('email'=>$this->email))!=array())
		$this->addError('uniqueEmail','Cette adresse email est deja prise');
		
		
	}
	//Les labels
	
	public function attributeLabels()
	{
		return array(

			'nom' => 'Nom',
			'prenom' => 'Pr&eacute;nom',
			'email' => 'Email',
			'password' => 'Nouveau mot de passe',
			'password_repeat'=>'Confirmer votre mot de passe'
		);
	}
	
}