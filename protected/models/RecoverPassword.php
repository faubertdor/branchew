<?php
class RecoverPassword extends CFormModel{
	
	public $email;

	
	public function rules(){
		
		return array(
						array('email','required'),
						array('email','email')			
				);
	}
	
 	public function attributeLabels()
	{
		return array(
	
					'email' => 'Email   ',
			);
			
	}
	//Url
	
	public static function getRecoverPasswordUrl(){
		
		return Yii::app()->createUrl('/site/recoverPassword',array());
	}
	
	
	
}