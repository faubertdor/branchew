<?php
class Helper{
	
	//Assests url to publish pitures.
	
	public static function getImageAssetsUrl(){
		
		return $assetsUrl=Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.profil.assets.images')).'/';
	}
	
	//Pour additionner un nombre de jours a la date d'aujourd'hui
	
	public static function addDaysToNow($nbreJours){
		
		return date('Y-m-d',mktime(0,0,0,date('m'),date('d')+ $nbreJours,date('Y')));
	}
	
	//Gestion de date pour formulaire
	public static function date($value){
		
			if($value==='annee'){
				
				 $annee=array();
				 $year=date('Y');
				 
				for($i=$year;$i>=1905;$i--)
			  		$annee[$i]=$i;
			  		
				return $annee;
			}
			
			if($value==='mois'){
				
				$mois=array();
				
				for($i=12;$i>=1;$i--){

			  	if($i<10)
			  		$mois['0'.$i]=Helper::monthsInText('0'.$i);
			  	else 
			  	{	
			  		if($i===10)
			  			$mois['10']=Helper::monthsInText('10');
			  		else 
			  			if($i===11)
			  				$mois['11']=Helper::monthsInText('11');
			  			else 
			  				if($i===12)
			  					$mois['12']=Helper::monthsInText('12');
			  	}
			  }
			  
			  return $mois;
				
			}
			
			if($value==='jour'){
			  
			  $jour=array();

			   for($i=31;$i>=1;$i--)
			   {
			   	if($i<10)
			  		$jour['0'.$i]='0'.$i;
			  	else 
			  		$jour[$i]=$i;
			
			   }
			   
			   return $jour;
			}
		

	}
	
	//Affichage des mois 
	public static function monthsInText($month){
		
		if($month==='01')
			return 'janvier';
		else
			if($month==='02')
				return 'février';
			else
				if($month==='03')
					return 'mars';
				else
					if($month==='04')
						return 'avril';
					else 
						if($month==='05')
							return 'mai';
						else 	
							if($month==='06')
								return 'juin';
							else
								if($month==='07')
									return 'juillet';
								else 	
									if($month==='08')
										return 'aout';
									else
										if($month==='09')
											return 'septembre';
										else
											if($month==='10')
												return 'octobre';
											else 
												if($month==='11')
													return 'novembre';
												else
													if($month==='12')
														return 'décembre';
					
			
					
	}
	
	//Affichage date pour Experiences et Educations
	public static function dateProfileFormat($date){
		
		$month=substr($date,-5,2);
		$month=Helper::monthsInText($month);
		$year =substr($date,0,4);
		
		return $month.'  '.$year;
	}
	
	//Afficher une date
	public static function dateViewFormat($date){
		
		$day=substr($date,8,2);
		$month=substr($date,-5,2);
		$month=Helper::monthsInText($month);
		$year =substr($date,0,4);
		
		return $day.' '.$month.' '.$year;
	}
	
	//Afficher groupe statut
	public static function statutViewFormat($statut){
		
		if($statut==='ferme')
			return 'Fermé';
		else 
			if($statut==='ouvert')
				return 'Ouvert';
			else 
				if($statut=='false')
					echo 'Brouillons';	
				else 
					if($statut=='true')
						echo 'Publi&eacute;';
	}
	
	//Afficher secteur entreprise
	public static function secteurViewFormat($secteur){
		
		if($secteur==='prive')
			return 'Privé';
		else
			if($secteur==='public')
			return 'Public';
			else 
				if($secteur==='ong')
					return 'ONG';
				
	
	}
	
	//Format Emploi type
	public static function formatTypeEmploi($type)
	{
		if($type=='temps_plein')
		return 'à Plein temps';
		else
		if($type=='temps_partiel')
		return 'à Temps partiel';
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
