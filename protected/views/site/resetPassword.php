<?php $this->pageTitle=Yii::app()->name.' | Changer mon mot de passe';?>
<div class="span-23">

	<h2>Réinitialiser votre mot de passe</h2>

<div class="wide form">

<?php 
		$form=$this->beginWidget('CActiveForm', array(
													 'id'=>'contact-form',
													 'enableClientValidation'=>true,
													 'clientOptions'=>array(
																			 'validateOnSubmit'=>true,
																			),
)); 
?>

<div class="span-12">
<?php echo $model->id;?>
<div class="span-3">
	<?php echo $form->labelEx($model,'password');?>
</div>
<div class="span-7">
	<?php echo $form->passwordField($model,'password',array('size'=>20,'maxlength'=>60))?>
</div>

<div class="span-3">
	<?php echo $form->labelEx($model,'password_repeat');?>
</div>
<div class="span-7">
	<?php echo $form->passwordField($model,'password_repeat',array('size'=>20,'maxlength'=>60))?>
</div>


<div class="span-7">
<div class="span-3">
		<?php echo CHtml::submitButton('Envoyer',array('class'=>'button'));?>
</div>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->


</div>
<?php
		 if(isset($salt))
		 	echo $salt;


?>

</div>
