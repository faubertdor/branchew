<?php $this->PageTitle='Branchew | Conditions d\'utilisation';?>
<div class="span-22 doc">
<div class="span-22" align="right"> <i>Le 12 Janvier 2013</i></div>
<div class="span-22" align="center"><h2>Conditions d'utilisation de Branchew<br></h2></div>

<div class="span-22, " align="justify">
Ces conditions régissent votre utilisation de notre site Web, en utilisant notre site vous acceptez ces "conditions d'utilisation" dans leur intégralité. Si vous êtes en désaccord avec ces conditions, vous ne devez pas utiliser notre site Web.
Notre site utilise des cookies, en utilisant notre site et en acceptant ces "conditions d'utilisation" vous consentez à notre utilisation de cookies en conformité avec les termes de notre politique de confidentialité qui sera élaborée dans les sections suivantes.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>1- Permis d'utilisation du site</h3>
Tout professionnel peu importe votre champs d’activités est éligible à s’inscrire sur Branchew. Lors de votre inscription tous les champs qui doivent contenir les informations devront être remplis correctement. Parmi les champs d’informations certains d’entre eux sont obligatoires et doivent refléter votre parcours professionnel.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>2- L'utilisation acceptable</h3>
<a href="http://www.branchew.com">http://www.branchew.com</a> est un site qui vise les professionnels haïtiens et les entreprises haïtiennes afin de faciliter les offres et les demandes d’emplois. Pour rendre possible cette initiative nous avons offert à tout un chacun la possibilité de créer leur propre compte sur le site.
La création d’un profil sur Branchew est une opération relativement facile, du moins si l’on a tous les éléments en tête. Dans une optique professionnelle, la création du profil demandera le même soin que la rédaction d’un CV. Tant que le profil ne sera pas rempli convenablement, les rappels et les incitations pour le parfaire seront constants.
Des milliers de CV à disposition, des profils mis à jour par les intéressés sont naturellement une aubaine à la fois pour les candidats et pour les professionnels du recrutement.
Lors de votre inscription sur Branchew vous vous offrez une autre opportunité sur le marché du travail.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>3- Les utilisateurs enregistrés</h3>
L'accès à certaines zones de notre site est réservé aux utilisateurs enregistrés.
Pour devenir un utilisateur enregistré, vous devez vous rendre sur la page d’accueil du site en tapant <a href="http://www.branchew.com">http://www.branchew.com</a>. Plusieurs catégories de secteurs pourront faire partie de ces utilisateurs. Il suffit de donner la 
garantie que toutes les informations que vous nous fournissez dans le cadre de la procédure d'inscription sont vraies, exactes, justes et complètes.
Nous allons fournir aux utilisateurs enregistrés un nom d'utilisateur et mot de passe afin de leur permettre d'accéder aux zones restreintes de notre site Web. Les utilisateurs enregistrés doivent tenir leurs identifiants et mots de passe confidentiels. 
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>4- Le CV des utilisateurs enregistrés</h3>
	En ce qui concerne les informations professionnelles ces dernières sont accessibles à la grande communauté d’internaute. N’importe qui peut visiter votre CV et vous contactez en cas de besoin, dans la mesure où 	cette personne décide d’utiliser notre site web qui possède un outil de recherche très puissant.
<br><br>
</div>

<div class="span-22 " align="justify">
<h3>5- Validation d’inscription</h3>
Après votre inscription sur Branchew il faudra valider cette inscription, pour cela un code de confirmation vous sera donné à partir de votre email d’inscription afin de pouvoir confirmer cette dernière. Vous reconnaissez que nous pouvons refuser d'autoriser, ou pouvons exiger que vous changiez un nom d'utilisateur qui usurpe l'identité d'une autre personne ou de manquements aux dispositions établies sur notre site Web.
Lors de votre inscription <b><u>un mot de passe contenant au moins 6 caractères</u></b> vous sera exiger mais ce serait préférable qu’elle comporte chiffre et lettre à la fois pour vous assurez un bon niveau de sécurité.
Vous devez nous aviser immédiatement par écrit si vous avez connaissance de toute utilisation non autorisée de votre compte.
Vous êtes responsable de toute activité sur notre site Web résultant de tout manquement à garder vos détails de compte confidentiel et peut être tenu responsable de toutes pertes découlant d'une telle défaillance.
Vous ne devez pas utiliser l’identifiant d’une autre personne et son mot de passe pour accéder à notre site Web, sauf si vous avez la permission expresse de cette personne à le faire. 
<br><br><h4>Voici en gros les principales fonctionnalités du site :</h4>
	1. La possibilité de remplir un profil personnel détaillé sur le site, de 	publier ce profil sur le site.
    <br>
	2. La possibilité de créer ou de rejoindre des groupes sur le site, de partager des informations entre les membres d'un groupe.
    <br>
    3. La possibilité d'envoyer des messages privés via le site à des groupes ou à des individus particuliers inscrits sur le site.
	<br><br>
NB: Nous pouvons désactiver les identifiants et mots de passe, supprimer des comptes et supprimer toute information associée à un compte à notre seule discrétion sans préavis ni explication dans le cas d’une violation ou une mauvaise utilisation du site.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>6- Contenu généré par les utilisateurs</h3>
Réseaux sociaux, sites communautaires, plates-formes de partage de photos, de vidéos et blogs sont une explosion du contenu généré par les utilisateurs qui constituent egalement pour les annonceurs une opportunité de communiquer différemment  auprès des cibles difficiles à atteindre.

Près de 150 millions d'internautes dans le monde consommeraient du contenu généré par les utilisateurs. La publicité constitue la principale source de revenus des sites de ce type. Mais choisir ce genre de support comporte également des risques, notamment en termes d'image, qui ne sont pas à négliger.

<br><br>Avec le web 2.0 les utilisateurs deviennent des acteurs. D’une manière générale il sera permis aux utilisateurs de Branchew de mettre une photo de profil convenable, écrire des messages à des professionnels ayant un compte sur Branchew ou faisant partie de votre réseau professionnel. Tous les professionnels de Branchew auront accès à un chat également pour pouvoir discuter en temps réel sur un sujet avec des professionnels faisant parti de votre domaine d’activité ou autre.
Ensuite il y a une fonctionnalité qui vous permet de voir qui a consulté votre profil ou ses liens. Cette même fonctionnalité permet de détecter des contacts potentiels. En plus c’est très flatteur de voir que l’on a un profil qui “ intéresse”. Il est tentant de voir qui a eu la curiosité et la bonne idée de s’arrêter sur votre profil ou vos liens et de remonter jusqu’à cette personne. Si la personne ne fait pas partie de votre liste de contact professionnel certaines informations de votre profil ne seront pas disponibles pour ce denier. Par contre comme nous l’avons mentionné plus haut quiconque pourra visualiser votre CV.

<br><br>
<h4>Chaque membre de Branchew peut créer des groupes :</h4>
1-  Nous n’accepterons pas la publication de n’importe quelle image.
<br>
2- Nous n’accepterons pas de messages avec des propos malsains.
<br><br>
</div>


<div class="span-22, " align="justify">
<h3>7- Les garanties limitées</h3>
L’utilisation de Branchew qui est un réseau social professionnel impose de bien connaître les spécificités. Jusqu’à présent, il n’y avait que les stars et autres VIP qui pouvaient craindre des détournements de profils sur les réseaux sociaux. Mais le vol d’identité de personnes moins connues existe. Des mesures strictes seront prises en cas de repérage des auteurs de vol d’identité.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>8- Les violations de ces conditions d'utilisation</h3>
Dans les sections précédentes la majorité des principes d’utilisation ont été élaborés toute violation à ces principes entrainent la fermeture de votre compte de manière définitive et ceci sans préavis car les principes ont été clairement définis. En cas de pertinence d’autres sanctions seront appliquées.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>9- Les marques de commerce</h3>
Aujourd’hui, une société peut facilement réaliser un site Web pour promouvoir sa marque ou effectuer des ventes (e-Business). Elle peut ensuite acheter différents moyens pour faire venir des internautes sur ce site. Pour autant, cette relation est souvent statique, à dimension informative seulement et dans un seul sens : de la marque vers le consommateur. Les réseaux sociaux offrent une nouvelle perspective de lien avec le consommateur. En proposant une approche de type “ sociale ”, voire professionnelle la marque peut alors s’immerger dans le flux de connections entre les personnes et bénéficier de facto de la recommandation du bouche à oreille pour se déployer. 
Car une information prenons un événement co-organisé par une marque peut alors être relayé sur les lieux virtuels où se retrouveront les “passionnés” de cet événement, qui alors va se propager très rapidement dès lors qu’il touche effectivement sa cible.

Pour autant, le processus de recrutement n’est pas profondément modifié par l’arrivée des réseaux sociaux. 
<br><br>
<h4>Les entreprises continuent de suivre la même démarche :</h4>
1. Déterminer un profil de poste.
<br>
2. Rédiger et diffuser une annonce d’offre d‘emploi.
<br>
3. Sélectionner des candidats potentiels en fonction du profil choisi.
<br>
4. Réaliser des entretiens d’embauche.
<br>
5. Choisir le candidat adapté au poste à pourvoir.
<br><br>
</div>

<div class="span-22, " align="justify">
<h3>10- Nou contacter</h3>
Rendez-vous sur <a href="http://www.branchew.com/site/contact">http://www.branchew.com/site/contact</a>
</div>
</div>
<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1001172322;
var google_conversion_label = "XiiKCO6DnQQQ4tqy3QM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1001172322/?value=0&amp;label=XiiKCO6DnQQQ4tqy3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
