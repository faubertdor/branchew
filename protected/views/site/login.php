<?php $this->pageTitle=Yii::app()->name . ' | Connexion';?>

<div class="span-22">
<h2>CONNEXION</h2>
</div>

<div class="span-22">
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	
	<div class="row rememberMe small">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
	<div class="span-5" align="left">
		<?php echo CHtml::submitButton('Me connecter',array('class'=>'button'));?>
	</div>
	</div>
	
	<div class="row">
	
		<?php echo CHtml::link('Mot de passe oublié?',RecoverPassword::getRecoverPasswordUrl());?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
