<?php
$this->pageTitle=Yii::app()->name . ' | Nous Contacter';
?>

<div class="span-22">
<h2>NOUS CONTACTER</h2>
</div>


<?php if(Yii::app()->user->hasFlash('contact')): ?>
<div class="span-22 flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>


<?php else: ?>

<div class="span-22">
<p>Si vous avez  des questions ou des suggestions, s’il vous plait remplissez le formulaire suivant pour nous contacter.</p>	


<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'contact-form',
														'enableClientValidation'=>true,
														'clientOptions'=>array(
																				'validateOnSubmit'=>true,
																			   ),
													)); 
?>

	<p class="note small">Les champs avec <span class="required">*</span>  sont obligatoires.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>20,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>45)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="span-3">
	<?php echo $form->labelEx($model,'verifyCode'); ?>	
	</div>
	
	<div class="span-3">
		<?php $this->widget('CCaptcha'); ?>
	</div>
	
	<div class="8">	
		<?php echo $form->textField($model,'verifyCode',array('size'=>23,'maxlength'=>30)); ?>
	</div>
	<?php endif; ?>
	<div class="span-10">
	<div class="span-2">
	<pre> </pre>
	</div>
	<div class="span-3" align="right">
		<?php echo CHtml::submitButton('Envoyer',array('class'=>'button')); ?>
	</div>
	<div class="23">
	<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	</div>
<?php $this->endWidget(); ?>
</div>
<!-- form -->
<?php endif; ?>
</div>
<div class="span-6" align="left">
<h3>Yves Estival</h3>
<h4>Marketing  Specialist
<br>
Cell      : (509) 3404-1409 / 3478-2352
<br>
Email   : Yestival@branchew.com
</h4>
</div>
<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1001172322;
var google_conversion_label = "XiiKCO6DnQQQ4tqy3QM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1001172322/?value=0&amp;label=XiiKCO6DnQQQ4tqy3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

