<div class="span-12 card">
<div class="wide form span-10">
<?php $form=$this->beginWidget('CActiveForm', array(
													 'id'=>'membres-form',
													 'enableAjaxValidation'=>false,
		)); 
?>
<div class="span-10">
		<?php echo $form->errorSummary($model); ?>
</div>
<div class="span-10">
		<?php echo $form->error($model,'pays'); ?>
</div>

<div class="span-10">
<div class="span-3">
		<?php echo $form->labelEx($model,'pays'); ?>
</div>
		
<div class="span-6">
		<?php echo $form->dropDownList($model,'pays',
									   Pays::loadItems(),
									  	 array(	
									  	 		'prompt'=>'S&eacute;lectionnez un pays:',
												'ajax' => array(
												'type'=>'POST', //request type
												'url'=>Controller::createUrl('requestCities'), //url to call.
												
												'update'=>'#'.CHtml::activeId($model,'ville')
										))); 
		?>
</div>
</div>

<div class="span-10">
		<?php echo $form->error($model,'ville'); ?>
</div>

<div class="span-10">
<div class="span-3">
		<?php echo $form->labelEx($model,'ville');?>
</div>
<div class="span-6">
		<?php 
		
			  $ville=array();
			  $ville=array($model->ville=>Villes::itemByInteger($model->ville));		
			  echo $form->dropDownList($model,'ville',$ville);

		 ?>	
</div>
</div>


<div class="span-10">
<div class="span-10">	
		<?php if(CCaptcha::checkRequirements()){ ?>
		<?php echo $form->error($model,'verifyCode'); ?>
</div>



<div class="span-4" align="right">
		<?php $this->widget('CCaptcha'); ?>
</div>	

		
<div class="span-4">	
Tapez le code
</div>	


<div class="span-5">
		<?php echo $form->textField($model,'verifyCode'); ?>
		<?php }?>

</div>	
</div>

<div class="span-10">
<div class="span-4"><pre></pre></div>
<div class="span-5">
		<?php echo CHtml::submitButton('Envoyer',array('class'=>'button'));?>
</div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
