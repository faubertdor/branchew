<?php $this->pageTitle=Yii::app()->name . ' | M\'inscrire';?>
<div class="span-22">
<h2>
Inscrivez-vous gratuitement sur Branchew
</h2>
<h4>Etape 1/2</h4>
<div class="span-3">
<pre>


</pre>
</div>
<div class="wide form span-16 card">
<div class="span-8">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/register',array()),
	'id'=>'membres-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<br>
	<?php echo $form->errorSummary($model); ?>
	
	<div class="span-3">
		<?php echo $form->labelEx($model,'nom');?>
	</div>
	<div class="span-3">
		<?php echo $form->textField($model,'nom',array('size'=>20,'maxlength'=>60)); ?>
	</div>	
	<div class="span-7">
		<?php echo $form->error($model,'nom'); ?>
	</div>

	<div class="span-3">
		<?php echo $form->labelEx($model,'prenom');?>
	</div>
	<div class="span-3">
		<?php echo $form->textField($model,'prenom',array('size'=>20,'maxlength'=>60)); ?>
	</div>	
	<div class="span-7">
		<?php echo $form->error($model,'prenom'); ?>
	</div>

	<div class="span-3">
		<?php echo $form->labelEx($model,'email');?>
	</div>
	<div class="span-3">
		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>60)); ?>
	</div>	
	<div class="span-7">
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="span-3">
		<?php echo $form->labelEx($model,'password');?>
	</div>
	<div class="span-3">
		<?php echo $form->passwordField($model,'password',array('size'=>20,'maxlength'=>60)); ?>
	</div>	
	<div class="span-7">
		<?php echo $form->error($model,'password');	?>
	</div>
	
	<div class="span-3">
		<?php echo $form->labelEx($model,'password_repeat');?>
	</div>
	<div class="span-3">
		<?php echo $form->passwordField($model,'password_repeat',array('size'=>20,'maxlength'=>60)); ?>
	</div>	
	<div class="span-7">
		<?php echo $form->error($model,'password_repeat');	?>
	</div>
	<div class="span-10" align="left">
	<div class="span-3"><pre></pre></div>
	<div class="span-4 ">
		<?php echo CHtml::submitButton('Je m\'inscris',array('class'=>'button'));?>
	</div>
	</div>
	<?php $this->endWidget(); ?>
</div>
<div class="span-5">
<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/negmarron.jpg','carriere',array('width'=>300,'height'=>210));?>
</div>
</div>
</div>