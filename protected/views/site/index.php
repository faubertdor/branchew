<?php $this->pageTitle=Yii::app()->name; ?>

<div class="span-24 home">
<div class="span-13">
<div class="span-13 networking">
<br>
Networking
</div>
<div class="span-13">
<div class="span-2" align="center">
<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/networking.png','networking',array('width'=>90,'height'=>90));?>
</div>
<div class="span-10 doc">
Rejoignez tous les professionnels Haitiens sur <strong>Branchew</strong>, retrouvez vos coll&egrave;gues, camarades d'&eacute;tudes et partagez des id&eacute;es avec d'autres professionnels de votre r&eacute;seau.
<br>
</div>
</div>


<div class="span-13 carriere">
<br>
Carri&egrave;re
</div>
<div class="span-14">
<div class="span-2" align="center">
<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/carriere.png','carriere',array('width'=>90,'height'=>90));?>
</div>
<div class="span-10 doc">
Vos contacts connaissent votre futur employeur. 
Int&eacute;grez des groupes dans vos domaines de comp&eacute;tence.
Emploi, rencontre, et plus encore...
<br>
</div>
<div class="span-13 business">
<br>
Business
</div>
<div class="span-13">
<div class="span-2" align="center">
<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/business.png','business',array('width'=>90,'height'=>90));?>
</div>
<div class="span-10 doc">
Auguementez la visibilit&eacute; de votre entreprise sur <strong>Branchew</strong>, identifiez les experts de votre secteur et recrutez-les.
<br>
</div>
</div>
</div>
<!-- end welcome description -->
</div>

<!-- registration -->

<div class="span-9 signup form">
<div class="span-9" align="left">
<h1>Inscrivez-vous Gratuitement! </h1>
<h3>Le site web de r&eacute;seau social professionnel en Haïti!</h3>
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('site/register',array()),
	'id'=>'membres-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<br>
	<?php echo $form->errorSummary($model); ?>
	
	<div class="span-9">
		<?php echo $form->labelEx($model,'nom');?>

		<?php echo $form->textField($model,'nom',array('size'=>40,'maxlength'=>60)); ?>

		<?php echo $form->error($model,'nom'); ?>
	</div>

	<div class="span-9">
		<?php echo $form->labelEx($model,'prenom');?>
		<?php echo $form->textField($model,'prenom',array('size'=>40,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'prenom'); ?>
	</div>

	<div class="span-9">
		<?php echo $form->labelEx($model,'email');?>
		<?php echo $form->textField($model,'email',array('size'=>40,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="span-9">
		<?php echo $form->labelEx($model,'password');?>
		<?php echo $form->passwordField($model,'password',array('size'=>40,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'password');	?>
	</div>
	
	<div class="span-9">
		<?php echo $form->labelEx($model,'password_repeat');?>
		<?php echo $form->passwordField($model,'password_repeat',array('size'=>40,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'password_repeat');	?>
	</div>
	
	<div class="span-10" align="left">
	<div class="span-3" align="left">
		<?php echo CHtml::submitButton('Je m\'inscris',array('class'=>'button'));?>
	</div>
	<div class="span-6 small" align="left">
		En cliquant sur Je m'inscris, vous acceptez nos 
		<?php echo CHtml::link('Conditions',$this->createUrl('site/policy',array()));?> et reconnaissez avoir lu et comprendre notre 
		<?php echo CHtml::link('Politique d’utilisation des données',$this->createUrl('site/policy',array()));?>, 
		y compris <?php echo CHtml::link('Utilisation des cookies',$this->createUrl('site/policy',array()));?>.
	</div>
	</div>
	<?php $this->endWidget(); ?>
</div>
<!-- end sign up form -->
</div>

<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1001172322;
var google_conversion_label = "XiiKCO6DnQQQ4tqy3QM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1001172322/?value=0&amp;label=XiiKCO6DnQQQ4tqy3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>












