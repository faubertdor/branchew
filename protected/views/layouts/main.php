<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Le Réseau Social Professionnel en Haïti" />
	<meta name="keywords" content="haiti, branchew,reseau social haitien, reseau social professionnel, reseau social,reseau professionnel,retbranche,professionel haitien,professionel,web en haiti,reseau haitien,publier sur internet,internet en haiti,offre,emploi,forum,bourse,études,université,entreprise,économie,éducation,conférence,publicité,bailleur,stratégie,travaux de sortie,mémoire,job,business, pages, pro, jaunes, yellow, telephone, cellulaire, phone, book, hotel, restaurant, annuaire, directory, port-au-prince,haitibusiness,emploi,travail,appel,cv,formation,marché,porte,ouverte,curriculum,vitae,étude,annonce"/>
	<meta name="language" content="en" />
	<meta property="og:image" content="<?php echo Yii::app()->request->baseUrl; ?>/images/B_fb_share.png"/>
	
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/template.css"/>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/assets/ckeditor/ckeditor.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
<!-- Share This  -->
	<script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
<!-- End Share This -->
	
<!-- Domain verification for Google/apps -->
	<meta name="google-site-verification" content="p40GK8ymoV1ykMZOSS78qdcCknyOGbw9v47gEbnvpKI" />
<!-- End domain verification -->
	
<!-- Facebook like api code-->
	<script>
	(
	function(d, s, id) 
	{
  		var js, fjs = d.getElementsByTagName(s)[0];
  		if (d.getElementById(id)) return;
  		js = d.createElement(s); js.id = id;
  		js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1";
  		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk')
	);
	</script>
	
	
<!-- End Facebook like api code -->

<!-- Google Analytics set up -->
	<script type="text/javascript">
  	var _gaq = _gaq || [];
  	_gaq.push(['_setAccount', 'UA-36735788-1']);
  	_gaq.push(['_trackPageview']);

  	(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  	})();
	</script>
<!-- End Google Analytics set up -->

</head>
<!-- End head -->


<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  	var js, fjs = d.getElementsByTagName(s)[0];
  	if (d.getElementById(id)) return;
  	js = d.createElement(s); js.id = id;
  	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=387237101329550";
  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<!-- page begin-->
<div class="top">
<div class="container">
<!-- header begin-->
<div class="span-24 header">
<div class="span-5 logo" align="left">	
<!-- FB like button -->
<div class="span-5 fblike" align="left">
<div class="fb-like" data-href="http://www.facebook.com/pages/Branchew/110720969046224" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
</div>
<!-- End FB like button -->	
<div class="span-5" align="left">
	<?php 
	if(!Yii::app()->user->isGuest)
			echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png','logo',array('width'=>170,'height'=>43)),ContactMembres::model()->getIndexUrl());
	else
			echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/logo.png','logo',array('width'=>170,'height'=>43)),$this->createUrl('index',array()));
	?>	
</div>					
</div>	



<?php 
		if(Yii::app()->user->isGuest)
		{
			$login=new LoginForm();
			$form=$this->beginWidget('CActiveForm', array(
															'id'=>'login-form',
															'action'=>Yii::app()->createUrl('site/index',array()),
															'enableClientValidation'=>false,
															'clientOptions'=>array(
																					'validateOnSubmit'=>true,
																		   ),));
			echo '<div class="span-5"><pre> </pre></div>';																   
			echo '<div class="span-13 loginTop" align="right">';
			echo '<table border="0" cellpadding="0">';
			echo '<tr>';
			echo '<td>';
			echo '<div class="span-4">';
			echo 'Email';
			echo '</div>';
			echo $form->textField($login,'username',array('size'=>24));
			echo $form->error($login,'username');
			echo '<div class="span-4 rememberMeLogin">';
			echo $form->checkBox($login,'rememberMe').' '.$form->label($login,'rememberMe');
			echo $form->error($login,'rememberMe');
			echo '</div>';
			echo '</td>';
			echo '<td>';
			echo '<div class="span-4">';
			echo 'Mot de passe';
			echo '</div>';
			echo $form->passwordField($login,'password',array('size'=>24)); 
			echo $form->error($login,'password');
			echo '<div class="span-4 rememberMeLogin">';
			echo CHtml::link('Mot de passe oublié?',RecoverPassword::getRecoverPasswordUrl());
			echo '</div>';
			echo '</td>';
			echo '<td>';
			echo CHtml::submitButton('Me connecter',array('class'=>'button'));
			echo '</td>';
			echo '</tr>';
			echo '</table>';
			echo '</div>';

			$this->endWidget();	
		}
?>
<div class="span-12">

<?php	
		if(!Yii::app()->user->isGuest)
		{
			echo '<div class="span-12 search" align="center">';
			$form=$this->beginWidget('CActiveForm', array(
															'action'=>Yii::app()->createUrl('profil/default/search',array()),
															'method'=>'GET',
												  		 )); 
	
			$this->widget('CAutoComplete', array(
													'name'=>'autocomplete',
													'url'=>$this->createUrl('default/autocomplete'),
													'htmlOptions'=>array('size'=>30),
													'delay'=>500,
													'max'=>20,
													'methodChain'=>".result(function(event,item){\$(\"#search\").val(item[2]);})",
												));
			echo CHtml::hiddenField('search');	
			echo CHtml::submitButton('Rechercher',array('class'=>'button'));
			$this->endWidget();
		}
		else
		{
			
		}
	echo '</div>';
?>

</div>
<!-- Search form -->

<div class="span-5 topmenu" align="right">
<?php 
if(!Yii::app()->user->isGuest)
{
	$membre = Membres::model()->membreById(Yii::app()->user->id);

	echo CHtml::link(ucwords($membre['prenom']).' '.strtoupper($membre['nom']),ContactMembres::model()->getIndexUrl(Yii::app()->user->getID())).'<br>';
	echo CHtml::link('D&eacute;connexion',$this->createUrl('/site/logout'),array('class'=>'button'));
}
?>
</div>
<!-- Top menu -->

<!-- main menu -->
<div id="mainmenu" class="span-24">
<?php 
/**
	if(Yii::app()->user->isGuest)
	{
		 $this->widget('zii.widgets.CMenu',array( 				  
	 		 'items'=>array(	
	  						array('label'=>'ACCUEIL', 'url'=>Yii::app()->createUrl('site/index',array())),
	  						array('label'=>'CONNEXION', 'url'=>Yii::app()->createUrl('site/login',array())),
							array('label'=>'NOUS CONTACTER','url'=>Yii::app()->createUrl('site/contact',array()))
	  				)));
	}
	
**/
	if(!Yii::app()->user->isGuest)
	  $this->widget('zii.widgets.CMenu',array( 			  
	  'items'=>array(	
	  array('label'=>'ACCUEIL', 'url'=>ContactMembres::model()->getIndexUrl()),
	  array('label'=>'CONTACTS', 'url'=>ContactMembres::model()->getContactsUrl(Yii::app()->user->getID())),
	  array('label'=>'PROFIL', 'url'=>ContactMembres::model()->getViewUrl(Yii::app()->user->getID()),
	  'items'=>array(
	  					 array('label'=>'Voir mon CV', 'url'=>ContactMembres::model()->getCvUrl(Yii::app()->user->getID())),
	  					 array('label'=>'Modifier profil', 'url'=>ContactMembres::model()->getUpdateUrl(Yii::app()->user->getID()))
	  		        )),
	  
	  array('label'=>'EMPLOI', 'url'=>OffreEmp::model()->getIndexUrl(),
	  'items'=>array(
	  					 array('label'=>'Offres d\'emploi', 'url'=>OffreEmp::model()->getIndexUrl()),
	  					 array('label'=>'Gérer mes emplois', 'url'=>OffreEmp::model()->getMyOpenOffersUrl())
	  		        )),
	  array('label'=>'MESSAGES', 'url'=>Messages::model()->getReceivedUrl(),
	  		'items'=>array(
	 						 	array('label'=>'Boite de reception', 'url'=>Messages::model()->getReceivedUrl()),
								array('label'=>'Recommandations', 'url'=>Recommandations::model()->getReceivedUrl())
	  					  )),
	    array('label'=>'GROUPES', 'url'=>Groupes::model()->getIndexUrl(),
	  		'items'=>array(
	 						 	array('label'=>'Créer un groupe', 'url'=>Groupes::model()->getCreateUrl()),
								array('label'=>'Mes groupes', 'url'=>Groupes::model()->getMyGroupsUrl()),
								array('label'=>'Chat room', 'url'=>ContactMembres::model()->getChatroomUrl())
	  					  )),
	  array('label'=>'PLUS +','url'=>Groupes::model()->getIndexUrl(),
	  'items'=>array(
	  					array('label'=>'Recherche avancée', 'url'=>Yii::app()->createUrl('profil/default/advancedSearch',array())),
	  					array('label'=>'Créer mon entreprise', 'url'=>Entreprises::model()->getCreateUrl()),
	  					array('label'=>'Mes entreprises', 'url'=>AdminEntreprises::model()->getMyEntreprisesUrl()),
	  					array('label'=>'Mes visites', 'url'=>VisiteMembres::model()->getViewUrl()),
	  					array('label'=>'Mes articles', 'url'=>Articles::model()->getViewArticlesUrl(Yii::app()->user->getID()))
	  				))
	  
	 ))); 
?>
</div>
<!-- end main menu -->

</div>
<!-- end header -->



</div>
</div>

<div class="page container">
<!-- Content -->
<div class="span-23 content">
	<?php echo $content; ?>
</div>
<!-- End Content -->



<!-- footer begin -->
<div class="footermenu span-23" align="center">
<?php 

if(Yii::app()->user->isGuest)
{
	echo Chtml::link('Accueil', $this->createUrl('/site/index')).' | ';  
	echo Chtml::link('Connexion', $this->createUrl('/site/login')).' |  '; 
	echo Chtml::link('Emploi', OffreEmp::model()->getIndexUrl()).' | '; 
	echo Chtml::link('Conditions d\'utilisation', $this->createUrl('/site/policy')).' | '; 
	echo Chtml::link('Nous contacter', $this->createUrl('/site/contact'));
}
else 
{

?>
<div class="span-4 ftformat" align="left">
<strong><?php echo CHtml::link('ACCUEIL', ContactMembres::model()->getUpdateUrl(Yii::app()->user->getID()));?></strong><br>
<?php echo Chtml::link('Contactez-Nous', $this->createUrl('/site/contact'));?><br>
<?php echo Chtml::link('Conditions d\'utilisation', $this->createUrl('/site/policy'));?>
</div>

<div class="span-4 ftformat" align="left">
<strong><?php echo CHtml::link('PROFIL',ContactMembres::model()->getViewUrl(Yii::app()->user->getID()));?></strong><br>
<?php echo CHtml::link('Voir mon profil',ContactMembres::model()->getViewUrl(Yii::app()->user->getID()));?><br>
<?php echo CHtml::link('Voir mon CV',ContactMembres::model()->getCvUrl(Yii::app()->user->getID()));?><br>
<?php echo CHtml::link('Modifier profil',ContactMembres::model()->getUpdateUrl(Yii::app()->user->getID()));?>
</div>
<div class="span-5 ftformat" align="left">
<strong><?php echo Chtml::link('EMPLOI', OffreEmp::model()->getIndexUrl());?></strong><br>
<?php echo CHtml::link('Les offres d’emploi', OffreEmp::model()->getIndexUrl());?><br>
<?php echo CHtml::link('Créer une offre d’emploi', OffreEmp::model()->getCreateUrl());?><br>
<?php echo CHtml::link('Gérer mes emplois',OffreEmp::model()->getMyOpenOffersUrl());?>
</div>

<div class="span-4 ftformat" align="left">
<strong><?php echo CHtml::link('GROUPES',Groupes::model()->getMyGroupsUrl());?></strong><br>
<?php echo CHtml::link('Créer un groupe',Groupes::model()->getCreateUrl());?><br>
<?php echo CHtml::link('Chat room',ContactMembres::model()->getChatroomUrl());?>
</div>

<div class="span-5" align="left">
<strong><?php echo CHtml::link('ENTREPRISES',AdminEntreprises::model()->getMyEntreprisesUrl());?></strong><br>
<?php echo CHtml::link('Le profil de mon entreprise',Entreprises::model()->getCreateUrl());?>

</div>
	
<?php 
}
?>

</div>
<!-- end footermenu -->

<div class="span-24 footer" align="center">

Copyright &copy; <?php echo date('Y').' | '; ?> 
www.branchew.com
</div>
<!-- end footer -->

<!-- end right menu -->
</div>
<!-- end page -->
</body>
</html>