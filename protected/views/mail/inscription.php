<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	
	<title>Bienvenue sur branchew</title>
</head>

<body>
<div class="container">
	<div class="span-15">
		<img src="http://www.branchew.com/images/logo" width=170 height=60></img>
	</div>
							  
	<div class="span-15">
							  <br></br>
							  <strong class="large">Bienvenue sur branchew</strong>
							  <br></br>
							  <br></br>
							  
							  Restez connecter &agrave; votre r&eacute;seau!
							  <br></br>
							  Rejoignez tous les professionnels de branchew
							  <br></br>
						      Partagez des id&eacute;es avec d'autres professionnels de votre r&eacute;seau
							  <br></br>
							  Int&eacute;grez des groupes de vos domaines de comp&eacute;tence
							  <br></br>
							  Et plus encore...
							   <br></br>
							  <br></br>
							  <br></br>
							  <strong class="large">
							   Cliquez sur le lien suivant ou copiez-le dans la barre d’adresse de votre navigateur Internet pour activer votre compte.
							  </strong>
							  <br></br>
							  <br></br>
							  
							  http://www.branchew.com/index.php?r=site/activateProfile<?php echo '&id='.$membre->password.'&email='.$membre->email;?>
</div>
</div>	

</body>
</html>