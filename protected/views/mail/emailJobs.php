<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en"/>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="http://www.branchew.com/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="http://www.branchew.com/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="http://www.branchew.com/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="http://www.branchew.com/css/main.css" />
	<link rel="stylesheet" type="text/css" href="http://www.branchew.com/css/form.css" />

	
<title>Offres d'emploi disponible sur Branchew!</title>
</head>
<body>
<div class="container">
<div class="span-18">
	<div class="span-6">
		<img src="http://www.branchew.com/images/logo" width=170 height=60></img>
	</div>
	
	<div class="span-10">
	<h3>
	<a href="http://www.branchew.com/index.php?r=emploi/offreEmp/index">
		Offres d'emploi disponible sur Branchew !
	</a>
	</h3>
	</div>
</div>

<br></br>

<div class="span-18" align="left">	
<?php	
		
		
		foreach ($offreEmp as $value) {
			$this->renderPartial('application.views.mail._itemJob',array('data'=>$value));
		}
?>
</div>

<br></br>

<div class="span-18">
<strong><a href="http://www.branchew.com">Le réseau social professionnel Haïtien !</a></strong>
</div>

</div>
</body>
</html>	