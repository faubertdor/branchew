<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'experiences-form',
														'enableAjaxValidation'=>true,
													));
?>
	
<div class="span-16" align="left">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
</div>

<div class="span-16" align="left">
	
	<div class="span-16">
	<?php echo $form->error($model,'entreprise'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'entreprise'); ?>
		<?php echo $form->textField($model,'entreprise',array('size'=>30,'maxlength'=>60)); ?>
	</div>




	<div class="span-16">
	<?php echo $form->error($model,'position'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>30,'maxlength'=>60)); ?>
		
	</div>




	<div class="span-16">
	<?php echo $form->error($model,'adresse'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'adresse'); ?>
		<?php echo $form->textArea($model,'adresse',array('rows'=>3, 'cols'=>40)); ?>
	</div>
		
	
	
	
	
	<div class="span-16">
	<?php echo $form->error($model,'date_debut'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'date_debut'); ?>
		<?php 
			  echo $form->dropDownList($model,'moisDebut',Helper::date('mois'),array('prompt'=>'Mois :'));
			   echo ' - ';
			  echo $form->dropDownList($model,'anneeDebut',Helper::date('annee'),array('prompt'=>'Année :')); 
			  ?>
		
	</div>
	


	
	
	<div class="span-18">
	<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="span-3">
		<?php echo $form->labelEx($model,'description'); ?>
	</div>
	
	<div class="span-10">
		<textarea id="description" name="description" ><?php echo $model->description;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('description',
				{
					toolbar : 'Basic'
				} );
			</script>	
	</div>
	
	
	
	
	
	
	<div class="span-16">
	<?php echo $form->error($model,'date_fin'); ?>
	</div>
	<div id="date-fin" class="span-16">
		<?php echo $form->labelEx($model,'date_fin'); ?>
		<?php 
			  echo $form->dropDownList($model,'moisFin',Helper::date('mois'),array('prompt'=>'Mois :'));
			   echo ' - ';
			  echo $form->dropDownList($model,'anneeFin',Helper::date('annee'),array('prompt'=>'Année :')); 
		?>
	</div>
</div>
<div class="span-13" align="right">
	
	<?php $url=$this->createUrl('default/update',array());
		echo CHtml::submitButton($model->isNewRecord ? 'Créer' : 'Sauvegarder',array('class'=>'button')); echo '  ou  '.CHtml::link('Annuler',$url);?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->