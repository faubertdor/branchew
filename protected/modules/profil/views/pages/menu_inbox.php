
<div class="span-5 vmenu" align="left">
<div id="inbox" class="span-5">
<div class="title">
<strong>Boite de reception</strong>
</div>
<div class="body">
<?php echo CHtml::link('Ecrire un message',Messages::model()->getComposeUrl(),array('class'=>'button'));?>
<br>
<strong>
<?php 
		if($nouveauMessages==='0')
			echo CHtml::link('Messages  ('.CHtml::encode($nouveauMessages).') ',Messages::model()->getReceivedUrl());
		
		else 
			echo CHtml::link('Messages  (<strong class="infobox">'.CHtml::encode($nouveauMessages).'</strong>)',Messages::model()->getReceivedUrl());
			
			
?>
</strong>
<br>

<strong>
<?php 

	if($demandesContacts==='0')
		echo CHtml::link('Contacts à confirmer  ('.CHtml::encode($demandesContacts).')',ContactMembres::model()->getReceivedUrl());
	else 
		echo CHtml::link('Contacts à confirmer  (<strong class="infobox">'.CHtml::encode($demandesContacts).'</strong>)',ContactMembres::model()->getReceivedUrl());
?>
</strong>
<br>
<strong><?php //echo CHtml::link('Recommandations',Recommandations::model()->getReceivedUrl())?>
<?php 

	if($recommandationRequest==='0')
		echo CHtml::link('Recommandations ('.CHtml::encode($recommandationRequest).') ',Recommandations::model()->getReceivedUrl());
	else 
		echo CHtml::link('Recommandations (<strong class="infobox">'.CHtml::encode($recommandationRequest).'</strong>)',$this->createUrl('recommandations/received#demandes'));
?>
</strong>
</div>
<div class="title">
<strong>El&eacute;ments envoy&eacute;s</strong>
</div>
<div class="body">
<strong><?php echo CHtml::link('Messages',Messages::model()->getSentUrl());?></strong>
</div>
</div>
</div>


<div class="span-5 vmenu" align="left">
<div id="inbox" class="span-5">
<div class="title">
<strong>Articles</strong>
</div>
<div class="body">
<strong>
<?php echo CHtml::link('Ecrire un article',Articles::model()->getCreateUrl(),array('class'=>'button'))?>
</strong>
<br>
<strong>
<?php 
	  echo CHtml::link('Mes articles',Articles::model()->getViewArticlesUrl(Yii::app()->user->getID()));
?>
</strong>
</div>
</div>
</div>