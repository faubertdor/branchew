<?php $this->pageTitle=Yii::app()->name.' | Modifier compte';?>
<div class="span-23">
<div class="span-16" align="left">
<h2>Informations compte</h2>
<div class="span-16" align="left">
<div class="span-5">
<?php echo CHtml::link('Charger une photo',Membres::model()->getUploadImageUrl(),array('class'=>'button'));?> 
</div>
<div class="span-5">
<?php echo CHtml::link('Changer mon mot de passe',Yii::app()->createUrl('site/resetPassword',array('token'=>$model->password,'email'=>$model->email)),array('class'=>'button'));?>
</div>
</div>
<?php echo $this->renderPartial('_form', array(
												'model'=>$model
											  )); 
?>
</div>
<!-- end form -->
<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div> 