<div class="wide form">
<br>
<br>
<?php 
		$form=$this->beginWidget('CActiveForm', array(
														'id'=>'membres-form',
														'enableAjaxValidation'=>true,
												     )); 
?>
<div class="span-16" align="left">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
</div>

<div class="span-16" align="left">
	
	<div class="span-16">
		<?php echo $form->error($model,'nom'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'nom'); ?>
		<?php echo $form->textField($model,'nom',array('size'=>25,'maxlength'=>60)); ?>
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'prenom'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'prenom'); ?>
		<?php echo $form->textField($model,'prenom',array('size'=>25,'maxlength'=>60)); ?>
		
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'sexe'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'sexe'); ?>
		<?php echo $form->dropDownList($model,'sexe',array('m'=>'Masculin','f'=>'Feminin'),array('prompt'=>'Selectionnez:')); ?>	
	</div>
	
	<div class="span-16">
	<?php echo $form->error($model,'date_naissance'); ?>
	</div>
	
	<div class="span-16">
		<?php echo $form->labelEx($model,'date_naissance');?>
		<?php 
	
			  echo $form->dropDownList($model,'jour',Helper::date('jour'),array('prompt'=>'Jour :')); 
			  echo ' - ';
			  echo $form->dropDownList($model,'mois',Helper::date('mois'),array('prompt'=>'Mois :')); 
			  echo ' - ';
			  echo $form->dropDownList($model,'annee',Helper::date('annee'),array('prompt'=>'Année :')); 
			 
			?>	
	</div>
	
	
	<div class="span-16">
	<?php echo $form->error($model,'statut_matrimonial'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'statut_matrimonial'); ?>
		<?php echo $form->dropDownList($model,'statut_matrimonial',array('0'=>'Célibataire','1'=>'Marié'),array('prompt'=>'Selectionnez:')); ?>
		
	</div>



	<div class="span-16">
	<?php echo $form->error($model,'domaine_activite'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'domaine_activite'); ?>
		<?php echo $form->dropDownList($model,'domaine_activite',DomaineActivites::loadItems()); ?>
	
	</div>
	
	
	
	<div class="span-16">
	<?php echo $form->error($model,'objectif'); ?>
	</div>
	<div class="span-3">
		<?php echo $form->labelEx($model,'objectif'); ?>	
	</div>
	<div class="span-10">	
		<textarea id="objectif" name="objectif" ><?php echo $model->objectif;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('objectif',
				{
					toolbar : 'Basic'
				} );
			</script>	
	</div>




	<div class="span-16">
	<?php echo $form->error($model,'pays'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'pays'); ?>
		<?php echo $form->dropDownList($model,'pays',
									   Pays::loadItems(),
									  	 array(
												'ajax' => array(
												'type'=>'POST', //request type
												'url'=>Controller::createUrl('requestCities'), //url to call.
												
												'update'=>'#'.CHtml::activeId($model,'ville') //selector to update
												//'data'=>'js:javascript statement' 
												//leave out the data key to pass all form values through
										))); 
		?>
		
	</div>
	
	
	<div class="span-16">
	<?php echo $form->error($model,'ville'); ?>
	</div>	
		
	<div class="span-16">
		<?php
				 //Ville
				$ville=array();
				$ville=array($model->ville=>Villes::itemByInteger($model->ville));

				echo $form->labelEx($model,'ville');
		?>
		<?php echo $form->dropDownList($model,'ville',$ville,array('prompt'=>'Selectionnez un pays:')); ?>		
	</div>


	
	<div class="span-16">
		<?php echo $form->error($model,'adresse'); ?>
	</div>
	
	<div class="span-16">
		<?php echo $form->labelEx($model,'adresse'); ?>
		<?php echo $form->textArea($model,'adresse',array('rows'=>3, 'cols'=>40)); ?>
	</div>
	
	<div class="span-16">
	<?php echo $form->error($model,'siteweb_entreprise'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'siteweb_entreprise'); ?>
		<?php echo $form->textField($model,'siteweb_entreprise',array('size'=>25,'maxlength'=>60)); ?>	
		<?php echo 'http://www.example.com'?>
	</div>
	
	
	
	<div class="span-16">
	<?php echo $form->error($model,'telephone'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>15,'maxlength'=>15)); ?>
	</div>


	
	
	<div class="span-16">
	<?php echo $form->error($model,'info_additionel'); ?>
	</div>

	<div class="span-16">
		<?php echo $form->labelEx($model,'info_additionel'); ?>
		<?php echo $form->textArea($model,'info_additionel',array('rows'=>3, 'cols'=>40)); ?>
	</div>
</div>
	<div class="span-13" align="right">
		<?php 	
				echo CHtml::submitButton('Sauvegarder',array('class'=>'button')); 
				echo '  ou  '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl());
		 ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
