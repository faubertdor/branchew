<?php $this->pageTitle=Yii::app()->name.' | Charger une photo de profil'?>
<div class="span-23">
<div class="form">
<div class="span-16"><h2>Charger une photo de profil</h2></div>
<div class="span-16">
	<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data'))?>
	<?php echo CHtml::error($imageProfil, 'image')?>
	<?php echo CHtml::activeFileField($imageProfil, 'image')?>
</div>
<div class="span-16">
<div class="span-2">
	<?php echo CHtml::submitButton('Charger',array('class'=>'button'))?>
	<?php echo CHtml::endForm()?>
</div>
<div class="span-2">
<?php echo 'ou '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl())?>
</div>
</div>
</div>
</div>