<?php $this->pageTitle=Yii::app()->name.' | Mes contacts';?>
<div class="span-16">
<div class="span-16">
<h2>Mes Contacts</h2>
</div>

<div class="span-16">	
<?php	
		$this->widget('zii.widgets.CListView', array(
  					 									 'dataProvider'=>$contactMembres,
   					 									 'itemView'=>'_itemView',   // refers to the partial view named '_itemView'
													 ));
?>
</div>
</div>
<!-- end contacts listing -->

<div class="span-7">
<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>
<!-- end right menu -->