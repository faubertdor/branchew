<div class="span-16 vcard">
<div class="span-7" align="left">
<?php 
	
	echo Membres::model()->getViewID($data['membres_id'], $data['chemin_avatar'], $data['nom'], $data['prenom'], $data['domaine_activite']);

?>
</div>
<div class="span-3" align="center">
<strong><?php echo CHtml::link('Accepter',$this->createUrl('contactMembres/accept',array('id'=>$data['membres_id'])));?></strong>
</div>
<div class="span-3" align="center">
<strong><?php echo CHtml::link('Refuser',$this->createUrl('contactMembres/deny',array('id'=>$data['membres_id'])));?></strong>	  
</div>
</div>
