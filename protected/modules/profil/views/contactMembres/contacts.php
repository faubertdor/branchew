<?php $this->pageTitle=Yii::app()->name.' | '.$membre['nom'].' '.$membre['prenom'];?>
<div class="span-16">	
<div class="span-16">	


<h2><?php echo 'Contacts de '.$membre['nom'].' '.$membre['prenom'];;?></h2>	

<?php //echo CHtml::link('Retour ',ContactMembres::model()->getViewUrl($membre['id']));?>

</div>

<div class="span-16" align="left">	
<?php	
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$contactMembres,
   					  'itemView'=>'_itemViewContacts',   // refers to the partial view named '_result'
   
					 ));
?>
</div>
</div>
<!-- end contacts listing -->

<div class="span-7">
<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>
<!-- end right menu -->
