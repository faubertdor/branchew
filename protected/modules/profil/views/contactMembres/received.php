<?php $this->pageTitle=Yii::app()->name.' | Demandes de contacts';?>
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest))?>
</div>
<div class="span-18" align="left">
<h2>Demandes de contacts</h2>
</div>
<div class="span-18" align="left">
<?php 		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$demandes,
   					  'itemView'=>'_itemViewDemande',   // refers to the partial view named '_itemViewDemande'
   
					 ));
		
?>
</div>
