<div class="container">
<div class="span-4">
<?php if($membre['chemin_avatar']!=NUll){

		echo CHtml::link(CHtml::image($membre['chemin_avatar'],$membre['nom'].' '.$membre['prenom'],array(
																								'width'=>150,
																								'height'=>160,																			
																							 )),ContactMembres::model()->getViewUrl($membre['id']));
}
?>

</div>
<div class="span-12" align="left">

<h1><?php echo CHtml::link($membre['nom'].' '.$membre['prenom'],ContactMembres::model()->getViewUrl($membre['id']));?></h1>
<h3><?php echo DomaineActivites::itemByInteger($membre['domaine_activite']).', '.Villes::itemByInteger($membre['ville']);?></h3>

<div class="span-12" align="left">
<h5><?php 	
		
			if($contactNombre!=0)
				echo ' '.CHtml::link($contactNombre.' contacts',
											$this->createUrl('contactMembres/contacts',array('id'=>$membre['id'])));
			else 
			 echo ' 0 contacts';
	?>
</h5>
<h5><?php 
	if($visiteNombre!=0)
				echo ' '.$visiteNombre.' visites';
			else 
			 echo ' 0 visites';
	?>
</h5>
<h5><?php echo CHtml::link($membre['siteweb_entreprise'],$membre['siteweb_entreprise']);?></h5>	

</div>	
</div>
</div>
<br>
<div class="span-16" align="left">
<h3>Objectif et spécialité</h3>
<?php echo '<h4>'.$membre['objectif'].'</h4>';?>
</div>