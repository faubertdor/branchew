<?php $this->pageTitle=Yii::app()->name.' | '.$message->objet;?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest))?>
</div>

<div class="span-17">
<div class="span-17">
<?php 

	$this->widget('zii.widgets.CListView', array(
					  'id'=>'Discussion',
  					  'dataProvider'=>$conversation,
   					  'itemView'=>'_conversation',   // refers to the partial view named '_conversation'
				 ));
					 


?>
</div>


<!-- Reply section -->
<div class="span-17 newscard">
<h4>Ecrire une r&eacute;ponse</h4>
<div class="span-14 wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'messages-form',
														'enableAjaxValidation'=>true,
							  ));
?>
<div class="span-14" align="left">
<?php echo $form->errorSummary($reply); ?>
</div>
	
	<div class="row">
	<?php echo $form->hiddenField($reply,'objet');?>
	<?php echo $form->hiddenField($reply,'destinataire_id');?>
	</div>

	<div class="span-10">
		<?php echo $form->textArea($reply,'contenu',array('rows'=>4,'cols'=>70))?>
		<?php echo $form->error($reply,'contenu'); ?>
	</div>

	<div class="span-11" align="left">
		<?php echo CHtml::submitButton('Envoyer',array('class'=>'button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
</div>
