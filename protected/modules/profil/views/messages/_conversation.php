<div class="span-17" align="left">
<div class="span-17 newscard">
<div class="span-2" align="right">
<?php echo CHtml::link(CHtml::image($data['chemin_avatar'],$data['prenom'].' '.$data['nom'],array('width'=>60,'height'=>60)),ContactMembres::model()->getViewUrl($data['membres_id']));?>
</div>
<div class="span-14">
<div class="span-14">
<div class="span-8">
<h4>
<?php echo CHtml::link(ucwords($data['prenom']).' '.strtoupper($data['nom']),ContactMembres::model()->getViewUrl($data['membres_id']));?>
</h4>
</div>
<div class="span-5"align="right">
<?php echo Yii::app()->dateFormatter->format("dd MMMM y, HH:mm",$data['date_envoie']) ?>
</div>
</div>
<div class="span-14" align="left">
<div class="message-content">
<?php echo $data['contenu']; ?>
</div>
</div>
</div>
</div>
</div>