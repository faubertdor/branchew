<?php $this->pageTitle=Yii::app()->name.' | Boite de reception';?>
<div class="span-23">
<div class="span-5">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest))?>
</div>

<div class="span-17">
<div class="span-18" align="left">
<h2>Messages</h2>
</div>
<?php if(Yii::app()->user->hasFlash('messageSent')):?>
    <div class="span-17 flash-success">
        <?php echo Yii::app()->user->getFlash('messageSent'); ?>
    </div>
<?php endif; ?>
<div class="span-18" align="left">
<?php 

	$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$received,
   					  'itemView'=>'_itemViewMessages',   // refers to the partial view named '_itemViewMessages'
   
				 ));
					 


?>
</div>
</div>
</div>