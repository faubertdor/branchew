<?php $this->pageTitle=Yii::app()->name.' | Messages';?>
<?php if(Yii::app()->user->hasFlash('messageSent')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('messageSent'); ?>
    </div>
<?php endif; ?>

<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox');?>
</div>


	
<div class="span-17" align="left">
<div class="span-17">

<h2><?php echo $destinataire;?></h2>	

</div>
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'messages-form',
														'enableAjaxValidation'=>true,
							  ));
?>
<div class="span-17" align="left">
<?php echo $form->errorSummary($model); ?>
</div>
	
		<?php echo $form->hiddenField($model,'destinataire_id');?>

	<div class="row">
		<?php echo $form->labelEx($model,'objet'); ?>
		<?php echo $form->textField($model,'objet',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'objet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contenu'); ?>
		<?php echo $form->textArea($model,'contenu',array('rows'=>10,'cols'=>45))?>
		<?php echo $form->error($model,'contenu'); ?>
	</div>


	<div class="span-11" align="right">
		<?php echo CHtml::submitButton('Envoyer'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

</div>

</div>