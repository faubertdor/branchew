<div class="form" align="left">

<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'messages-form',
														'enableAjaxValidation'=>true
							  						));
?>
	<?php echo $form->errorSummary($model); ?>

	<div class="span-4">
		<?php echo $form->labelEx($model,'objet'); ?>
		<?php echo $form->textField($model,'objet',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'objet'); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,'contenu'); ?>
		<?php echo $form->textArea($model,'contenu',array('rows'=>6,'cols'=>30))?>
		<?php echo $form->error($model,'contenu'); ?>
	</div>


	<div class="span-5" align="right">
		<?php echo CHtml::ajaxSubmitButton('Envoyer',Messages::model()->getComposeProfileUrl()); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->