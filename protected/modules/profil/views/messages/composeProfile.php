<div class="container" align="left">
<div class="span-10" align="left">
<?php 

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'message',
    'options'=>array(
        'title'=>'Nouveau message',
        'width'=>'350',
        'height'=>'auto',
        'autoOpen'=>false,
        'resizable'=>false,
        'modal'=>true,
        'overlay'=>array(
            'backgroundColor'=>'#000',
            'opacity'=>'0.5'
        ),
        'buttons'=>array(
            //'OK'=>'js:function(){alert("OK");}',
            //'Cancel'=>'js:function(){$(this).dialog("close");}',    
        ),
    ),
));

?>
<div class="span-5" align="left">
<?php 
	
	//if(Yii::app()->request->isAjaxRequest)
    	echo $this->renderPartial('application.modules.profil.views.messages._form',array(
																							'model'=>$model,
						 				 											 ));
?>
</div>
				 				  
<?php 				  

	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
?>

</div>
</div>