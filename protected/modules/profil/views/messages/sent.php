<?php $this->pageTitle=Yii::app()->name.' | Messages envoyés';?>

<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest))?>
</div>

<div class="span-17" align="left">
<div class="span-18" align="left">
<h2>Messages envoyés</h2>
</div>

<div class="span-18" align="left">
<?php 

	$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$sent,
   					  'itemView'=>'_itemViewSent',   // refers to the partial view named '_itemViewMessages'
   
				 ));
					 


?>
</div>
</div>
</div>