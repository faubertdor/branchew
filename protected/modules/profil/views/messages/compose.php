<?php $this->pageTitle=Yii::app()->name.' | Nouveau message';?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest));?>
</div>
<div class="span-17" align="left">
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'messages-form',
														'enableAjaxValidation'=>true,
							  ));
?>
<div class="span-12" align="left">
<?php echo $form->errorSummary($model); ?>
</div>
<div class="span-12">
<div class="span-2" align="left">
<?php echo $form->labelEx($model,'destinataire_id'); ?>
</div>

<div class="span-9" align="left">
<?php 

	$this->widget('CAutoComplete', array(
											'name'=>'message',
											'url'=>$this->createUrl('messages/autocomplete'),
											'htmlOptions'=>array('size'=>20),
											'delay'=>500,
											'max'=>10,
											'methodChain'=>".result(function(event,item){\$(\"#destinataire_id\").val(item[1]);})",
										)
   
				 );

	echo CHtml::hiddenField('destinataire_id');
?>
</div>
</div>

	<div class="span-12">
	<div class="span-2" align="left">
		<?php echo $form->labelEx($model,'objet'); ?>
	</div>
	<div class="span-9">
		<?php echo $form->textField($model,'objet',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'objet'); ?>
	</div>
		
	</div>
	<div class="span-14" align="left">
	<div class="span-2" align="left">
		<?php echo $form->labelEx($model,'contenu'); ?>
	</div>
	<div class="span-11">
		<?php echo $form->textArea($model,'contenu',array('rows'=>8,'cols'=>45))?>
		<?php echo $form->error($model,'contenu'); ?>
	</div>
	</div>
	<div class="span-12" align="left">
		<div class="span-4" align="right">
		<?php echo CHtml::submitButton('Envoyer',array('class'=>'button')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
</div>
<!-- form -->

</div>
</div>