<div class="span-18 vcard">
<div class="span-7" align="left">
<?php 
	
	echo Membres::model()->getViewID($data['membres_id'], $data['chemin_avatar'], $data['nom'], $data['prenom'], $data['domaine_activite']);

?>
</div>
<div class="span-5" align="left">

<?php 
		if($data['date_lecture']===NULL)
			echo '<strong>'.CHtml::link($data['objet'],Messages::model()->getReadUrl($data['messages_id'])).'</strong>';
		else
			echo CHtml::link($data['objet'],Messages::model()->getReadUrl($data['messages_id']));
?>		
</div>

<div class="span-4" align="left">
	
	<?php 
			if($data['date_lecture']===NULL)
				echo '<strong>'.Yii::app()->dateFormatter->format("dd MMMM y, HH:mm",$data['date_envoie']).'</strong>';
			else 
				echo Yii::app()->dateFormatter->format("dd MMMM y, HH:mm",$data['date_envoie']);
	?>
	
</div>

<div class="span-1" align="right">	
	<strong>
			<?php echo CHtml::link('Effacer','#',array('submit'=>array('messages/delete'),
													   'params'=>array('id'=>$data['messages_id']),
													   'confirm'=>'Voulez-vous vraiment supprimer cet élément ?'
								 ));
			?>
	</strong>
</div>
</div>







