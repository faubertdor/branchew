<?php $this->pageTitle=Yii::app()->name.' | Mes visites';?>
<div class="span-23" align="left">
<div class="span-16" align="left">
<h2>Mes visites</h2>
<div class="span-16" align="left">

	<?php 
			
		$this->widget('zii.widgets.CListView', array(
  					 								  'dataProvider'=>$dataProvider,
   					  								  'itemView'=>'_itemView',   // refers to the partial view named '_itemView'
   
					 ));
	?>

</div>	
</div>

<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
<!-- end right menu -->
</div>
