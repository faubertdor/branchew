<div class="span-16">
<div class="span-12 vcard" align="left">
<div class="span-7" align="left">
<?php 
	
	echo Membres::model()->getViewID($data['membres_id'], $data['chemin_avatar'], $data['nom'], $data['prenom'], $data['domaine_activite']);

?>
</div>
<div class="span-3 small" align="left">
<br>
<strong>
<?php 
	
	$date=Helper::dateViewFormat($data['date']);
	echo $date;
?>
</strong>
</div>
</div>
</div>