<?php Yii::app()->clientScript->registerMetaTag($data['statut'],'Description')?>
<meta property="og:title" content="<?php echo 'Branchew statut | '.$data['statut'];?>"/>
<meta property="og:type" content="Picture"/>
<meta property="og:image" content="http://www.branchew.com<?php echo $data['m_avatar'];?>"/>
<meta property="og:description" content="<?php echo $data['statut'];?>"/>
<?php 
		if(isset($setTitle))
			$this->pageTitle='Branchew | '.$data['statut'];
?>
<div class="span-2" align="right">
<?php 
echo CHtml::link(CHtml::image($data['m_avatar'],$data['prenom'].' '.$data['nom'],array(
																			'width'=>50,
																			'height'=>50,
																		)),ContactMembres::model()->getViewUrl($data['m_id']));
?>																		
</div>
<div class="span-12">
<div class="span-8"><h4>
<?php 
 echo CHtml::link(ucwords($data['prenom']).' '.strtoupper($data['nom']),ContactMembres::model()->getViewUrl($data['m_id']));
?>
</h4></div>
<div class="span-10">
<?php echo $data['statut'];?>
</div>
<div class="span-1" align="right">
<div class="fb-like" data-href="http://www.branchew.com<?php echo Statuts::model()->getViewUrl($data['id']);?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
</div>
<div class="span-13">
<div class="span-5" align="left">
<?php echo 'le '.Yii::app()->dateFormatter->format("dd MMMM y, HH:mm", $data['date']);?>
</div>
<div id="<?php echo $data['id'];?>" class="span-8" align="left">

<?php
		
	  if(!Yii::app()->user->isGuest)
	  echo CHtml::link('Commenter', '#',
                    array('onClick'=>' {'.CHtml::ajax(
                        array(
                                'update'=>'#'.$data['id'],
                                'url'=>Yii::app()->createUrl('profil/statuts/comment',array('id'=>$data['id'])),
                                'type'=>'GET',),
                                        array('live'=>false, 'id'=>$data['id'],)

                                            ).' return false; }'

                        ));
?>
<?php //echo CHtml::ajaxLink('Commenter',Yii::app()->createUrl('profil/statuts/comment',array('id'=>$data['id'])),array('update'=>'#'.$data['id']));?>
</div>

<div class="span-3" align="left">
<?php 
	  if($data['b_membres_id']==Yii::app()->user->getID())
	  echo CHtml::link('Supprimer','#',array('submit'=>array('statuts/delete'),
													   'params'=>array('id'=>$data['id']),
													   'confirm'=>'Voulez-vous vraiment supprimer?'
								 ));						 
?>
</div>
</div>
</div>