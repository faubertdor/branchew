<div class="span-23">
<div class="form">
<div class="span-22"><h2>Charger une photo pour votre article</h2></div>
<div class="span-22">
	<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data'))?>
	<?php echo CHtml::error($image, 'image')?>
	<?php echo CHtml::activeFileField($image, 'image')?>
</div>
<div class="span-18">
<div class="span-2">
	<?php echo CHtml::submitButton('Charger',array('class'=>'button'))?>
	<?php echo CHtml::endForm()?>
</div>
<div class="span-3">
<?php echo 'ou '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl())?>
</div>
</div>
</div>
</div>