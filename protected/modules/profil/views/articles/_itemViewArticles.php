<div class="span-17 vcard">
<div class="span-2">
<?php
echo CHtml::link(CHtml::image($data['chemin_avatar'],'Image',array(
																			'width'=>70,
																			'height'=>70,
																)),Articles::model()->getViewUrl($data['id']));
?>
</div>
<div class="span-6">
<h4>
<?php echo CHtml::link($data['titre'],Articles::model()->getViewUrl($data['id']));?>
</h4>
</div>
<div class="span-2">

<?php echo Yii::app()->dateFormatter->format("dd MMMM y, HH:mm", $data['date']);?>
<br>
</div>
<div class="span-1">

<?php echo Helper::statutViewFormat($data['actif']);?>

<br>
</div>
<div class="span-2" align="right">	
	<strong>
			<?php
					 echo CHtml::link('Effacer','#',array('submit'=>array('articles/delete'),
													   'params'=>array('id'=>$data['id']),
													   'confirm'=>'Voulez-vous vraiment supprimer cet élément ?'
								 ));
			?>
	</strong>
</div>
<div class="span-2" align="right">	
	<strong>
			<?php 
					echo CHtml::link('Modifier',Articles::model()->getUpdateUrl($data['id']));
			?>
	</strong>
</div>
</div>
