<?php $this->pageTitle=Yii::app()->name.' | '.$model->titre; ?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox');?>
</div>
<div class="span-17">
<h2>Modifier</h2>
</div>
<div class="span-17">
<?php echo CHtml::link('Ajouter une image',Articles::model()->getUploadImageUrl($model->id),array('class'=>'button'));?>
</div>

<div class="span-17">
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
</div>