<?php $this->pageTitle=Yii::app()->name.' | '.$model->titre; ?>
<div class="span-23">

<?php if(Yii::app()->user->hasFlash('publish')):?>
    <div class="flash-success span-15">
        <?php echo Yii::app()->user->getFlash('publish'); ?>
    </div>
<?php endif; ?>

<div class="span-16">
<div class="span-16">
<div class="span-13" align="right">
<?php
		if(Yii::app()->user->getID()==$model->b_membres_id)
		{
			if($model->actif==Articles::STATUS_DRAFT)
				echo CHtml::link('Publier dans votre reseau',Articles::model()->getPublishUrl($model->id),array('class'=>'button'));
			
			if($model->actif==Articles::STATUS_PUBLISHED)
				echo CHtml::link('Restaurer dans brouillons',Articles::model()->getunpublishUrl($model->id),array('class'=>'button'));
		}
 ?>
</div>
<div class="span-2" align="right">
<?php
		if(Yii::app()->user->getID()==$model->b_membres_id)
			echo CHtml::link('Modifier',Articles::model()->getUpdateUrl($model->id),array('class'=>'button'));
 
 ?>
</div>
</div>
<div class="span-15">
<h1><?php echo $model->titre;?></h1>
</div>
<div class="span-5"> Publi&eacute; le <?php echo Yii::app()->dateFormatter->format("dd MMMM y, HH:mm", $model->date)?></div>
<div class="fb-like span-10" align="right" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-action="recommend" data-font="arial">
</div>
<div class="span-15 doc">
<div class="span-5" align="center">
<?php 
	
		if($model->chemin_avatar=="")
		{
			if(Yii::app()->user->getID()==$model->b_membres_id)
				echo CHtml::link('Ajouter une image',Articles::model()->getUploadImageUrl($model->id),array('class'=>'button'));
		}
		else
			echo CHtml::image($model->chemin_avatar,'Image',array('width'=>190,'height'=>200));
	
?>
</div>
<?php 
		echo $model->contenu;
?>

<div class="span-10">
<br>
<br>
<?php echo Membres::model()->getViewID($author['id'], $author['chemin_avatar'], $author['nom'], $author['prenom'], $author['domaine_activite']);?>
</div>
<div class="span-15">
<br>

<?php
if($model->actif==Articles::STATUS_PUBLISHED)	
	echo '<h2>les commentaires</h2>
	  	  <div class="fb-comments" data-width="470" data-num-posts="10" data-colorscheme="light">
	   	  <fb:comments href="http://www.branchew.com/'.Articles::model()->getViewUrl($model->id).'" num_posts="10" width="500" style="padding-top: 20px; margin-top: 20px; border-top: 1px dotted grey;">
	  	  </fb:comments>
          </div>';
?>
</div>
</div>
</div>
<?php
		if(!Yii::app()->user->isGuest)
			$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
<!-- end right menu -->
</div>