<div class="span-15 vcard">
<div class="span-2">
<?php
echo CHtml::link(CHtml::image($data['chemin_avatar'],'Image',array(
																			'width'=>70,
																			'height'=>70,
																)),Articles::model()->getViewUrl($data['id']));
?>
</div>
<div class="span-8">
<h4>
<?php echo CHtml::link($data['titre'],Articles::model()->getViewUrl($data['id']));?>
</h4>
</div>
<div class="span-4 note">

<?php echo 'Publi&eacute;  le '.Helper::dateViewFormat($data['date']);?>

</div>
</div>
