<?php $this->pageTitle=Yii::app()->name.' | Mes articles';?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox');?>
</div>
<div class="span-17">
<h2>Mes articles</h2>
</div>
<div class="span-17">
<?php 

	$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$dataProvider,
   					  'itemView'=>'_itemViewArticles',   // refers to the partial view named '_itemViewMessages'
   
				 ));
					 


?>
</div>
</div>