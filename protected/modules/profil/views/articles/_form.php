<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'articles-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="span-15">
		<?php echo $form->labelEx($model,'titre'); ?>
		<?php echo $form->textField($model,'titre',array('size'=>30,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'titre'); ?>
	</div>

	<div class="span-15">
		
	</div>

	<div class="span-20">
		<?php echo $form->labelEx($model,'contenu'); ?>
		<div class="span-15">
		<textarea id="contenu" name="contenu" ><?php echo $model->contenu;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('contenu',
				{
					toolbar : 'Basic'
				} );
			</script>	
	</div>
		<?php echo $form->error($model,'contenu'); ?>
	</div>
	<div class="span-18" align="right">
	
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Créer' : 'Sauvegarder',array('class'=>'button')); ?>
	<?php echo ' ou '.CHtml::link('Annuler',Yii::app()->createUrl('profil/articles/viewArticles',array('id'=>Yii::app()->user->getID())));?>
	</div>
	

<?php $this->endWidget(); ?>

</div><!-- form -->