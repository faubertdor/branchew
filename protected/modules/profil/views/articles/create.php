<?php $this->pageTitle=Yii::app()->name.' | Publier un article'; ?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox');?>
</div>
<div class="span-17">
<h2>Prenez la parole et publiez un article</h2>
</div>
<div class="span-17">
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
</div>