<div class="span-16 vcard" align="left">
<div class="span-7" align="left">
<?php 
	
	echo Membres::model()->getViewID($data['membres_id'], $data['chemin_avatar'], $data['nom'], $data['prenom'], $data['domaine_activite']);

?>
</div>

<div class="span-5" align="left">

<?php
	echo $data['message'];
?>

</div>
<div class="span-2" align="left">

<?php echo CHtml::link('Confirmer',Recommandations::model()->getConfirmUrl($data['recommandations_id']));?>		  

</div>
<div class="span-1" align="left">

<?php echo CHtml::link('Effacer','#',array('submit'=>array('recommandations/delete'),
										   'params'=>array('id'=>$data['recommandations_id']),
										   'confirm'=>'Voulez-vous vraiment supprimer cet élément?'));
?>	
</div>
</div>
