<?php $this->pageTitle=Yii::app()->name.' | Recommandations';?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox',array('demandesContacts'=>$demandesContacts,'nouveauMessages'=>$nouveauMessages,'recommandationRequest'=>$recommandationRequest))?>
</div>
<div class="span-17" align="left">
<h2>Recommandations</h2>
</div>
<div class="span-17" align="left">

<?php 

		$this->widget('CTabView',		array('tabs'=>array(
												'demandes'=>array(
																	'title'=>'Demandes reçus',
																	'view'=>'_demandes',
																	'data'=>array('demandes'=>$demandes),
																 ),
												'en_attentes'=>array(
																	'title'=>'Demandes à confirmer',
																	'view'=>'_en_attentes',
																 	'data'=>array('en_attentes'=>$en_attentes),
																 ),
											)));
		
?>	
					 
</div>
</div>