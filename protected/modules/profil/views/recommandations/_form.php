<?php $this->pageTitle=Yii::app()->name.' | Recommander';?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.profil.views.pages.menu_inbox')?>
</div>

<div class="span-17">
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recommandations-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('cols'=>50,'rows'=>8));?>
		<?php echo $form->error($model,'message'); ?>
	</div>
	<div class="span-12" align="right">
	<div class="row buttons">
		<?php 
				echo CHtml::submitButton('Envoyer');
				echo ' ou '.CHtml::link('Annuler',$this->createUrl('recommandations/received#demandes'));
		?>
	</div>
	</div>
<?php $this->endWidget(); ?>
</div>
</div>
</div>