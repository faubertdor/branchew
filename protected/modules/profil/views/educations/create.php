<?php $this->pageTitle=Yii::app()->name.' | Ajouter une formation'?>
<div class="span-23">
<div class="span-16"><h2>Ajouter une formation</h2>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
<!-- end form -->

<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>