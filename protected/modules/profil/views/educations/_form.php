<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
													 'id'=>'educations-form',
											         'enableAjaxValidation'=>true,
												    ));
?>

<div class="span-16" align="left">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
</div>

<div class="span-16" align="left">
	
	<div class="span-16">
	<?php echo $form->error($model,'ecole'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'ecole'); ?>
		<?php echo $form->textField($model,'ecole',array('size'=>30,'maxlength'=>60)); ?>
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'domaine_etude'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'domaine_etude'); ?>
		<?php echo $form->textField($model,'domaine_etude',array('size'=>30,'maxlength'=>60)); ?>
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'diplome'); ?>
	</div>

	<div class="span-16">
		<?php echo $form->labelEx($model,'diplome'); ?>
		<?php echo $form->dropDownList($model,'diplome',array('Certification'=>'Certification','Licence'=>'Licence','Maitrise'=>'Maitrise','Doctorat'=>'Doctorat')); ?>
		
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'date_debut'); ?>
	</div>
	<div class="span-16">
	<?php echo $form->labelEx($model,'date_debut'); ?>
	<?php
			  echo $form->dropDownList($model,'moisDebut',Helper::date('mois'),array('prompt'=>'Mois :')); 
			   echo ' - ';
			  echo $form->dropDownList($model,'anneeDebut',Helper::date('annee'),array('prompt'=>'Année :')); 
	?>
	</div>

	<div class="span-16">
	<?php echo $form->error($model,'competence_specifique'); ?>
	</div>
	<div class="span-3">
		<?php echo $form->labelEx($model,'competence_specifique'); ?>
	</div>
	<div class="span-10">
		<textarea id="competence_specifique" name="competence_specifique" ><?php echo $model->competence_specifique;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('competence_specifique',
				{
					toolbar : 'Basic'
				} );
			</script>
		
	</div>
	
	<div class="span-16">
	<?php echo $form->error($model,'date_fin'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'date_fin'); ?>
		<?php 
			  echo $form->dropDownList($model,'moisFin',Helper::date('mois'),array('prompt'=>'Mois :')); 
			  echo ' - ';
			  echo $form->dropDownList($model,'anneeFin',Helper::date('annee'),array('prompt'=>'Année :'));
		?>
	</div>
	
	<div class="span-13" align="right">
		<?php $url=$this->createUrl('default/update',array());
		echo CHtml::submitButton($model->isNewRecord ? 'Créer' : 'Sauvegarder',array('class'=>'button')); echo '  ou  '.CHtml::link('Annuler',$url);?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div>