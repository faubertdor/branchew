<div class="container" align="left">

<?php 

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'message',
    'options'=>array(
        'title'=>'Nouveau message',
        'width'=>'350',
        'height'=>'auto',
        'autoOpen'=>false,
        'resizable'=>false,
        'modal'=>true,
        'overlay'=>array(
            'backgroundColor'=>'#000',
            'opacity'=>'0.5'
        ),
     //   'buttons'=>array(
            //'OK'=>'js:function(){alert("OK");}',
            //'Cancel'=>'js:function(){$(this).dialog("close");}',    
     //   ),
    ),
));

?>
<div class="span-5">
<div class="form" align="left">

<?php
		 $form=$this->beginWidget('CActiveForm', array(
														'id'=>'messages-form',
														'enableAjaxValidation'=>true
							  						 ));
?>
	<?php echo $form->errorSummary($model); ?>

	<div class="span-4">
		<?php echo $form->labelEx($model,'objet'); ?>
		<?php echo $form->textField($model,'objet',array('size'=>20,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'objet'); ?>
	</div>

	<div class="span-4">
		<?php echo $form->labelEx($model,'contenu'); ?>
		<?php echo $form->textArea($model,'contenu',array('rows'=>6,'cols'=>30))?>
		<?php echo $form->error($model,'contenu'); ?>
	</div>


	<div class="span-5">
		 <?php 
		 
		 	  echo CHtml::ajaxSubmitButton('Envoyer',CHtml::normalizeUrl(array('messages/composeProfile','render'=>false)),array('success'=>'js: function(data) {
                        $("#message").dialog("close");
                    }'),array('id'=>'closeMessageDialog')); 
		  ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

				 				  
<?php 				  
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

</div>