<?php $this->pageTitle=Yii::app()->name.' |  Recherche avancee';?>
<div class="span-23" >
<div class="span-16">
<h2>Recherche avanc&eacute;e</h2>
<div class="span-16">
<div class="wide form" align="left">

<?php 
	$form=$this->beginWidget('CActiveForm', array(
													'action'=>Yii::app()->createUrl($this->route),
													'method'=>'get',
							)); 
?>

	<div class="row">
		<?php echo $form->label($model,'nom'); ?>
		<?php echo $form->textField($model,'nom',array('size'=>25,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prenom'); ?>
		<?php echo $form->textField($model,'prenom',array('size'=>25,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domaine_activite'); ?>
		<?php echo $form->dropDownList($model,'domaine_activite',DomaineActivites::loadItems(),array('prompt'=>'Selectionnez:')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>25,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pays'); ?>
		<?php echo $form->dropDownList($model,'pays',
									   Pays::loadItems(),
									  	 array(	'prompt'=>'Selectionnez un pays:',
												'ajax' => array(
												'type'=>'POST', //request type
												'url'=>Controller::createUrl('membres/requestCities'), //url to call.
												
												'update'=>'#'.CHtml::activeId($model,'ville') //selector to update
												
										))); 
		?>
		
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'ville');?>
		<?php echo $form->dropDownList($model,'ville',array('prompt'=>'Selectionnez un pays:')); ?>		
	</div>
	
	
	<div class="row buttons">
		<?php 
				echo CHtml::submitButton('Rechercher',array('class'=>'button')); 
				echo ' ou '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl(Yii::app()->user->Id));
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div>
</div>
<?php $this->renderPartial('_rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>