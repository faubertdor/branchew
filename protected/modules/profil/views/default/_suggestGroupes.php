<div id="infobox" class="span-7" align="left">
<div class="title"><strong>Participer</strong></div>
<div class="body span-6">
<?php 
if($suggestGroups!=array())
foreach ($suggestGroups as $value)
	 	 {
			$groupe=Groupes::model()->getInfo($value);
?>
<div class="span-7 vcard">
			<?php echo Groupes::model()->getViewID($groupe['id'], $groupe['chemin_avatar'], $groupe['nom'], $groupe['domaine']);?>
</div>
<?php 
	  	 }
?>	
</div>
<div class="span-7 element">
<?php echo CHtml::link('Rejoindre un groupe',Groupes::model()->getIndexUrl());?>
</div>
<div class="span-7 element">
<?php echo CHtml::link('Inviter vos contacts',$this->createUrl('invite',array()));?>
</div>
<div class="span-7 element">
<?php echo CHtml::link('Créer un groupe',Groupes::model()->getCreateUrl());?>
</div>
</div>

