<?php $this->pageTitle=Yii::app()->name.' | Recherche'; ?>
<div class="span-23">
<div class="span-16">
<h2>Resultat(s) de votre recherche</h2>
</div>
<div class="span-16">
<?php 

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_itemView',   // refers to the partial view named '_result'
   
));

?>
</div>
</div>