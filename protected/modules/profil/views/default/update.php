<div class="span-16">
<div class="span-15">
<?php 
	$urlMembresImages=$this->createUrl('membres/uploadImage',array());
	echo '<strong>'.CHtml::link('Modifier',Membres::model()->getUpdateUrl()).'</strong> les informations personelles';
?>
<br>
<?php 
	echo '<strong>'.CHtml::link('Changer la photo',$urlMembresImages).'</strong> du profil';
?>
<br>
</div>

<div class="span-15">
<?php include_once '_id.php';?>
</div>


<div class="span-15">
<br>
<div class="span-10">
<h2>EXPERIENCES</h2>
</div>
<div class="span-10" align="right">
<?php echo CHtml::link('Ajouter',Experiences::model()->getCreateUrl(),array('class'=>'button')); ?>
</div>
<?php
	
	foreach ($experiences as $value) 
	{
			//Gestion de l'affichage de la date
			$dateDebut=Helper::dateProfileFormat($value['date_debut']);
			$dateFin=Helper::dateProfileFormat($value['date_fin']);	
	
?>


<div class="span-15" align="left">

<?php 
echo CHtml::link('Modifier',Experiences::model()->getUpdateUrl($value['id'])).
' | '.CHtml::link('Supprimer','#',array('submit'=>array('experiences/delete'),
		 								   'confirm'=>'Voulez-vous vraiment supprimer cet élément?',
		 								   'params'=>array('id'=>$value['id'])));
?>
<br>
<h3><?php echo CHtml::encode($value['entreprise']);?></h3>
</div>

<div class="span-15" align="left">
<h4><?php echo CHtml::encode($value['position']);?></h4>
</div>
<br>
<div class="span-15" align="left">
	<?php echo CHtml::encode($dateDebut).' - ';?>

	<?php
		   if($value['date_fin']===NULL or $value['date_fin']==='0000-00-00')
		   		
		   		echo 'Jusqu’à maintenant';
		   else
		   		echo CHtml::encode($dateFin);
	 ?>

</div>
<div class="span-15" align="left">
	 <?php echo CHtml::encode($value['adresse']);?>
</div>

<div class="span-15" align="left">
<br>
	 <?php echo $value['description'];?>

</div>

<br>
<?php  
	}
?>

<br>
<br>
<div class="span-15" align="left">
<br>
<div class="span-10">
<h2>EDUCATIONS</h2>
</div>
<div class="span-10" align="right">
<?php echo CHtml::link('Ajouter',Educations::model()->getCreateUrl(),array('class'=>'button'));?>
</div>
</div>

<?php
	foreach ($educations as $key=>$value) {
	
?>

<div class="span-15">
<?php 
	$dateDebut=Helper::dateProfileFormat($value['date_debut']);
	$dateFin=Helper::dateProfileFormat($value['date_fin']);		
?>
</div>
<div class="span-15" align="left">
<?php 
echo CHtml::link('Modifier',Educations::model()->getUpdateUrl($value['id'])).
' | '.CHtml::link('Supprimer','#',array('submit'=>array('educations/delete'),
		 								   'confirm'=>'Voulez-vous vraiment supprimer cet élément?',
		 								   'params'=>array('id'=>$value['id'])));
?>
<h3><?php echo CHtml::encode($value['ecole']).' ';?></h3>
</div>

<div class="span-15" align="left">
<h4><?php echo CHtml::encode($value['diplome']).' en '.CHtml::encode($value['domaine_etude']);?></h4>
</div>

<div class="span-15" align="left">
	<?php echo CHtml::encode($dateDebut).' - ';?>

	<?php
		   if($value['date_fin']===NULL or $value['date_fin']==='0000-00-00')
		   		
		   		echo 'jusqu’à maintenant';
		   else
		   		echo CHtml::encode($dateFin);
	 ?>
</div>

<div class="span-15" align="left">
<?php echo $value['competence_specifique'];?>
</div>


<br>

<?php  
	}
?>
<br>
<br>

<div class="span-15" align="left">
<br>
<div class="span-10">
<h2>INFORMATIONS ADDITIONNELLES </h2>
</div>
<div class="span-10" align="right">
<?php echo CHtml::link('Modifier',$this->createUrl('membres/update',array('id'=>$membre['id'])),array('class'=>'button'));?>
</div>
</div>
<br>

<div class="span-15" align="left">
<div class="span-3">
<strong>Pays</strong>
</div>
<div class="span-10" align="left">
<?php echo CHtml::encode(Pays::itemByInteger($membre['pays']));?>
</div>
</div>


<div class="span-15" align="left">
<div class="span-3">
<strong>Ville</strong>
</div>
<div class="span-10" align="left">
<?php echo CHtml::encode(Villes::itemByInteger($membre['ville']));?>
</div>
</div>



<div class="span-15" align="left">
<div class="span-3">
<strong>Site web</strong>
</div>
<div class="span-10" align="left">
<?php 
	
	if($membre['siteweb_entreprise']==='' or $membre['siteweb_entreprise']===null)
		echo CHtml::link('Ajouter',Membres::model()->getUpdateUrl());
	else 
		echo CHtml::encode($membre['siteweb_entreprise']);

?>
</div>
</div>



<div class="span-15" align="left">
<div class="span-3">
<strong>T&eacute;l&eacute;phone</strong>
</div>
<div class="span-10" align="left">
<?php 
	
	if($membre['telephone']==='' or $membre['telephone']===null)
		echo CHtml::link('Ajouter',Membres::model()->getUpdateUrl());
	else 
		echo CHtml::encode($membre['telephone']);

?>
</div>
</div>



<div class="span-15" align="left">
<div class="span-3">
<strong>Autres</strong>
</div>
<div class="span-10" align="left">
<?php 
	
	if($membre['info_additionel']==='' or $membre['info_additionel']===null)
		echo CHtml::link('Ajouter',Membres::model()->getUpdateUrl());
	else 
		echo $membre['info_additionel'];

?>
</div>
</div>



<div class="span-10">
<br>
<?php if($membre['id']==Yii::app()->user->getID())
	
			echo '<h2>MES RECOMMANDATIONS</h2>';
		else 	
			echo '<h2>RECOMMANDATIONS</h2>';

?>
<div class="span-15" align="left">
<?php 
	
	if($recommandations!=array()){

		foreach ($recommandations as $value){
?>
	
<div class="span-6" align="left">
<?php echo Membres::model()->getViewID($value['destinataire_id'], $value['chemin_avatar'], $value['nom'], $value['prenom'], $value['domaine_activite']);?>
</div>

<div class="span-8" align="left">
<?php echo $value['message'];?>
</div>
<div class="span-2" align="left">
	<?php echo CHtml::link('Effacer',"",array('submit'=>array('recommandations/deleteConfirm'),
											  'confirm'=>'Voulez-vous vraiment supprimer cet élément?',
											  'params'=>array('id'=>$value['recommandations_id'])));
	?>
</div>
<hr>
<?php 
		
		}
	}
	else 
		echo 'Aucunes'
?>
</div>
</div>
</div>
</div>
<?php $this->renderPartial('_rightMenu',array('id'=>$membre['id'],'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>