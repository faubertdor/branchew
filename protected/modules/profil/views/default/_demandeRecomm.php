<?php

	$etatContact=ContactMembres::model()->etatDemande($membre['id']);
		
		if($etatContact!=array()){
			
			if($etatContact['etat']==='accepte'){
				
				$etatRecommandation=Recommandations::etatRecommandations($membre['id']);
					
					if($etatRecommandation===array())
						echo CHtml::ajaxLink('Demander une recommandation', Recommandations::model()->getRequestUrl($membre['id']),
											    array('update'=>'#demande-recomm'),array('confirm'=>'Voulez-vous vraiment envoyer une demande de recommandation à ce contact ?'));
					else 
						if($etatRecommandation['etat']==='envoye')
							echo 'Demande de recommandation envoyée.';
						else
							if($etatRecommandation['etat']==='refuse') 
								echo 'Demande de recommandation refusée.';
			}
		}
			
