<?php $this->pageTitle=Yii::app()->name;?>
<div class="span-23">
<div class="span-16">
<div class="span-5" align="left">
				<?php echo CHtml::link('Publier un article',Articles::model()->getCreateUrl(),array('class'=>'button'));?>		

<br>
<br>
</div>
<div class="span-10" align="right">
<h5>
<?php 

	if($demandesContacts==='0')
		echo CHtml::link('Contacts  ('.CHtml::encode($demandesContacts).')',ContactMembres::model()->getReceivedUrl());
	else 
		echo CHtml::link('Contacts  (<strong class="infobox">'.CHtml::encode($demandesContacts).'</strong>)',ContactMembres::model()->getReceivedUrl());
?>
 | 
<?php 
		if($nouveauMessages==='0')
			echo CHtml::link('Messages  ('.CHtml::encode($nouveauMessages).') ',Messages::model()->getReceivedUrl());
		
		else 
			echo CHtml::link('Messages  (<strong class="infobox">'.CHtml::encode($nouveauMessages).'</strong>)',Messages::model()->getReceivedUrl());
			
			
?>
 | 
<?php 

	if($recommandationRequest==='0')
		echo CHtml::link('Recommandations ('.CHtml::encode($recommandationRequest).') ',$this->createUrl('recommandations/received#demandes'));
	else 
		echo CHtml::link('Recommandations (<strong class="infobox">'.CHtml::encode($recommandationRequest).'</strong>)',$this->createUrl('recommandations/received#demandes'));
?>

</h5>
</div>
<!-- end user menu -->

<div class="span-16">
<div class="span-15 card ">
<div class="span-2">
<?php 			echo	CHtml::link(CHtml::image($membre['chemin_avatar'],$membre['prenom'].' '.$membre['nom'],array(
																			'width'=>70,
																			'height'=>75,
																		)),ContactMembres::model()->getViewUrl($membre['id']))
?>														
</div>

<div class="span-6" align="left">
<h4>
<?php 			
				echo CHtml::link(ucwords($membre['prenom']).' '.strtoupper($membre['nom']),ContactMembres::model()->getViewUrl($membre['id']));
?>
</h4>
</div>
<!-- form to create a status -->	
<div class="span-12 form">
<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'statuts-form',
														'enableAjaxValidation'=>true,
														'action'=>Statuts::model()->getCreateUrl(),
														'method'=>'POST'
													)); 
?>

	<?php echo $form->errorSummary($statut); ?>

	<div class="span-12">
		<?php echo $form->textField($statut,'statut',array('size'=>60,'maxlength'=>140)); ?>
		<?php echo $form->error($statut,'statut'); ?>
	</div>
	<div class="span-12" align="left">
		<?php echo CHtml::submitButton($statut->isNewRecord ? 'Publier mon statut' : 'Sauvegarder',array('class'=>'button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->			
</div>
</div>




<div class="span-15">
<?php 
	
	//Membres
	//echo CHtml::link('Changer la photo',Membres::model()->getUploadImageUrl()).' du profil <br>';
	//echo CHtml::link('Modifier',Membres::model()->getUpdateUrl()).' les informations personelles';
	
?>
</div>
<!-- end user ID -->

<div class="span-15">
<?php	
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$articlesFeed,
   					  'itemView'=>'_itemViewNews', 
					  'sortableAttributes'=>array('date')
   
					 ));
?>
<br>
<br>
</div>	
</div>
<!-- end index content -->

<div class="span-6">
<?php $this->renderPartial('_lastVisitors',array('id'=>Yii::app()->user->getID(),'lastVisitors'=>$lastVisitors));?>
<?php $this->renderPartial('_rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>
</div>