<?php $this->pageTitle=Yii::app()->name.' | Recherche de CV'; ?>
<div class="span-23">
<div class="span-16">
<h2>Resultat(s)</h2>
<hr>
</div>
<div class="span-16">
<?php 

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_itemViewCv',   // refers to the partial view named '_result'
   
));

?>
</div>
</div>