<?php $this->pageTitle='CV | '.strtoupper($membre['nom']).' '.ucwords($membre['prenom']);?>
<div class="span-23">
<div class="span-16">
<div class="span-15 underline-link" align="right">
<?php 

	if(Yii::app()->user->getID()!=$membre['id'])
	 echo '<strong>'.CHtml::link('Envoyer un message','#',array('submit'=>array('messages/reply'),
	 															   'params'=>array(
	 															   					'id'=>$membre['id'],
	 																				'nom'=>$membre['nom'],
	 																				'prenom'=>$membre['prenom']
	 																			   )
													  )).'</strong>';
?>
</div>


<div class="span-15">
<h3>
<?php echo strtoupper($membre['nom']).' '.ucwords($membre['prenom']);?>
</h3>
<?php 
		
	  echo Villes::itemByInteger($membre['ville']).', '.Pays::itemByInteger($membre['pays']);

	  if($contactNombre!=0)
	  	echo ' - '.CHtml::link($contactNombre.' contact(s)',$this->createUrl('contactMembres/contacts',array('id'=>$membre['id'])));
	  else 
	  	echo ' - Zero contact';
?>
</div>


<div class="span-10">
<br>
<h2>OBJECTIF</h2>
</div>
<div class="span-15">
<?php
	echo $membre['objectif']; 
?>
<br>

</div>


<div class="span-15">
<?php 
	 //Profil already formated
	 include_once '_profil.php';
?>
</div>
</div>
<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('_rightMenu',array('id'=>$membre['id'],'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
<!-- end right menu -->
</div>