<div class="span-15 newscard">
<?php
If($data['newsId']=='AR')
{
echo
'<div class="span-2" align="right">'.
CHtml::link(CHtml::image($data['m_avatar'],$data['prenom'].' '.$data['nom'],array(
																			'width'=>50,
																			'height'=>50,
																		)),ContactMembres::model()->getViewUrl($data['m_id']))
																		
.'</div><div class="span-11">

<div class="span-11"><h4>'.
CHtml::link(ucwords($data['prenom']).' '.strtoupper($data['nom']),ContactMembres::model()->getViewUrl($data['m_id']))
.'</h4></div>
<div class="span-11 formatnews"><div class="span-3" align="right">';
if($data['chemin_avatar']!="")
	echo CHtml::link(CHtml::image($data['chemin_avatar'],'Image',array(
																			'width'=>105,
																			'height'=>110,
																)),Articles::model()->getViewUrl($data['id']));
echo '</div>
<div class="span-7">
'.CHtml::link($data['titre'],Articles::model()->getViewUrl($data['id'])).
'</div>
<div class="span-7 note">
Publi&eacute;  le '.Yii::app()->dateFormatter->format("dd MMMM y, HH:mm", $data['date']).
'</div></div></div>';
}
else 
{
if($data['newsId']=='EMP')		
echo
'<div class="span-2"><h4>Offre d\'emploi</h4></div>
<div class="span-7">
<h4>'.CHtml::link($data['titre'],OffreEmp::model()->getViewUrl($data['id'])).
'</h4>
</div>
<div class="span-5 note">
date limite : '.Yii::app()->dateFormatter->format("dd MMMM y", $data['date']).
'</div>';	
else 
{
if($data['newsId']=='STA')
$this->renderPartial('application.modules.profil.views.statuts._view',array('data'=>$data));
}													
}
?>
</div>
