<?php $this->pageTitle=Yii::app()->name.' | '.ucwords($membre['prenom']).' '.strtoupper($membre['nom']);?>

<div id="demande-recomm" class="span-13 underline-link" align=left>
<?php 
	 
	  $this->renderPartial('_demandeRecomm',array('membre'=>$membre)); 
?>
</div>

<div class="span-4 card" align="center">

<?php 
if($membre['chemin_avatar']!=NUll)
{
echo CHtml::image($membre['chemin_avatar'],ucwords($membre['prenom']).' '.strtoupper($membre['nom']),array('width'=>150,'height'=>155));
}
?>
</div>

<div class="span-8" align="left">
<br>
<h3><?php echo ucwords($membre['prenom']).' '.strtoupper($membre['nom']);?></h3>
<i><?php echo Villes::itemByInteger($membre['ville']).', '.Pays::itemByInteger($membre['pays']);?></i>
<br><i><b><?php echo DomaineActivites::itemByInteger($membre['domaine_activite'])?></b></i>

<div class="span-8 underline-link" align="left">
<strong>
<?php 
	  if($membre['id']!=Yii::app()->user->getID())
	 	echo CHtml::link('Curriculum Vitae',ContactMembres::model()->getCvUrl($membre['id']));
	 else 
	 	echo CHtml::link('Mon CV',ContactMembres::model()->getCvUrl($membre['id']));
?>
</strong>
</div>

<div class="span-8 underline-link" align="left">
<strong>
<?php 
if($contactNombre>1)
echo CHtml::link($contactNombre.' contacts',$this->createUrl('contactMembres/contacts',array('id'=>$membre['id'])));
elseif($contactNombre==1)
echo Chtml::link('Un contact',$this->createUrl('contactMembres/contacts',array('id'=>$membre['id'])));
else
echo 'Z&eacute;ro contact';

?>
<br>
<?php 

echo $visiteNombre.' visite(s)';

/**
if($visiteNombre>1)
echo ' | '.$visiteNombre.' visites';
elseif($visiteNombre==1)
echo ' | Une visite';
else 
echo ' | Z&eacute;ro visite';

**/
?>
<br>
<?php 
if($membre['siteweb_entreprise']!=null)
echo CHtml::link($membre['siteweb_entreprise'],$membre['siteweb_entreprise']);
?>
</strong>
</div>
<div class="span-8 underline-link">
<?php 

	/**
	if(Yii::app()->user->getID()!=$membre['id'])
	 echo '<strong>'.CHtml::link('Envoyer un message','#',array('submit'=>array('messages/reply'),
	 															   'params'=>array(
	 															   					'id'=>$membre['id'],
	 																				'nom'=>$membre['nom'],
	 																				'prenom'=>$membre['prenom']
	 																			   )
													  )).'</strong>';
													  
	*/
?>
</div>
</div>



<div class="span-15">
<div class="span-15">
<br>
<h2>OBJECTIF</h2>
</div>

<?php
echo $membre['objectif']; 
?>
<br>

</div>