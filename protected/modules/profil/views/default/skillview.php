<?php $this->pageTitle=Yii::app()->name.' | Recherche de CV'; ?>
<div class="span-23">
<div class="span-16">
<div class="span-16">
<h2>Tapez un domaine de compétence</h2>
<hr>
</div>
<div class="span16">
<div class="span-10 search">
<?php 
  $form=$this->beginWidget('CActiveForm', array(
															'action'=>Yii::app()->createUrl('profil/default/searchCv',array()),
															'method'=>'GET',
												  )); 
	
  $this->widget('CAutoComplete', array(
											'name'=>'cvcomplete',
											'url'=>$this->createUrl('default/cvcomplete'),
											'htmlOptions'=>array('size'=>35),
											'delay'=>500,
											'max'=>20,
											'methodChain'=>".result(function(event,item){\$(\"#search\").val(item[2]);})",
										));

echo CHtml::hiddenField('search');
echo CHtml::submitButton('Rechercher');
$this->endWidget(); 
?>
</div>
</div>
<!-- end search -->
</div>

<?php $this->renderPartial('_rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
<!-- end right menu -->
</div>