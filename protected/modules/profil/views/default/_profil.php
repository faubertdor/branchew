<div class="span-16" align="left">
<div class="span-10">
<h2>EXPERIENCES</h2>
</div>
<div class="span-15" align="left">
<?php

	foreach ($experiences as $key => $value) {
?>

<?php 
	//Gestion de l'affichage de la date
	$dateDebut=Helper::dateProfileFormat($value['date_debut']);
	$dateFin=Helper::dateProfileFormat($value['date_fin']);	
?>
<div class="span-15" align="left">
<h3><?php echo CHtml::encode($value['entreprise']);?></h3>
</div>

<div class="span-15" align="left">
	<h4><?php echo CHtml::encode($value['position']);?></h4>
</div>

<div class="span-15" align="left">
	<?php echo CHtml::encode($dateDebut).' - ';?>

	<?php
		   if($value['date_fin']===NULL or $value['date_fin']==='0000-00-00')
		   		
		   		echo 'jusqu’à maintenant';
		   else
		   		echo CHtml::encode($dateFin);
	 ?>
</div>
<div class="span-15" align="left">
	 <?php echo CHtml::encode($value['adresse']);?>
</div>
<br>
<div class="span-15" align="left">
<?php echo $value['description'];?>
</div>

<?php  
	}
?>
</div>
<br>
<br>
<div class="span-10">
<h2>EDUCATIONS</h2>
</div>
<div class="span-15" align="left">
<?php

	foreach ($educations as $key => $value) {
?>

<?php 
	//Gestion de l'affichage de la date
	$dateDebut=Helper::dateProfileFormat($value['date_debut']);
	$dateFin=Helper::dateProfileFormat($value['date_fin']);	
?>
<div class="span-15" align="left">
<h3><?php echo CHtml::encode($value['ecole']);?></h3>
</div>

<div class="span-15" align="left">
<i><b><?php echo CHtml::encode($value['diplome']).' en '.CHtml::encode($value['domaine_etude']);?></b></i>
</div>

<div class="span-6" align="left">
	<?php echo CHtml::encode($dateDebut).' - ';?>

	<?php
		   if($value['date_fin']===NULL or $value['date_fin']==='0000-00-00')
		   		
		   		echo 'jusqu’à maintenant';
		   else
		   		echo CHtml::encode($dateFin);
	 ?>
</div>

<div class="span-15" align="left">
<?php echo $value['competence_specifique'];?>
</div>

<br>
<?php  
	}
?>
</div>
<br>
<br>


<div class="span-10">
<h2>INFORMATIONS ADDITIONNELLES</h2>
</div>
<div class="span-15" align="left">
<?php 
	$siteWeb=$membre['siteweb_entreprise'];
?>
<div class="span-15" align="left">
<h3>Groupes</h3>
<div class="span-8" align="left">
<?php 
	if($groupes===array())
		echo 'Aucun';
	else
		foreach($groupes as $key=>$value){
?>
<div class="span-7 vcard" align="left">
<?php echo Groupes::model()->getViewID($value['g_id'], $value['g_avatar'], $value['g_nom'], $value['g_domaine']);?>
</div>

<?php 	
	}
?>
</div>
</div>

<div class="span-15" align="left">
<br>
<h3>Entreprises</h3>
<?php 
	
	if($entreprises===array())
		echo 'Aucunes';
	else
		foreach($entreprises as $key=>$value){
?>

<div class="span-8" align="left">
	<?php 
	
		if($value['actif']==='true')
		{
	?>
	<div class="span-7 vcard">
	<?php 
		echo Entreprises::model()->getViewInfo($value['b_entreprises_id'], $value['chemin_avatar'], $value['nom'], $value['type_entreprise']);	
	?>
	</div>
	<?php 
		}
	?>
</div>

<?php 	
	}
?>
<br>
</div>

<br><br><br>
<div class="span-14" align="left">
<div class="span-8" align="left">
<br>
<h3>Autres</h3>
<?php 
	
	if($membre['info_additionel']==='' or $membre['info_additionel']===null)
	{
		echo 'Aucunes';
?>
</div>

<div class="span-10" align="left">
<?php
 
	}
	else 
		echo $membre['info_additionel'];
?>
</div>
</div>



</div>
</div>
<br>
<br>