<div class="span-16">
<div class="span-15">
<?php include_once '_id.php';?>
</div>

<div class="span-15">
<?php include_once '_profil.php';?>
</div>

<div class="span-15">
<?php include_once '_recommandations.php';?>
</div>
</div>


<?php $this->renderPartial('_rightMenu',array('id'=>$membre['id'],'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
<!-- end right menu -->
<div id="block-contact" class="span-22" align="right">
<strong class="small"><?php echo CHtml::link('Bloquer ce contact',ContactMembres::model()->getBlockUrl($membre['id']),array('confirm'=>'Voulez-vous vraiment bloquer ce contact?'));?></strong>
</div>
