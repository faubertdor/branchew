<?php

/**
 * This is the model class for table "{{recommandations}}".
 *
 * The followings are the available columns in table '{{recommandations}}':
 * @property string $id
 * @property string $expediteur_id
 * @property string $destinataire_id
 * @property string $etat
 * @property string $message
 * @property string $confirmer
 *
 * The followings are the available model relations:
 * @property Membres $expediteur
 * @property Membres $destinataire
 */
class Recommandations extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Recommandations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{recommandations}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expediteur_id, destinataire_id', 'required'),
			array('expediteur_id, destinataire_id', 'length', 'max'=>10),
			array('etat', 'length', 'max'=>7),
			array('confirmer', 'length', 'max'=>5),
			array('message', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, expediteur_id, destinataire_id, etat, message, confirmer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'expediteur_id' => 'Expediteur',
			'destinataire_id' => 'Destinataire',
			'etat' => 'Etat',
			'message' => 'Message',
			'confirmer' => 'Confirmer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('expediteur_id',$this->expediteur_id,true);
		$criteria->compare('destinataire_id',$this->destinataire_id,true);
		$criteria->compare('etat',$this->etat,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('confirmer',$this->confirmer,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Recommandations Etat
	
	public static function etatRecommandations($id){
		
		$etatRow=Yii::app()->db->createCommand()
		->select('id,expediteur_id, destinataire_id, etat')
		->from('b_recommandations')
		->where(array('and','expediteur_id='.Yii::app()->user->getID(),'destinataire_id='.$id))
		->queryRow();
		
		if($etatRow!=array())
			return $etatRow;
		else 
			return array();
	}
	
	
	//Receive Url
	public function getReceivedUrl(){
		
		return Yii::app()->createUrl('profil/recommandations/received');
	}
	//Recommandations Url
	public function getRecommandationsUrl(){
		
		return Yii::app()->createUrl('profil/recommandations/recommandations');
	}
	
	//Waiting Url
	public function getWaitingUrl(){
		
		return Yii::app()->createUrl('profil/recommandations/waiting');
	}
	
	
	//Confirm Url
	public function getConfirmUrl($id){
		
		return Yii::app()->createUrl('profil/recommandations/confirm',array('id'=>$id));
	}
			
	//Request Url
	public function getRequestUrl($id){
		
		return Yii::app()->createUrl('profil/recommandations/request',array('id'=>$id));
	}
	
	
	//Recommander un contact URL
	public function getRecommandUrl($id){
		
		
		return Yii::app()->createUrl('profil/recommandations/recommand',array('id'=>$id));
	}
	
	//Refuser une demande
	public function getDenyUrl($id){
		
		return Yii::app()->createUrl('profil/recommandations/deny',array('id'=>$id));
	}
	
	//retrieve recommandations by id of member
	public function getByMembreId($id){
		
	$recommandationsResult=Yii::app()->db->createCommand()
		->select('b_recommandations.id recommandations_id,expediteur_id,destinataire_id,etat,message,confirmer,
		          b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_recommandations')
		->where('expediteur_id='.$id.' AND etat=\'accepte\' AND confirmer=\'true\'')
		->join('b_membres','b_membres.id=b_recommandations.destinataire_id')
		->query();
		
		$recommandationsArray=$recommandationsResult->readAll();
		$recommandations=array();
		
		foreach ($recommandationsArray as $key=>$value){
			$recommandations[$key]=$value;
		}
		
		return $recommandations;
	}
	
	//count request for recommandations
	public function getRecommandationRequestCount(){
		
		return self::model()->count('destinataire_id='.Yii::app()->user->getID().' AND etat=\'envoye\'');
	}
	
	//count confirm for recommandations
	public function getRecommandationConfirmCount(){
		
		return self::model()->count('expediteur_id='.Yii::app()->user->getID().' AND etat=\'accepte\' AND confirmer=\'false\'');
	}
	
	
	
	
	
	
	
	
	
	
}