<?php
class SearchForm extends CFormModel{
	
	public $search;
	public $category;
	
	public function rules(){
		
		return array(
						array('search, category','required'),
						array('search', 'length', 'max'=>100),
				);
	}
	
 	public function attributeLabels()
	{
		return array(
	
					'search' => 'Rechercher   ',
			);
			
	}
	//Url
	
	public static function getAdvancedSearchUrl(){
		
		return Yii::app()->createUrl('profil/default/advancedSearch',array());
	}
	
	
	
}