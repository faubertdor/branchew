<?php

/**
 * This is the model class for table "{{contact_membres}}".
 *
 * The followings are the available columns in table '{{contact_membres}}':
 * @property string $id
 * @property string $expediteur_id
 * @property string $destinataire_id
 * @property string $etat
 */
class ContactMembres extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ContactMembres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contact_membres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array( 
			
		); 
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		
		return array(
		); 
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
	
	}
	
	//Query each contact by id
	public function queryContact($id){
 		
		try {
				$membreArray=Yii::app()->db->createCommand()
					->select('id,chemin_avatar,nom,prenom,domaine_activite')
					->from('b_membres')
					->where('id='.$id)
					->queryRow();//Parce que nous allons utiliser un CArrayDataProvider
					
					
		
				if($membreArray==array())
						throw new CHttpException(400,'Requête invalide.');
				else				
					return $membreArray;
				
		}
	     catch (Exception $e) {
	     	
	     	throw new CHttpException(400,'Requête invalide.');
		}
	
 	}
 	
 	public function getSuggestIndex()
 	{
 		$suggestIndex=array();
 		$suggestArray=$this->getSuggest();
 		$count=count($suggestArray);
 	
 			if($count==0)
 			{
 				return $suggestIndex;
 			}
 			else 
 			{
 				if($count==1)
 				{
 					$suggestIndex[]=$suggestArray[rand(0,$count - 1)];
 					return $suggestIndex;
 				}
 				else
 				{
 					if($count==2)
 					{
 						$val=$count/2;
 						$suggestIndex[]=$suggestArray[rand(0, $val)];
 						$suggestIndex[]=$suggestArray[rand($val + 1, $count-1)];
 						return $suggestIndex;
 				}
 				else 
 				{
 					$val=$count/3;
 					$suggestIndex[]=$suggestArray[rand(0, $val)];
 					$suggestIndex[]=$suggestArray[rand($val + 1, $val * 2)];
 					$suggestIndex[]=$suggestArray[rand(($val * 2) + 1, ($val * 3) -1)];
 					return $suggestIndex;
 				}
 				
 			}
 		
 	
 		}
 		
 			
 	}
	
	//Suggest contacts to user based on contacts in common
	public function getSuggest()
	{
		//Select the contact list for the current user
		$suggestContacts=array();
		$contactsId=$this->contacts(Yii::app()->user->getID());
		
		//Select the contacts for each contact of the current user
		foreach ($contactsId as $value) 
		{
			//The contacts
			$userIdContacts=array();
			$userIdContacts=$this->contacts($value);
	
			foreach ($userIdContacts as $suggest)
			{
				$suggestContacts[]=$suggest;
			}
			
		}		
		
		
		foreach ($contactsId as $value)
	 	{
			foreach ($suggestContacts as $key=>$id)
			 {
				if($value==$id or $id==Yii::app()->user->getID() or $this->etatDemande($id)!=array())
				{					
						unset($suggestContacts[$key]);						
				}
			}	 		
	 	}
		
	 
	 		$membres=Yii::app()->db->createCommand()
	 		->select('b_membres.id id,pays,ville')
	 		->from('b_membres')
	 		->where('b_membres.id='.Yii::app()->user->getID())
	 		->queryRow();
	 		
	 		$suggests=Yii::app()->db->createCommand()
	 		->select('b_membres.id id,pays,ville')
	 		->from('b_membres')
	 		->where('pays='.$membres['pays'].' OR ville='.$membres['ville'])
	 		->queryAll();
	 		
	 		foreach ($suggests as $value) 
	 		{
	 			if($value['id']!=Yii::app()->user->getID() and $this->etatDemande($value['id'])==array())
	 			{ 
	 				$suggestContacts[]=$value['id'];
	 			}
	 		}
	 	
	 		$suggestArray=array();
	 		
	 		foreach ($suggestContacts as $value)
	 		{
	 			$suggestArray[]=$value;
	 		}
	 	
	
		return $suggestArray;
	}
	
	//Mes contacts si $id===Yii::app()->user->getID()
	public function contacts($id){
	
		
				$contactsId=array();
 		
 				//Tout les demandes de connexion envoyee par ce membre et accepte par ses contacts
				   $contactMembresResult=Yii::app()->db->createCommand()
										->select('id,expediteur_id,destinataire_id,etat')
										->from('b_contact_membres')
										->where(array('and','expediteur_id='.$id,'etat=\'accepte\''))
										->query();
						
							$contactMembresArray=$contactMembresResult->readAll();
							
							foreach ($contactMembresArray as $value) {
								$contactsId[$value['id']]=$value['destinataire_id'];
							}
							
					$contactMembresArray=array();
					
					//Tout les demandes de connexion recu par ce membre et accepte par lui
					$contactMembresResult=Yii::app()->db->createCommand()
										->select('id,expediteur_id,destinataire_id,etat')
										->from('b_contact_membres')
										->where(array('and','destinataire_id='.$id,'etat=\'accepte\''))
										->query();
						
							$contactMembresArray=$contactMembresResult->readAll();
							
							foreach ($contactMembresArray as $value) {
									$contactsId[$value['id']]=$value['expediteur_id'];
							}
							
					return $contactsId;
	}
	
	//Si vous avez envoye ou recu une demande de connexion a la personne qui veut acceder a votre proil
	public function etatDemande($id){
		
		if(!Yii::app()->user->isGuest)
		{
			$etatArray=Yii::app()->db->createCommand()
				 ->select('expediteur_id,destinataire_id,etat')
				 ->from('b_contact_membres')
				 ->where(array('or',array('and','expediteur_id='.$id,'destinataire_id='.Yii::app()->user->getID()),
				 					array('and','expediteur_id='.Yii::app()->user->getID(),'destinataire_id='.$id)))
				 ->queryRow();
				 
			if($etatArray===false)
			return array();
			else
			return $etatArray;
		}
		else
		return array();
	}
	
	//Si cette personne est un contact
	
	public function isContact($id){
		
		$demande=$this->etatDemande($id);
		
		if($demande!=array()){
			if($demande['etat']==='accepte')
				return true;
		}
		else 
			return false;
	}
	//URL pour les contacts
	
	public function getContactsUrl($id){
		
		return Yii::app()->createUrl('profil/contactMembres/contacts',array('id'=>$id));
	}
	
	//URL for block
	
	public function getBlockUrl($id){
		
		return Yii::app()->createUrl('profil/contactMembres/block',array('id'=>$id));
	}
	
	//URL pour Chat room
	public function getChatroomUrl(){
		
		return Yii::app()->createUrl('profil/default/chatroom',array());
	}
	
	//URL pour envoyer une demande
	
	public function getCreateUrl($id){
		
		return Yii::app()->createUrl('profil/contactMembres/create',array('id'=>$id));
	}
	
	
	//Pour creer l'URL View d'un profil a partir d'un $id 
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('profil/default/view',array(			
																	'id'=>$id,
																));
	}
	//Se connecter
	
	public function getConnectUrl($id){
		
		return Yii::app()->createUrl('profil/default/connect',array('id'=>$id));
	}
	
	//URL pour index du profil
	
	public function getIndexUrl(){
		
		return Yii::app()->createUrl('profil/default/index',array());
	}
	
	//Cv url
	
	public function getCvUrl($id){
	
		return Yii::app()->createUrl('profil/default/cv',array('id'=>$id));
	}
	
	//URL pour modifier un profil
	
	public function getUpdateUrl(){
		
		return Yii::app()->createUrl('profil/default/update',array());
	}
	
	//URL pour voir un appercu d'un contact
	
	public function getReceivedUrl(){
		
		return Yii::app()->createUrl('profil/contactMembres/received',array());
	}
	
	//URL pour voir les contacts suggerer	
	public function getSuggestUrl(){
		
		return Yii::app()->createUrl('profil/contactMembres/suggest',array());
	}
	
	
	public function getDemandes($id){
		
		//Tous les demandes de contacts recu par le membre $id
		
			  $demandeContactsArray=Yii::app()->db->createCommand()
											->select('b_contact_membres.id contact_membres_id,expediteur_id,destinataire_id,etat,
													  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
											->from('b_contact_membres')
											->where(array('and','destinataire_id='.$id,'etat=\'envoye\''))
											->join('b_membres','b_membres.id=expediteur_id')
											->queryAll();
											
		return $demandeContactsArray;
	}
	
	//Calculer le nombre de demandes de contacts
	public function getDemandesCount(){
		
		return self::model()->count('destinataire_id='.Yii::app()->user->getID().' AND etat=\'envoye\'');
	}
	
	//Suggest members
	public function getSuggestMembers(){
		
		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=$this->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			 foreach ($suggestId as $value)
			 {
					$suggestContacts[$value]=$this->queryContact($value);
												
			 }
		}
		
		return $suggestContacts;	
	}
	
	
}