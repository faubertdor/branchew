<?php

/**
 * This is the model class for table "{{domaine_activites}}".
 *
 * The followings are the available columns in table '{{domaine_activites}}':
 * @property integer $id
 * @property string $domaine_activite
 */
class DomaineActivites extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return DomaineActivites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{domaine_activites}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domaine_activite',$this->domaine_activite,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Recuperer tous les domaines disponible
	public static function loadItems(){
		
		$items=array();
		$data=Yii::app()->db->createCommand()
		->select('*')
		->from('b_domaine_activites')
		->order('domaine_activite ASC')
		->query();
		
		$dataArray=$data->readAll();
		
		foreach ($dataArray as $value) {
			
			$items[$value['id']]=$value['domaine_activite'];
		}
		return $items;
	}
	
	//Recuperer un element par son code entier
	
	public static function itemByInteger($id){
		
		if($id!=NULL){
		$data=Yii::app()->db->createCommand()
		->select('*')
		->from('b_domaine_activites')
		->where('id='.$id)
		->query();
		
		$domaineArray=$data->readAll();
		
		if($domaineArray===array())
			return false;
		else
		{	
			foreach($domaineArray as $value)
				$domaine[$value['id']]=$value['domaine_activite'];
				
				return $domaine[$id];
		}
	}
	return false;
	
	}
}