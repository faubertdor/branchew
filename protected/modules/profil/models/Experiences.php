<?php

/**
 * This is the model class for table "{{experiences}}".
 *
 * The followings are the available columns in table '{{experiences}}':
 * @property string $id
 * @property string $entreprise
 * @property string $position
 * @property string $adresse
 * @property string $description
 * @property string $date_debut
 * @property string $date_fin
 * @property string $b_membres_id
 *
 * The followings are the available model relations:
 * @property Membres $bMembres
 */
class Experiences extends CActiveRecord
{
	public $anneeDebut;
	public $moisDebut;
	public $anneeFin;
	public $moisFin;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Experiences the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{experiences}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entreprise, position, adresse, description, date_debut', 'required'),
			array('entreprise, position', 'length', 'max'=>60),
			array('date_debut','validateDateDebut'),
			//array('description','length','max'=>255),
			array('anneeDebut','length','max'=>'4'),
			array('moisDebut','length','max'=>'2'),
			array('anneeFin','length','max'=>'4'),
			array('moisFin','length','max'=>'2'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entreprise, position, adresse, description, date_debut, date_fin', 'safe', 'on'=>'search'),
		);
	}

	//Valider la date de debut elle ne doit pas etre ulterieure a la date d'aujourd'hui
	
	public function validateDateDebut($attribute,$params){
		
			$now=date('Y-m');
			$now=new DateTime($now);
			$now->format('Ym');
		
		try
		  {
				$date=new DateTime($this->date_debut);
				$date->format('Ym');
			
				if($date > $now)
					$this->addError('validateDateDebut','La date de debut est ulterieure a la date d\'aujourd\'hui!');
		 }
		 catch (Exception $e) {
			
		 	$this->addError('validateDateDebut','Veuillez choisir une date debut!');
		}
			
	}
	
	//Valider la date de fin elle ne doit pas etre ulterieure a la date d'aujourd'hui
	
	public function validateDateFin($attribute,$params){
		
			$now=date('Y-m');
			$now=new DateTime($now);
			$now->format('Ym');
		
		
			try {
					$date=new DateTime($this->date_fin);
					$date->format('Ym');
					
					if($date > $now)
						$this->addError('validateDateFin','La date de fin est ulterieure a la date d\'aujourd\'hui!');
						
			} catch (Exception $e) {
				
			}				
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
			'entreprise' => 'Entreprise',
			'position' => 'Position',
			'adresse' => 'Adresse',
			'description' => 'Description',
			'date_debut' => 'Date de début',
			'date_fin' => 'Date de fin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('entreprise',$this->entreprise,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('adresse',$this->adresse,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_debut',$this->date_debut,true);
		$criteria->compare('date_fin',$this->date_fin,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);

		return new CActiveDataProvider($this, array(
														'criteria'=>$criteria,
													));
	}
	
//assignation de b_membres_id avant la sauvegarde
	protected function beforeSave(){
		
		
			if($this->isNewRecord){
				
				
				$this->b_membres_id=Yii::app()->user->getID();
				
			}
			
		return parent::beforeSave();
		
	}
	
	
	//Create Url
	
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('profil/experiences/create',array());
	}
	
	//Update Url
	
	public function getUpdateUrl($id){
		
		return Yii::app()->createUrl('profil/experiences/update',array('id'=>$id));
	}
	
	
	//Pour charger les experiences d'un membre par son id
	public function getByMembreId($id){
		
	$experiencesResult=Yii::app()->db->createCommand()
		->select('*')
		->from('b_experiences')
		->where('b_membres_id='.$id)
		->order('date_debut DESC')
		->query();
		
		$experiencesArray=$experiencesResult->readAll();
		$experiences=array();
		
		foreach ($experiencesArray as $key => $value) {
			$experiences[$key]=$value;
		}
		
		return $experiences;
	}
	
	
}
