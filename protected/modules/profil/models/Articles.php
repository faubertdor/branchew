<?php

/**
 * This is the model class for table "{{articles}}".
 *
 * The followings are the available columns in table '{{articles}}':
 * @property string $id
 * @property string $titre
 * @property string $chemin_avatar
 * @property string $contenu
 * @property string $date
 * @property string $actif
 * @property string $b_membres_id
 */
class Articles extends CActiveRecord
{
	const STATUS_DRAFT='false';
    const STATUS_PUBLISHED='true';
    const STATUS_ARCHIVED='review';
	/**
	 * Returns the static model of the specified AR class.
	 * @return Articles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titre, contenu, b_membres_id', 'required'),
			array('titre', 'length', 'max'=>100),
			array('chemin_avatar', 'length', 'max'=>60),
			 array('actif', 'in', 'range'=>array('true','false','review')),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, titre, contenu, date, actif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'titre'=>'Titre',
			'contenu' => 'Contenu',
			'date' => 'Date de publication',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('titre',$this->titre,true);
		$criteria->compare('contenu',$this->contenu,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('actif',$this->actif,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	//Get articles index
	public function getArticlesIndex(){
		
		//Array to construct data provider  with all articles in the network
		$articlesFeed=array();
		$arrayOfArray=array();
		
		//Load my contacts ID
		$contactsId=ContactMembres::model()->contacts(Yii::app()->user->getID());
		
		//Load my articles
		$arrayOfArray[]=Articles::model()->getPublishedArticles(Yii::app()->user->getID());
		
		//Load array of acticles for each contact
		foreach ($contactsId as  $value) {
			$arrayOfArray[]=Articles::model()->getPublishedArticles($value);
		}
		
		//construct a single array with all articles
		foreach ($arrayOfArray as  $value){
			
			foreach ($value as $article){
				
				$articlesFeed[]=$article;
			}
		}
		
		return $articlesFeed;
	}
	
	
	//Get articles
	public function getArticles($id)
	{
		
		 $articlesArray=Yii::app()->db->createCommand()
										->select('id,chemin_avatar,titre,b_membres_id,actif,date')
										->from('b_articles')
										->where('b_membres_id='.$id)
										->order('date DESC')
										->queryAll();					
		return $articlesArray;
	}
	
	//Get published articles
	public function getPublishedArticles($id)
	{
		
		 $articlesArray=Yii::app()->db->createCommand()
										->select('b_articles.id,b_articles.chemin_avatar,titre,b_membres_id,b_articles.actif,date,newsId,
												  b_membres.id m_id,b_membres.chemin_avatar m_avatar,nom,prenom,domaine_activite')
										->from('b_articles')
										->where(array('and','b_membres_id='.$id,'b_articles.actif=true'))
										->join('b_membres','b_membres_id=b_membres.id')
										->order('date DESC')
										->queryAll();				
		return $articlesArray;
	}
	
	//View articles URL
	public function getViewArticlesUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/viewArticles',array('id'=>$id));
	}
	
	//Create URL
	public function getCreateUrl()
	{
		return Yii::app()->createUrl('profil/articles/create',array());
	}
	
	//Publish URL
	public function getPublishUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/publish',array('id'=>$id));
	}
	
	//Unpublish URL
	public function getUnpublishUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/unPublish',array('id'=>$id));
	}
	
	//Update URL
	public function getUpdateUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/update',array('id'=>$id));
	}
	
	//View URL
	public function getViewUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/view',array('id'=>$id));
	}
	
	//Upload URL
	public function getUploadImageUrl($id)
	{
		return Yii::app()->createUrl('profil/articles/uploadImage',array('id'=>$id));
	}
}