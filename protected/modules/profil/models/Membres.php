<?php

/**
 * This is the model class for table "{{membres}}".
 *
 * The followings are the available columns in table '{{membres}}':
 * @property string $id
 * @property string $nom
 * @property string $prenom
 * @property string $chemin_avatar
 * @property string $sexe
 * @property string $date_naissance
 * @property integer $domaine_activite
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string $statut_matrimonial
 * @property string $date_inscription
 * @property string $telephone
 * @property integer $pays
 * @property string $ville
 * @property string $adresse
 * @property string $actif
 * @property string $profil_type
 * @property string $objectif
 * @property string $siteweb_entreprise
 * @property string $info_additionel
 *
 * The followings are the available model relations:
 * @property AdminEntreprises[] $adminEntreprises
 * @property AppEmp[] $appEmps
 * @property AppEmpExternes[] $appEmpExternes
 * @property AppelOffres[] $appelOffres
 * @property Commentaires[] $commentaires
 * @property ContactGroupes[] $contactGroupes
 * @property ContactMembres[] $contactMembres
 * @property ContactMembres[] $contactMembres1
 * @property Educations[] $educations
 * @property Entreprises[] $entreprises
 * @property Experiences[] $experiences
 * @property Groupes[] $groupes
 * @property Messages[] $messages
 * @property Messages[] $messages1
 * @property OffreEmp[] $offreEmps
 * @property Recommandations[] $recommandations
 * @property Recommandations[] $recommandations1
 * @property SuivreEntreprises[] $suivreEntreprises
 * @property SuperMembres[] $superMembres
 * @property VisiteMembres[] $visiteMembres
 * @property VisiteMembres[] $visiteMembres1
 */
class Membres extends CActiveRecord
{	
	public $annee;
	public $mois;
	public $jour;
	

	/**
	 * Returns the static model of the specified AR class.
	 * @return Membres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{membres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nom, prenom, email, password,pays,ville', 'required'),
			array('domaine_activite, pays,ville', 'numerical', 'integerOnly'=>true),
			array('nom, prenom, password, ville', 'length', 'max'=>60),
			array('password','length','min'=>6),
			array('email, siteweb_entreprise', 'length', 'max'=>60),
			array('objectif','length','max'=>255),
			array('info_additionel','length','max'=>255),
			array('telephone', 'length', 'max'=>15),
			array('email','email'),
			array('email','unique'),
			array('siteweb_entreprise','url'),		
			array('annee','length','max'=>'4'),
			array('mois','length','max'=>'2'),
			array('jour','length','max'=>'2'),
			array('sexe','in','range'=>array('f','m')),
			array('statut_matrimonial','in','range'=>array('1','0')),
			array('adresse,rh_expiration,profile_type', 'safe'),
		
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nom, prenom, sexe, date_naissance, domaine_activite, email, statut_matrimonial, telephone, pays, ville, adresse, objectif', 'safe', 'on'=>'search'),
		
		);
		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		
			'nom' => 'Nom',
			'prenom' => 'Prenom',
			'sexe' => 'Sexe',
			'date_naissance' => 'Date de naissance',
			'domaine_activite' => 'Domaine',
			'email' => 'Email',
			'statut_matrimonial' => 'Statut matrimonial',
			'date_inscription' => 'Date d\'inscription',
			'telephone' => 'Telephone',
			'pays' => 'Pays',
			'ville' => 'Ville',
			'adresse' => 'Adresse',
			'objectif' => 'Objectif et spécialité',
			'siteweb_entreprise' => 'Site web',
			'info_additionel' => 'Informations additionnelles',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	//Recherche
	public function searchEngine($search)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->compare('nom',$search,'OR');
		$criteria->compare('prenom',$search,'OR');
		$criteria->compare('domaine_activite',$search,'OR');
		$criteria->compare('email',$search,'OR');
		$criteria->compare('pays',$search,'OR');
		$criteria->compare('ville',$search,'OR');
		$criteria->limit=1000;

		return new CActiveDataProvider($this, array(
													 'criteria'=>$criteria,
													 'pagination'=>array(
																			'pageSize'=>10,
																		),
												   ));
	}
	//Recherche avancee
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('domaine_activite',$this->domaine_activite);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pays',$this->pays);
		$criteria->compare('ville',$this->ville,true);
		$criteria->limit=1000;

		return new CActiveDataProvider($this, array(
													 'criteria'=>$criteria,
													 'pagination'=>array(
																			'pageSize'=>10,
																		),
												   ));
	}
	
	
	//Before save
	
	protected function beforeSave(){
		
		
			if($this->isNewRecord){
				
				
				$this->date_inscription=date('Y-m-d');
				$this->profil_type='basic';
				$this->actif='false';
				$this->salt=uniqid('',true);
				$this->password=md5($this->salt.$this->password); //hash password with md5 algorithm
				
			}
			
		return parent::beforeSave();
		
	}
	
	//retourne un array avec toutes les informations d'un membre
	
	public function membreById($id){
		
	$membreArray=Yii::app()->db->createCommand()
					->select('id,nom,prenom,sexe,date_naissance,domaine_activite,email,statut_matrimonial,telephone,pays,ville,adresse,profil_type,objectif,chemin_avatar,siteweb_entreprise,info_additionel')
					->from('b_membres')
					->where('id='.$id)
					->queryRow();
					
	 return $membreArray;	
}
	
	//Retourne un array avec les informations pour un profil de type basic en dehors d'un reseau
	
	public function membreInfoById($id){
		
	$membreArray=Yii::app()->db->createCommand()
					->select('id,nom,prenom,email,domaine_activite,pays,ville,objectif,chemin_avatar,siteweb_entreprise')
					->from('b_membres')
					->where('id='.$id)
					->queryRow();
					
	 return $membreArray;
		
	}
	
	//Date d'expiration pour compte RH.
	public function isHr()
	{
		$membreArray=Yii::app()->db->createCommand()
		->select('id,rh_expiration')
		->from('b_membres')
		->where('id='.Yii::app()->user->getID())
		->queryRow();

		if($membreArray['rh_expiration']!=NULL)
		{
			$now=date('Y-m-d');
			$now=new DateTime($now);
			$now->format('Ymd');
		
		
			$rh_expiration=new DateTime($membreArray['rh_expiration']);
			$rh_expiration->format('Ymd');
		
			if($now > $rh_expiration)
				return false;
			else 
				return true;	
		}
		else
			return false;
	}
	
	//Verification du type d'un compte basic ou upgrade
	
	public function getType(){
		
		$membreArray=Yii::app()->db->createCommand()
										->select('id,profil_type')
										->from('b_membres')
										->where('id='.Yii::app()->user->getID())
										->queryRow();
		return $membreArray;
	}
	
	public function getViewIdInfo($id){
		
		$membreArray=Yii::app()->db->createCommand()
					->select('id,nom,prenom,domaine_activite,chemin_avatar,email')
					->from('b_membres')
					->where('id='.$id)
					->queryRow();
					
	 return $membreArray;	
	}
	
	
	
	//Affiche les info dans d'un membre, photo, nom, prenom, domaine et ville.
	public function getViewID($id,$avatar,$nom,$prenom,$domaine){
		
		return 
			   '<div class="span-2" align="right">'.
				CHtml::link(CHtml::image($avatar,$prenom.' '.$nom,array(
																			'width'=>60,
																			'height'=>60,
																		)),ContactMembres::model()->getViewUrl($id)).
																		
				'</div>'.
				'<div class="span-4" align="left">'.
				'<div class="span-4" align="left"><b>'.
				 CHtml::link(ucwords($prenom).' '.strtoupper($nom),ContactMembres::model()->getViewUrl($id)).
				'</b></div>'.
				'<div class="span-4" align="left"><i>'.
				 DomaineActivites::itemByInteger($domaine).
				'</i></div>'.
				'</div>';
		
	}
	
	
	
//Affiche les info dans d'un membre, photo, nom, prenom, domaine et ville avec un lien vers le CV.
	public function getViewCV($id,$nom,$prenom,$domaine){
		
		return 
				'<div class="span-5" align="left">'.
				'<div class="span-5" align="left"><br><b>'.
				 CHtml::link(strtoupper($nom).' '.ucwords($prenom),ContactMembres::model()->getCvUrl($id)).
				'</b></div>'.
				'<div class="span-5" align="left"><i>'.
				 DomaineActivites::itemByInteger($domaine).
				'</i></div>'.
				'</div>';
		
	}
	
	//Affiche les info dans d'un membre, photo, nom, prenom, domaine et ville dans une vue email
	public function getViewEmailID($id,$avatar,$nom,$prenom,$domaine){
		
		$urlRoot='www.branchew.com';
		return '<div class="row" align="left"><nobr>'.
			   '<div class="column" align="left">'.
				CHtml::link(CHtml::image($urlRoot.$avatar,$nom.' '.$prenom,array(
																			'width'=>70,
																			'height'=>75,
																		)),$urlRoot.ContactMembres::model()->getViewUrl($id)).
																		
				'</div>'.
				'<div class="column" align="left">'.
				'<div class="row" align="left"><b>'.
				 CHtml::link(strtoupper($nom).' '.ucwords($prenom),$urlRoot.ContactMembres::model()->getViewUrl($id)).
				'</b></div>'.
				'<div class="row" align="left">'.
				 DomaineActivites::itemByInteger($domaine).
				'</div>'.
				'</div>'.
				'</nobr></div>';
		
	}
	//Update url
	
	public function getUpdateUrl(){
		
		return Yii::app()->createUrl('profil/membres/update',array());
	}
	
	//Update url
	
	public function getUploadImageUrl(){
		
		return Yii::app()->createUrl('profil/membres/uploadImage',array());
	}
}
