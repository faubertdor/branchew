<?php

/**
 * This is the model class for table "{{statuts}}".
 *
 * The followings are the available columns in table '{{statuts}}':
 * @property string $id
 * @property string $statut
 * @property string $date
 * @property string $newsId
 * @property string $b_membres_id
 */
class Statuts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Statuts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{statuts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('statut, b_membres_id', 'required'),
			array('statut', 'length', 'max'=>140),
			array('b_membres_id', 'length', 'max'=>10),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, statut, date, newsId, b_membres_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'statut' => 'Statut',
			'date' => 'Date de publication',
		);
	}
	
	//Get statuts index
	public function getStatutsIndex(){
		
		//Array to construct data provider  with all articles in the network
		$statutsFeed=array();
		$arrayOfArray=array();
		
		//Load my contacts ID
		$contactsId=ContactMembres::model()->contacts(Yii::app()->user->getID());
		
		//Load my statuts
		$arrayOfArray[]=$this->getStatuts(Yii::app()->user->getID());
		
		//Load array of statuts for each contact
		foreach ($contactsId as  $value) {
			$arrayOfArray[]=$this->getStatuts($value);
		}
		
		//construct a single array with all statuts
		foreach ($arrayOfArray as  $value){
			
			foreach ($value as $statut){
				
				$statutsFeed[]=$statut;
			}
		}
		
		return $statutsFeed;
	}
	
	//Get statuts
	public function getStatuts($id)
	{
		
		 $statutsArray=Yii::app()->db->createCommand()
										->select('b_statuts.id,statut,b_membres_id,date,newsId,
												  b_membres.id m_id,b_membres.chemin_avatar m_avatar,nom,prenom,domaine_activite')
										->from('b_statuts')
										->where('b_membres_id='.$id)
										->join('b_membres','b_membres_id=b_membres.id')
										->order('date DESC')
										->queryAll();				
		return $statutsArray;
	}
	
	//Create URL
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('profil/statuts/create',array());
	}
	//View URL
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('profil/statuts/view',array('id'=>$id));
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('statut',$this->statut,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('newsId',$this->newsId,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}