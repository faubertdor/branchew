<?php

/**
 * This is the model class for table "{{messages}}".
 *
 * The followings are the available columns in table '{{messages}}':
 * @property string $id
 * @property string $expediteur_id
 * @property string $destinataire_id
 * @property string $objet
 * @property string $contenu
 * @property string $date_envoie
 * @property string $date_lecture
 *
 * The followings are the available model relations:
 * @property Membres $expediteur
 * @property Membres $destinataire
 */
class Messages extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Messages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{messages}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('objet,contenu,destinataire_id', 'required'),
			array('objet', 'length', 'max'=>60),
			array('contenu', 'length', 'max'=>255),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, expediteur_id, destinataire_id, objet, contenu, date_envoie, date_lecture', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
					);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(

			'objet' => 'Objet',
			'contenu' => 'Message',
			'date_envoie' => 'Date d\'envoie',
			'date_lecture' => 'Date de lecture',
			'destinataire_id'=>'&Agrave;'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('expediteur_id',$this->expediteur_id,true);
		$criteria->compare('destinataire_id',$this->destinataire_id,true);
		$criteria->compare('objet',$this->objet,true);
		$criteria->compare('contenu',$this->contenu,true);
		$criteria->compare('date_envoie',$this->date_envoie,true);
		$criteria->compare('date_lecture',$this->date_lecture,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	//Charger les messages envoyees
	public function loadSent(){
		
		$messagesArray=Yii::app()->db->createCommand()
					   ->select('b_messages.id messages_id,expediteur_id,destinataire_id,objet,date_envoie,date_lecture,
					   			 b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
					   ->from('b_messages')
					   ->where('expediteur_id='.Yii::app()->user->getID())
					   ->join('b_membres','destinataire_id=b_membres.id')
					   ->order('date_envoie DESC')
					   ->queryAll();
		
		$dataProvider = new CArrayDataProvider($messagesArray, array(																	
																		'keyField'=>'messages_id',
																		'pagination'=>array(
								 									    					  'pageSize'=>10,
																						    ),
																	));
		return $dataProvider;			
	}
	
	
	
	//Charger les messages recus.
	public function loadReceived(){
		
		$messagesArray=Yii::app()->db->createCommand()
					   ->select('b_messages.id messages_id,expediteur_id,destinataire_id,objet,date_envoie,date_lecture,
					   			 b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
					   ->from('b_messages')
					   ->where('destinataire_id='.Yii::app()->user->getID())
					   ->join('b_membres','expediteur_id=b_membres.id')
					   ->order('date_envoie DESC')
					   ->queryAll();
		
				$dataProvider = new CArrayDataProvider($messagesArray, array(
																				'keyField'=>'messages_id',
																				'pagination'=>array(
								 									    					 		 'pageSize'=>10,
																									),																	));
		 return $dataProvider;
	}
	
	//Messages between two people with the same subject is a conversation
	public function loadConversation($id,$objet)
	{
		$conversationArray=Yii::app()->db->createCommand()
					   ->select('b_messages.id messages_id,expediteur_id,destinataire_id,objet,contenu,date_envoie,date_lecture,
					   			 b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
					   ->from('b_messages')
					   ->where(array('or',array('and','destinataire_id='.Yii::app()->user->getID(),'expediteur_id='.$id,'b_messages.objet= "'.$objet.'"'),array('and','expediteur_id='.Yii::app()->user->getID(),'destinataire_id='.$id,'b_messages.objet="'.$objet.'"')))
					   ->join('b_membres','expediteur_id=b_membres.id')
					   ->order('date_envoie ASC')
					   ->queryAll();
		
		return $conversationArray;	
	}
	
	//Receive Url
	public function getReceivedUrl(){
		
		return Yii::app()->createUrl('profil/messages/received',array());
	}
	
	//Compose Url
	
	public function getComposeUrl(){
		
		return Yii::app()->createUrl('profil/messages/compose',array());
	}
	

	//ComposeProfile Url
	
	public function getComposeProfileUrl(){
		
		return Yii::app()->createUrl('profil/messages/composeProfile',array());
	}
	
	//Read Url
	public function getReadUrl($id){
		
		return Yii::app()->createUrl('profil/messages/read',array('id'=>$id));
	}
	
	
	//Sent Url
	public function getSentUrl(){
		
		return Yii::app()->createUrl('profil/messages/sent',array());
	}
	
	//the number of new messages
	public function getNewMessagesCount(){
		
		return self::model()->count('destinataire_id='.Yii::app()->user->getID().' AND date_lecture is null');
	}
	
}