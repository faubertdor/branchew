<?php

/**
 * This is the model class for table "{{visite_membres}}".
 *
 * The followings are the available columns in table '{{visite_membres}}':
 * @property string $id
 * @property string $b_membres_id
 * @property string $b_visiteurs_id
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Membres $bMembres
 * @property Membres $bVisiteurs
 */
class VisiteMembres extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return VisiteMembres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{visite_membres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}
	
	/**
	 * 
	 * Return the last 3 visitors of my profile
	 */
	public function getLastVistors()
	{
		$lastVisitorsArray=Yii::app()->db->createCommand()
		->select('b_membres_id,b_visiteurs_id,date,b_membres.id id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_visite_membres')
		->where('b_membres_id='.Yii::app()->user->getId())
		->join('b_membres','b_visiteurs_id=b_membres.id')
		->order('date DESC')
		->limit(3)
		->queryAll();
		
		return $lastVisitorsArray;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{ 
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);
		$criteria->compare('b_visiteurs_id',$this->b_visiteurs_id,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	//Url pour les visites
	
	public function getViewUrl(){
		
		return Yii::app()->createUrl('profil/visiteMembres/view',array());
	}
}