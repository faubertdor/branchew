<?php

/**
 * This is the model class for table "{{educations}}".
 *
 * The followings are the available columns in table '{{educations}}':
 * @property string $id
 * @property string $ecole
 * @property string $diplome
 * @property string $domaine_etude
 * @property string $date_debut
 * @property string $date_fin
 * @property string $competence_specifique
 * @property string $b_membres_id
 * @property string $chemin_diplome
 *
 * The followings are the available model relations:
 * @property Membres $bMembres
 */
class Educations extends CActiveRecord
{
	
	public $anneeDebut;
	public $moisDebut;
	public $anneeFin;
	public $moisFin;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Educations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{educations}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ecole, diplome, domaine_etude, date_debut, competence_specifique', 'required'),
			array('ecole, domaine_etude', 'length', 'max'=>60),
			array('diplome', 'length', 'max'=>15),
			array('date_fin', 'safe'),
			array('date_debut','validateDateDebut'),
			array('competence_specifique','length','max'=>255),
			array('anneeDebut','length','max'=>'4'),
			array('moisDebut','length','max'=>'2'),
			array('anneeFin','length','max'=>'4'),
			array('moisFin','length','max'=>'2'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ecole, diplome, domaine_etude, date_debut, date_fin, competence_specifique', 'safe', 'on'=>'search'),
		);
	}

		
	//Valider la date de debut elle ne doit pas etre ulterieure a la date d'aujourd'hui
	
	public function validateDateDebut($attribute,$params){
		
			$now=date('Y-m');
			$now=new DateTime($now);
			$now->format('Ym');
		
		try 
		{
			$date=new DateTime($this->date_debut);
			$date->format('Ym');
			
			if($date > $now)
				$this->addError('validateDateDebut','La date de debut est ulterieure a la date d\'aujourd\'hui!');
		}
		 catch (Exception $e) {
			
		 	$this->addError('validateDateDebut','Veuillez choisir une date debut!');
		}
	}
	
	//Valider la date de fin elle ne doit pas etre ulterieure a la date d'aujourd'hui
	
	public function validateDateFin($attribute,$params){
		
			$now=date('Y-m');
			$now=new DateTime($now);
			$now->format('Ym');
		
		
			try {
					$date=new DateTime($this->date_fin);
					$date->format('Ym');
					
					if($date > $now)
						$this->addError('validateDateFin','La date de fin est ulterieure a la date d\'aujourd\'hui!');
						
			} catch (Exception $e) {
				
			}				
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ecole' => 'Ecole',
			'diplome' => 'Diplome',
			'domaine_etude' => 'Domaine d\'etude',
			'date_debut' => 'Date de debut',
			'date_fin' => 'Date de fin',
			'competence_specifique' => 'Compétence spécifique',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ecole',$this->ecole,true);
		$criteria->compare('diplome',$this->diplome,true);
		$criteria->compare('domaine_etude',$this->domaine_etude,true);
		$criteria->compare('date_debut',$this->date_debut,true);
		$criteria->compare('date_fin',$this->date_fin,true);
		$criteria->compare('competence_specifique',$this->competence_specifique,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//assignation de b_membres_id avant la sauvegarde
	protected function beforeSave(){
		
		
			if($this->isNewRecord){
				
				
				$this->b_membres_id=Yii::app()->user->getID();
				
			}
			
		return parent::beforeSave();
		
	}
	

	
	//Create Url
	
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('profil/educations/create',array());
	}
	
	//Update Url
	
	public function getUpdateUrl($id){
		
		return Yii::app()->createUrl('profil/educations/update',array('id'=>$id));
	}
	//Delete Url
	
	
	public function getByMembreId($id){
		
	$educationsResult=Yii::app()->db->createCommand()
		->select('*')
		->from('b_educations')
		->where('b_membres_id='.$id)
		->order('date_debut DESC')
		->query();
		
		$educationsArray=$educationsResult->readAll();
		$educations=array();
		
		foreach ($educationsArray as $key => $value) {
			$educations[$key]=$value;
		}
		
		return $educations;
	}
	
	
	
	
	
	
}
