<?php
class UploadImages extends CFormModel{
	
	
	public $image;
	
	public function rules(){
		
		return array(
				
			   array(
			   		
			   		'image','file',
			   		'types'=>'jpg, gif, png',
			   		'maxSize'=>1024 * 1024 * 4,
			   ),
		);
	}
	
	
}