<?php
class SearchAction extends CAction{
	
	public $searchMode;
	
	public function run(){
		
	$controller=$this->getController();
		
	
		$model=new  SearchForm();
		$model->unsetAttributes();  // clear any default values
		$model->category='membre';
		
		if(isset($_GET['autocomplete']))
		{
			
			$model->search=$_GET['autocomplete'];
			
				
			if($model->validate())
			{
				//this part is used to remember the user choice on the search form
				$_SESSION['search']=$model->category;
		
				if($model->category==='membre')
				{
					/**
					$model->search=str_replace(" ","", $model->search);
					$membreArray=Yii::app()->db->createCommand()
						->select('id,chemin_avatar,nom,prenom,email,domaine_activite')
						->from('b_membres')
						->where(array('or',
								  			array('like','email','%'.$model->search.'%'),
								            array('like','nom','%'.$model->search),
								            array('like','prenom','%'.$model->search),
								            array('like','nom',$model->search.'%'),
								            array('like','prenom','%'.$model->search.'%'),
								))
						->queryAll();
					**/
					//Let's try the code below
					$model->search=str_replace(" ","", $model->search);
					$model->search=$model->search[0].$model->search[1].$model->search[2];
					$sql = 'SELECT id,chemin_avatar,nom,prenom,domaine_activite FROM b_membres WHERE (LCASE(nom) LIKE :term) OR (LCASE(prenom) LIKE :term)';
         			$cmd = Yii::app()->db->createCommand($sql);
         			$cmd->bindValue(":term","%".strtolower($model->search)."%", PDO::PARAM_STR);
         			$membreArray = $cmd->queryAll();
         			
					$dataProvider = new CArrayDataProvider($membreArray, array(
																		
																		'pagination'=>array(
								 									    'pageSize'=>10,
																							),
														  ));
			
				$controller->render('search',array(
												'dataProvider'=>$dataProvider,
						));
				}
				else 
				{
					if($model->category==='entreprise'){
						
						$entArray=Yii::app()->db->createCommand()
						->select('id entrepriseId,chemin_avatar,nom,type_entreprise,email,actif')
						->from('b_entreprises')
						->where(array('and',array('or',array('like','nom','%'.$model->search.'%'),
								   		  			   array('like','email','%'.$model->search.'%'),
								     ),'actif=\'true\''
								))
						->queryAll();
		
					$dataProvider = new CArrayDataProvider($entArray, array(
																		
																		'keyField'=>'entrepriseId',
																		'pagination'=>array(
								 									    'pageSize'=>10,
																							),
														));
			
				$controller->render('searchEntreprises',array(
												'dataProvider'=>$dataProvider,
						));
						
					}
					else 
						if($model->category==='appelOffres'){
							
						$appelOffresArray=Yii::app()->db->createCommand()
						->select('b_appel_offres.id appelOffresId,titre,numero, pret,introduction,b_appel_offres.description,date_fin')
						->from('b_appel_offres')
						->where(array('and',
									  array('or',array('like','titre','%'.$model->search.'%'),
								   		   array('like','numero','%'.$model->search.'%'),
								   		   array('like','pret','%'.$model->search.'%'),
								   		   array('like','introduction','%'.$model->search.'%'),
								   		   array('like','b_appel_offres.description','%'.$model->search.'%'),
								   		  
								   		   
								),
							       'b_appel_offres.actif=\'true\''))
				
						->queryAll();
		
					$dataProvider = new CArrayDataProvider($appelOffresArray, array(
																		
																		'keyField'=>'appelOffresId',
																		'pagination'=>array(
								 									    'pageSize'=>10,
																							),
														  ));
			
					$controller->render('searchAppelOffres',array(
																	'dataProvider'=>$dataProvider,
										));
							
						}
						else 
							if($model->category==='offreEmploi')
							{
								
								
								$offreEmploiArray=Yii::app()->db->createCommand()
								->select('b_offre_emp.id offreEmpId,titre,type, description,description_entreprise,qualification,domaine_activite,actif,date_fin')
								->from('b_offre_emp')
								->where(array('and',
									  array('or',array('like','titre','%'.$model->search.'%'),
								   		   array('like','type','%'.$model->search.'%'),
								   		   array('like','description','%'.$model->search.'%'),
								   		   array('like','description_entreprise','%'.$model->search.'%'),
								   		   array('like','qualification','%'.$model->search.'%'),
								   		  
								   		   
								),
							       'b_offre_emp.actif=\'true\''))
				
								->queryAll();
		
								$dataProvider = new CArrayDataProvider($offreEmploiArray, array(
																		
																		'keyField'=>'offreEmpId',
																		'pagination'=>array(
								 									    						'pageSize'=>10,
																							),
																	   ));
			
								$controller->render('searchOffreEmp',array(
																			 'dataProvider'=>$dataProvider,
																		  ));
								
								
								
							}
							else{
									if($model->category==='groupe')
									{
										$groupeArray=Yii::app()->db->createCommand()
													->select('id goupe_id,chemin_avatar,nom,domaine,description')
													->from('b_groupes')
													->where(array('or',array('like','nom','%'.$model->search.'%'),
								 									   array('like','description','%'.$model->search.'%'),
								 							 ))
													->queryAll();
		
										$dataProvider = new CArrayDataProvider($groupeArray, array(
																		
																									'keyField'=>'goupe_id',
																									'pagination'=>array(
								 									    										 			'pageSize'=>10,
																														),
																								   ));
			
										$controller->render('searchGroupes',array(
																					'dataProvider'=>$dataProvider,
														 						 ));
						
									}
								
							}
				}
					 
			}
			else 
				$controller->redirect(ContactMembres::model()->getUpdateUrl());
		  }
		  else 
		  	 $controller->redirect(ContactMembres::model()->getIndexUrl());
		}
		
 }
