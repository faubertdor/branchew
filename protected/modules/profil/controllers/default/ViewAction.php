<?php

class ViewAction extends CAction{
	
	// La methode qui sera executee par defaut quand on fait appel a la classe.
	public function run($id){
		
		if(isset($id) and is_numeric($id)){
			
			if(Yii::app()->user->getID()===$id){
				
				//Si c'est l'utilisateur courant $redirectTo  view
				$this->getController()->viewProfile(Yii::app()->user->getID(),'view');
			}
			else {			
					$mesContacts=array();
					$estContact=false;
					$mesContacts=ContactMembres::model()->contacts(Yii::app()->user->getID());
					
					if($mesContacts===array())
					{
						/*
							//Si c'est un compte Premium
							$membreArray=Membres::model()->getType();
							
							if($membreArray===false)
								throw new CHttpException(400,'Requête invalide.');		
							
							
											if($membreArray['profil_type']==='premium')
													$this->getController()->viewProfile($id,'viewUpgrade');
							
												else
											       //Afficher une vue Info
													$this->viewInfo($id);
						*/
						
						if(Membres::model()->isHr())
							$this->getController()->viewProfile($id,'viewContact');
						else 	
							$this->viewInfo($id);
							
					}
				else{
						
						foreach ($mesContacts as $contact){
							
							if($contact===$id){
								$estContact=true;
								break;
							}
						}
						
						if($estContact){
							
							//Si c'est un contact
							$this->getController()->viewProfile($id,'viewContact');
							
						}
						else 
						{	

						/*	
							//Si c'est un compte Premium
							$membreArray=Membres::model()->getType();
							
							if($membreArray===false)
								throw new CHttpException(400,'Requête invalide.');		
							
							
												if($membreArray['profil_type']==='premium')
													$this->getController()->viewProfile($id,'viewUpgrade');
							
												else
											       //Afficher une vue Info
													$this->viewInfo($id);	
													
							*/
							
							if(Membres::model()->isHr())
								$this->getController()->viewProfile($id,'viewContact');
							else 	
								$this->viewInfo($id);
						
						}
					
				}
				
			}	
		
		}
		else{
			
			throw new CHttpException(400,'Requête invalide.');
		}
			
		
	}
	
	
	
	//Affiche les informations pour toutes les membres qui ne sont pas dans votre reseau et qui n'ont pas un compte premium
	public function viewInfo($id){
		
				$contactNombre=0;
				$visiteNombre=0;
				
				//Suggest group
				$suggestGroups=ContactGroupes::model()->getSuggest();

				//Suggest contacts to user
				$suggestContacts=array();
 				$suggestId=array();
 				$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 				if($suggestId!=array())
 				{	
 											
						foreach ($suggestId as $value)
							{
								$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
							}
				}
				
				//Calculer le nombre de visite du profil				
				$visiteNombre=VisiteMembres::model()->count('b_membres_id='.$id);
				
 				//Calculer le nombre de contacts du profil			
				$contactNombre=ContactMembres::model()->count('(destinataire_id='.$id.' AND etat=\'accepte\')
															   OR (expediteur_id='.$id.' AND etat=\'accepte\')');
				
				//Membres
				$membre=array();
				$membre=Membres::model()->membreInfoById($id);
		
				if($membre===false)
					throw new CHttpException(400,'Requête invalide.');
				//Pour savoir si vous avez envoye ou recu une demande a cette personne.
				$etat=ContactMembres::model()->etatDemande($id);
				$this->visiteProfil($id);
										
				
				$this->getController()->render('viewInfo',array(
																		'membre'=>$membre,
																		'contactNombre'=>$contactNombre,
																		'visiteNombre'=>$visiteNombre,	
																		'contactMembres'=>$etat,
																		'suggestGroups'=>$suggestGroups,
																		'suggestMembres'=>$suggestContacts						
										));	
		
	}
	
	//Si on visite le profil de quelqu'un d'autre
	public function visiteProfil($id){
 					
				
					$idVisiteur=Yii::app()->user->getID();
					
					try 
					   {
						
							$visiteResult=Yii::app()->db->createCommand()
							->select('b_membres_id,b_visiteurs_id')
							->from('b_visite_membres')
							->where(array('and','b_membres_id='.$id,'b_visiteurs_id='.$idVisiteur))
							->query();
						
					} catch (Exception $e) {
						
						throw new CHttpException(400,'Requête invalide.');
					}
					
					
				$visiteArray=$visiteResult->readAll();
					
				if($visiteArray===array())
				{
						$visite=new VisiteMembres();
					
						$visite->b_membres_id=$id;
						$visite->b_visiteurs_id=$idVisiteur;
						$visite->date=date('Y-m-j');
						$visite->save();
						
						
						//Envoyer un email pour vous notifier que vous avez recu une visite.
						
						$visiteur=Membres::model()->getViewIdInfo($idVisiteur);
						$destinataire=Membres::model()->getViewIdInfo($id);
				
						//Send an email to notify $destinataire
						$mail= new YiiMailMessage;
						$mail->view='visiteProfil';
						$mail->setBody(array('nom'=>$visiteur['nom'],'prenom'=>$visiteur['prenom'],'id'=>$visiteur['id']),'text/html');
						$mail->addTo($destinataire['email']);
						$mail->from='branchew-notification@branchew.com';
						$mail->subject='Visite de votre profil sur branchew';
						Yii::app()->mail->send($mail);
						
				}
					
	}
 	
 			
}
