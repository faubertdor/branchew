<?php

class VisiteMembresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$visitesArray=array();
		$visiteurs=array();
		
		$visitesArray=Yii::app()->db->createCommand()
		->select('b_membres_id,b_visiteurs_id,date,b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite,pays')
		->from('b_visite_membres')
		->where('b_membres_id='.Yii::app()->user->getId())
		->join('b_membres','b_visiteurs_id=b_membres.id')
		->order('date DESC')
		->queryAll();
		
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		$dataProvider = new CArrayDataProvider($visitesArray, array(
																		'keyField'=>'membres_id',
																		'pagination'=>array(
								 									    'pageSize'=>10,
																							),
														));
		
		$this->render('view',array('dataProvider'=>$dataProvider,'suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups	));
		
	}

}
