<?php

class MembresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('update','uploadImage','requestCities'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('update','uploadImage','requestCities'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{

		$model=$this->loadModel(Yii::app()->user->getID());
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Membres']))
		{
			$model->attributes=$_POST['Membres'];
			$model->objectif=$_POST['objectif'];
			$model->date_naissance=$model->annee.'-'.$model->mois.'-'.$model->jour;
			
			if($model->save())
				$this->redirect(ContactMembres::model()->getUpdateUrl());
		}

		if($model->date_naissance!=NULL){
			
			$model->annee=substr($model->date_naissance,0,4);
			$model->mois=substr($model->date_naissance,-5,2);
			$model->jour=substr($model->date_naissance,8,2);
		}
		
		//Suggest group
	 	$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
		 	foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		$this->render('update',array(
										'model'=>$model,
										'suggestGroups'=>$suggestGroups,
										'suggestMembres'=>$suggestContacts
									));
	}

	public function actionUploadImage(){
		
		$dir=Yii::getPathOfAlias('application.modules.profil.assets.images');

		
		$nouveauImage=new UploadImages();
		
		if(isset($_POST['UploadImages'])){
			
			$nouveauImage->attributes=$_POST['UploadImages'];
			$this->performAjaxValidation($nouveauImage);
			
			$file=CUploadedFile::getInstance($nouveauImage,'image');
			
			
			if($nouveauImage->validate()){				
				
				$file->saveAs($dir.'/'.Yii::app()->user->getID());
				$membres=Membres::model()->findByPk(Yii::app()->user->getID());
				$membres->chemin_avatar=Yii::app()->assetManager->publish($dir.'/'.Yii::app()->user->getID());
				$membres->save();
				
				$this->redirect(ContactMembres::model()->getUpdateUrl());
			}
			
		}
		
		$this->render('uploadImage',array(
											'imageProfil'=>$nouveauImage,		
										 ));
		
	}
	
	//Pour modifier un membre
	public function loadModel($id)
	{
		$model=Membres::model()->findByPk($id);
		
		if($model===null)
			throw new CHttpException(404,'La page demandée n\'existe pas.');
			
			
		return $model;
	}

	//Pour ajouter les villes dans le formulaire
	public function actionRequestCities()
	{
		
		if(isset($_POST['Membres']['pays']))
		{
			$data=Villes::model()->findAll('b_pays_id=:parent_id',array(':parent_id'=>(int)$_POST['Membres']['pays']));

       		$data=CHtml::listData($data,'id','ville');
		
		
			foreach($data as $key=>$value)
				 echo CHtml::tag('option', array('value'=>$key), CHtml::encode($value), true);
		}
	}
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='membres-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
