<?php

class ContactMembresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('received','create','accept','deny','contacts','block','suggest'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('received','create','accept','deny','contacts','block','suggest'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		if(isset($id) and is_numeric($id))
		{	
			if(Yii::app()->request->isAjaxRequest){
				
				$model=new ContactMembres();
				$model->expediteur_id=Yii::app()->user->getID();
				$model->destinataire_id=$id;
				$model->save();
				echo 'Demande de contact envoyée.';
				Yii::app()->end();
			}
			else 
				throw new CHttpException(400,'Requête invalide.');
					
		}
	}
	
	//Accepte une demande de connexion
	public function actionAccept($id)
	{

		if(isset($id))
		{
			$model=$this->loadModel($id);
			$model->etat='accepte';
			
			if($model->save())
			{
				$expediteur=Membres::model()->getViewIdInfo($model->expediteur_id);
				$destinataire=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
				
				//Send an email to notify $destinataire
				$mail= new YiiMailMessage;
				$mail->view='infoContacts';
				$mail->setBody(array('nom'=>$destinataire['nom'],'prenom'=>$destinataire['prenom'],'id'=>$destinataire['id']),'text/html');
				$mail->addTo($expediteur['email']);
				$mail->from=$destinataire['email'];
				$mail->subject='Demande de contact acceptée';
				Yii::app()->mail->send($mail);
					
				$this->redirect(ContactMembres::model()->getReceivedUrl());
			}
		}

		$this->redirect(ContactMembres::model()->getReceivedUrl());
	}
	
	//Refuse une demande de connexion
	public function actionDeny($id)
	{

		if(isset($id))
		{
			$model=$this->loadModel($id);
			$model->etat='refuse';
			
			if($model->save())
				$this->redirect(ContactMembres::model()->getReceivedUrl());
		}

		$this->redirect(ContactMembres::model()->getReceivedUrl());
	}
	
	//Bloquer un contact
	public function actionBlock($id)
	{

		if(isset($id))
		{
			$model=ContactMembres::model()->find('expediteur_id='.$id.' AND destinataire_id='.Yii::app()->user->getID());
			
			if($model!=null)
			{
				$model->etat='refuse';
				
				if($model->save())
					$this->redirect(ContactMembres::model()->getViewUrl($id));
			}
			else 
			{
				$model=ContactMembres::model()->find('destinataire_id='.$id.' AND expediteur_id='.Yii::app()->user->getID());
				
				if($model!=null)
				{
					$model->etat='refuse';
		
					if($model->save())
						$this->redirect(ContactMembres::model()->getViewUrl($id));
				}
				else 
					throw new CHttpException(400,'Requête invalide.');
			}
			
		}

		$this->redirect(ContactMembres::model()->getReceivedUrl());
	}
	

	//Pour accepter ou refuser une demande de connexion
	public function loadModel($id)
	{
	
		try {
				$model=ContactMembres::model()->find('expediteur_id='.$id.' AND destinataire_id='.Yii::app()->user->getID());
		} catch (Exception $e) {
			
			throw new CHttpException(400,'Requête invalide.');
		}
		
		if($model===null)
			throw new CHttpException(404,'La page demandée n\'existe pas.');
			
		return $model;
	}


	//Les contacts du profil
	public function actionContacts($id){
		
	
		if(isset($id) and is_numeric($id))
			
			$this->voirContacts($id);
	
		else
			throw new CHttpException(400,'Requête invalide.');	
	}
	
	
	//query for one contact by id for current user because it's detail view of each contact
	public function queryMyContact($id){
 		
		try {
				$membreArray=Yii::app()->db->createCommand()
					->select('id,chemin_avatar,nom,prenom,email,adresse,domaine_activite,pays,ville,telephone')
					->from('b_membres')
					->where('id='.$id)
					->queryRow();//Parce que nous allons utiliser un CArrayDataProvider
					
					
		/**
				if($membreArray==array())
						throw new CHttpException(400,'Requête invalide.');
				else
		**/				
					return $membreArray;
				
		}
	     catch (Exception $e) {
	     	
	     	throw new CHttpException(400,'Requête invalide.');
		}
	
 	}
 	
 	
 	//Sugggest contacts
 	public function actionSuggest()
 	{
 		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggest();
 		
 		if($suggestId!=array())
 		{		
			 foreach ($suggestId as $value)
			 {
						$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
			 }
		}
		
		$dataProvider = new CArrayDataProvider($suggestContacts,array('pagination'=>array(
								 									   						 'pageSize'=>10,
																						))
											   );
											   
											   
	 
 				$this->render('suggest',array(
													'suggestMembres'=>$dataProvider,		
										  ));
 	}
	
 	
 	
 	
 	//Voir mes/ou les contacts de qqn
 	public function voirContacts($id){
 		
 		$contactMembres=array();
 		$contactsId=ContactMembres::model()->contacts($id);
 		

 		if($id==Yii::app()->user->getID())
 		{
				if($contactsId!=array()){
					
					foreach ($contactsId as $value) {
						
						$contactMembres[$value]=$this->queryMyContact($value);
						
					
					}
				}
				
 		}
 		else 
 		{
 			if($contactsId!=array()){
					
					foreach ($contactsId as $value) {
						
						$contactMembres[$value]=ContactMembres::model()->queryContact($value);
						
					
					}
				}
				
 		}
	
				
		$dataProvider = new CArrayDataProvider($contactMembres,array('pagination'=>array(
								 									   						 'pageSize'=>10,
																						))
											   );
		$dataProvider->sort->defaultOrder='prenom,nom ASC';
		
											   
 		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
				
		if($id==Yii::app()->user->getID()){

			$this->render('myContacts',array(
												'contactMembres'=>$dataProvider,
												'suggestMembres'=>$suggestContacts,	
												'suggestGroups'=>$suggestGroups								
										  ));
		
		}
 									 	    
 		else {
 			
 				$membre=Membres::model()->membreInfoById($id);
 				$this->render('contacts',array(
											'contactMembres'=>$dataProvider,
 											'membre'=>$membre,
 											'suggestMembres'=>$suggestContacts,	
											'suggestGroups'=>$suggestGroups				
										 ));
 			}
 	}
 	

 	
 	//Demandes de contacts
 	
	public function actionReceived(){
		
		//Tout les demandes de contacts recu par le membre.
		$demandeContactsArray=ContactMembres::model()->getDemandes(Yii::app()->user->getID());
		$demandeData=new CArrayDataProvider($demandeContactsArray,array(
																		
																		'keyField'=>'membres_id',
																		'pagination'=>array(
								 									 						   'pageSize'=>10,
																							),
																		));
																		
		//Inbox
		$demandesContacts=ContactMembres::model()->getDemandesCount();
		$nouveauMessages=Messages::model()->getNewMessagesCount();
		$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
		
		$this->render('received',array(
										'demandes'=>$demandeData,
										'demandesContacts'=>$demandesContacts,
										'nouveauMessages'=>$nouveauMessages,
										'recommandationRequest'=>$recommandationRequest
		
					 ));
	}
 	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contact-membres-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	
}
