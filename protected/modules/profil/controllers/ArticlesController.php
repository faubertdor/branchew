<?php

class ArticlesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'actions'=>array('index','create','update','delete','uploadImage','viewArticles','publish','unPublish'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('index','view','create','update','delete','uploadImage','viewArticles','publish','unPublish'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
		
		//If an article is not published, only you can see it
		if($model->actif==Articles::STATUS_DRAFT and $model->b_membres_id!=Yii::app()->user->getID())
			throw new CHttpException(404,'Requête invalide.');
			
		 $author=Membres::model()->membreInfoById($model->b_membres_id);
		
		if(!Yii::app()->user->isGuest)
		{
			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();

			//Suggest contacts to user
			$suggestContacts=array();
 			$suggestId=array();
 			$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 			if($suggestId!=array())
 			{	
 											
				 foreach ($suggestId as $value)
				 {
						$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
				 }
			}
			
			$this->render('view',array(
											 'model'=>$model,
											 'suggestGroups'=>$suggestGroups,
											 'suggestMembres'=>$suggestContacts,	
											 'author'=>$author
							  ));
		}
		else
			$this->render('view',array(
											'model'=>$model,
											'author'=>$author
						 ));
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
			$model=new Articles;
			$model->b_membres_id=Yii::app()->user->getID();
		
			// Uncomment the following line if AJAX validation is needed
			 $this->performAjaxValidation($model);

			if(isset($_POST['Articles']))
			{
				$model->attributes=$_POST['Articles'];
				$model->contenu=$_POST['contenu'];
				$model->date=date('Y-m-d H:i:s');
				$model->actif=Articles::STATUS_DRAFT;

				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
			}

			$this->render('create',array(
				'model'=>$model,
			));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModelAdmin($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Articles']))
		{
			$model->attributes=$_POST['Articles'];
			$model->contenu=$_POST['contenu'];
			$model->actif=Articles::STATUS_DRAFT;
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{	
			if(isset($_POST['id']) and is_numeric($_POST['id']))
			{
				$id=$_POST['id'];
				// we only allow deletion via POST request
				$this->loadModelAdmin($id)->delete();
				$this->redirect(Articles::model()->getViewArticlesUrl(Yii::app()->user->getID()));
			}
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		$articlesFeed=Articles::model()->getArticlesIndex();
		$dataProvider=new CArrayDataProvider($articlesFeed,array('pagination'=>array(
								 									   						 'pageSize'=>10,
																						))
											   );
								
		$this->render('index',array('dataProvider'=>$dataProvider));
	}

	//Upload a picture
	public function actionUploadImage($id){
		
		$dir=Yii::getPathOfAlias('application.modules.profil.assets.images');
		$nouveauImage=new UploadImages();
		
		if(isset($_POST['UploadImages'])){
			
			$nouveauImage->attributes=$_POST['UploadImages'];
			$this->performAjaxValidation($nouveauImage);
			
			$file=CUploadedFile::getInstance($nouveauImage,'image');
			
			
			if($nouveauImage->validate()){				
				
				$file->saveAs($dir.'/'.Yii::app()->user->getID().'_'.$id);
				$article=$this->loadModelAdmin($id);
				$article->chemin_avatar=Yii::app()->assetManager->publish($dir.'/'.Yii::app()->user->getID().'_'.$id);
				$article->save();
				
				$this->redirect(Articles::model()->getViewUrl($id));
			}
			
		}
		
		$this->render('uploadImage',array(
											'image'=>$nouveauImage,		
										 ));
	}
	
	public function queryArticles($id)
	{
		$articlesArray=Articles::model()->getArticles($id);
		$dataProvider = new CArrayDataProvider($articlesArray,array('pagination'=>array(
								 									   						 'pageSize'=>10,
																						))
											   );
		return $dataProvider;
	}
	
	//Voir mes articles ou les articles de qqn
	public function ActionViewArticles($id)
	{
		if(isset($id) and is_numeric($id))
		{
			if(Yii::app()->user->getID()==$id)
			{
				$dataProvider=$this->queryArticles($id);
		
				$this->render('myArticles',array('dataProvider'=>$dataProvider));
			}
		}
	}
	
	//Publish an article on your network
	public function actionPublish($id){
		
		$model=$this->loadModelAdmin($id);
		$model->actif=Articles::STATUS_PUBLISHED;
		$model->date=date('Y-m-d H:i:s');
		if($model->save())
		{
			 Yii::app()->user->setFlash('publish',"Success" );
			 $this->redirect(array('view','id'=>$model->id));
		}
	}
	
	//Unpublish an article on your network
	public function actionUnpublish($id){
		
		$model=$this->loadModelAdmin($id);
		$model->actif=Articles::STATUS_DRAFT;
		$model->date=date('Y-m-d H:i:s');
		if($model->save())
		{
			 Yii::app()->user->setFlash('publish',"Success" );
			 $this->redirect(array('view','id'=>$model->id));
		}
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Requête invalide.');
		return $model;
	}
	
	public function loadModelAdmin($id)
	{
		$model=Articles::model()->findByPk($id);
		if($model===null or $model->b_membres_id!=Yii::app()->user->getID())
			throw new CHttpException(404,'Requête invalide.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='articles-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
