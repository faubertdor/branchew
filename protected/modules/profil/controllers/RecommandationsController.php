<?php

class RecommandationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  // deny non authenticated users to perform actions
				'actions'=>array('request','received','confirm','recommand','deny','delete','deleteConfirm'),
				'users'=>array('?'),
			),
			array('allow', // allow authenticated user to perform actions
				'actions'=>array('request','received','confirm','recommand','deny','delete','deleteConfirm'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	// Envoyer une demande de recommandation	
	public function actionRequest($id){
		
		if(isset($id) and is_numeric($id)){
			
					$contactsId=array();
					$contactsId=ContactMembres::model()->contacts(Yii::app()->user->getID());
					
					
					if($contactsId!=array())
					{
						
						foreach($contactsId as $key=>$value){
						
							if($value===$id)
							{
							
								$etat=Recommandations::etatRecommandations($id);
								
								if($etat===array())
								{
									$demande=new Recommandations();
									$demande->expediteur_id=Yii::app()->user->getID();
									$demande->destinataire_id=$id;
									$demande->etat='envoye';
									$demande->confirmer='false';
						
									$demande->save();
									echo 'Demande de recommandation envoyée.';
									Yii::app()->end();
								}
								else 
								{
									echo 'Demande de recommandation envoyée.';
									Yii::app()->end();
								}
							}
						}
					}
				
			   $this->redirect(ContactMembres::model()->getContactsUrl(Yii::app()->user->getID()));
		}
		
		
	}
	
	
	
	
	// Recommander un contact.	
	public function actionRecommand($id){
		
		if(isset($id) and is_numeric($id)){
			
			$recommandation=$this->loadModelRecommander($id);
			
			if(isset($_POST['Recommandations']['message'])){
				
				$recommandation->message=$_POST['Recommandations']['message'];
				$recommandation->etat='accepte';
				
				if($recommandation->destinataire_id===Yii::app()->user->getID()){
					if($recommandation->save())
						$this->redirect($this->createUrl('recommandations/received#demandes'));
				}
				else
					throw new CHttpException(400,'Requête invalide.');
			}
			
			$this->render('_form',array('model'=>$recommandation,));
		}
		
	}
	
	//Refuser une demande de recommandation
	public function actionDeny($id){
		
	if(isset($id) and is_numeric($id)){
			
			$recommandation=$this->loadModelRecommander($id);
			
			if($recommandation->destinataire_id===Yii::app()->user->getID())
		    	$recommandation->etat='refuse';
		    else
				throw new CHttpException(400,'Requête invalide.');
				
				if($recommandation->save())
					$this->redirect($this->createUrl('recommandations/received#demandes'));
			
			
			$this->redirect($this->createUrl('recommandations/received#demandes'));
		}
		
	}
	
	
	//Confirmer mes recommandations
	public function actionConfirm($id){
		
		if(isset($id) and is_numeric($id)){
		
		$recommandation=$this->loadModelConfirmer($id);
		
		if($recommandation->expediteur_id===Yii::app()->user->getID())
			$recommandation->confirmer='true';
		else
			throw new CHttpException(400,'Requête invalide.');
				
		if($recommandation->save())
			$this->redirect($this->createUrl('recommandations/received#en_attentes'));
		}	
	
 	}
 
 	//Demandes de recommandations
 	public function actionReceived(){
 		
 		$demandes=$this->loadRecommandations();
 		$en_attentes=$this->loadWaiting();
 		
 		
 		//Inbox
		$demandesContacts=ContactMembres::model()->getDemandesCount();
		$nouveauMessages=Messages::model()->getNewMessagesCount();
		$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
		
		$this->render('received',array(
											'demandes'=>$demandes,
											'en_attentes'=>$en_attentes,
											'demandesContacts'=>$demandesContacts,
											'nouveauMessages'=>$nouveauMessages,
											'recommandationRequest'=>$recommandationRequest
									  ));
 	}
 
	
	
	//Charger les demmandes de recommandations
	public function loadRecommandations(){
		
		$demandesArray=Yii::app()->db->createCommand()
		->select('b_recommandations.id recommandations_id,expediteur_id,destinataire_id,etat,
				  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_recommandations')
		->where(array('and','destinataire_id='.Yii::app()->user->getID(),'etat=\'envoye\''))
		->join('b_membres','b_membres.id=expediteur_id')
		->queryAll();
		
		$dataProvider = new CArrayDataProvider($demandesArray, array(
																		'keyField'=>'recommandations_id',
																		'pagination'=>array(
								 									   						 'pageSize'=>10,
																							),
																	));
		return $dataProvider;
	}
	
	//Charger les recommandations en attentes de confirmations
	public function loadWaiting(){
		
		$recommandationsArray=Yii::app()->db->createCommand()
		->select('b_recommandations.id recommandations_id,expediteur_id,destinataire_id,etat,message,
				  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_recommandations')
		->where(array('and','expediteur_id='.Yii::app()->user->getID(),'etat=\'accepte\'','confirmer=\'false\''))
		->join('b_membres','b_membres.id=destinataire_id')
		->queryAll();
		
		$dataProvider = new CArrayDataProvider($recommandationsArray, array(
																			 'keyField'=>'recommandations_id',
																			 'pagination'=>array(
								 									         					 	'pageSize'=>10,
																							     ),
       																       ));
		return $dataProvider;
	}
	//Effacer des recommandations
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			
		  if(isset($_POST['id']))
		  {
		  	$id=$_POST['id'];
			$recommandation=$this->loadModelEffacer($id);
			
			if($recommandation->etat==='envoye')
			{
				$recommandation->delete();
				$this->redirect($this->createUrl('recommandations/received#demandes'));
			}
			else
			 {
				$recommandation->delete();
				$this->redirect($this->createUrl('recommandations/received#en_attentes'));
			 }
		  }
		  else
		  	throw new CHttpException(400,'Requête invalide.');
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	
	//Effacer des recommandations a partir de la vue update profile
	public function actionDeleteConfirm()
	{
		if(Yii::app()->request->isPostRequest)
		{
			
		  if(isset($_POST['id']))
		  {
		  		$id=$_POST['id'];
				$recommandation=$this->loadModelEffacer($id);
			
			
				if($recommandation->delete())
					$this->redirect(ContactMembres::model()->getUpdateUrl());
		  }
		  else
		  	throw new CHttpException(400,'Requête invalide.');
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}
	

	//ActiveRecord model quand on veut recommander un contact
	public function loadModelRecommander($id)
	{
		$model=Recommandations::model()->findByPk($id);
		if($model===null or $model->destinataire_id!=Yii::app()->user->getID())
			throw new CHttpException(404,'La page demandée n\'existe pas.');
		return $model;
	}
	
	//ActiveRecord model quand on doit confirmer une recommandation recu pour etre afficher sur son profil
	public function loadModelConfirmer($id)
	{
		
		$model=Recommandations::model()->find('id='.$id.' AND expediteur_id='.Yii::app()->user->getID().' AND etat=\'accepte\' AND confirmer=\'false\'');
		if($model===null)
			throw new CHttpException(404,'La page demandée n\'existe pas.');
		return $model;
	}
	
	//Pour effacer une recommandations
	public function loadModelEffacer($id)
	{
		
		$model=Recommandations::model()->find('id='.$id.' AND (expediteur_id='.Yii::app()->user->getID().' OR destinataire_id='.Yii::app()->user->getID().')');
		if($model===null) 
			throw new CHttpException(404,'La page demandée n\'existe pas.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='recommandations-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
