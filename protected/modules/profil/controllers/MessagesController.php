<?php

class MessagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', 
				'actions'=>array('received','sent','compose','delete','read','composeProfile','autocomplete','reply'),
			
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('received','sent','compose','delete','read','composeProfile','autocomplete','reply'),
			
				'users'=>array('@'),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	//Nouveau message
	public function actionCompose()
	{
		$model=new Messages;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Messages']))
		{  	
				$model->attributes=$_POST['Messages'];
				$model->date_envoie=Date('Y-m-d H:i:s');
				$model->expediteur_id=Yii::app()->user->getID();
				$model->destinataire_id=$_POST['destinataire_id'];
				
			if($model->save())
			{
				$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
				$destinataire=Membres::model()->getViewIdInfo($_POST['destinataire_id']);
	
				//Send an email to notify $destinataire
				$mail= new YiiMailMessage;
				$mail->view='message';
				$mail->setBody(array('nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
				$mail->addTo($destinataire['email']);
				$mail->from=$expediteur['email'];
				$mail->subject=$model->objet;
				Yii::app()->mail->send($mail);
				
				Yii::app()->user->setFlash('messageSent',"Message envoyé" );
				$this->redirect(Messages::model()->getReceivedUrl());
			}
			
		}
			//Inbox
			$demandesContacts=ContactMembres::model()->getDemandesCount();
			$nouveauMessages=Messages::model()->getNewMessagesCount();
			$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
		
			$this->render('compose',array(
											 'model'=>$model,
											 'demandesContacts'=>$demandesContacts,
											 'nouveauMessages'=>$nouveauMessages,
											 'recommandationRequest'=>$recommandationRequest
										 ));
	}
	
	//Repondre a un message
	public function actionReply(){
		
		$model=new Messages;
		$this->performAjaxValidation($model);
		
		if(isset($_POST['id']))
		{
			$model->destinataire_id=$_POST['id'];
			$destinataire=$_POST['nom'].' '.$_POST['prenom'];
			$this->render('reply',array(
											'model'=>$model,
											'destinataire'=>$destinataire
									  ));
		}
			
			
			if(isset($_POST['Messages']))
			{
				
				$model->attributes=$_POST['Messages'];
				$model->date_envoie=Date('Y-m-d');
				$model->expediteur_id=Yii::app()->user->getID();
				$destinataire=Membres::model()->getViewIdInfo($model->destinataire_id);
				
				if($model->save())
				{
					$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
					
				
					//Send an email to notify $destinataire
					$mail= new YiiMailMessage;
					$mail->view='message';
					$mail->setBody(array('nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
					$mail->addTo($destinataire['email']);
					$mail->from=$expediteur['email'];
					$mail->subject=$model->objet;
					Yii::app()->mail->send($mail);
				
					$this->redirect(Messages::model()->getReceivedUrl());
				}	
			}
			
		
	}
	//Nouveau message a partir d'un profil
	public function actionComposeProfile()
	{
		
		
				$model=new Messages;
				$this->performAjaxValidation($model);
				
				$flag=true;
				
				if(isset($_POST['id']))
					$model->destinataire_id=$_POST['id'];
					
				
				if(isset($_POST['Messages']))
				{
			    	$flag=false;
					$model->attributes=$_POST['Messages'];
					$model->date_envoie=Date('Y-m-d');
					$model->expediteur_id=Yii::app()->user->getID();
					$destinataire=Membres::model()->getViewIdInfo($model->destinataire_id);
					
					if($model->save())
					{
						$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
						
						/**
						//Send an email to notify $destinataire
						$mail= new YiiMailMessage;
						$mail->view='message';
						$mail->setBody(array('nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
						$mail->addTo($destinataire['email']);
						$mail->from=$expediteur['email'];
						$mail->subject=$model->objet;
						Yii::app()->mail->send($mail);
						*/
						
						Yii::app()->end();
					}
					else
						$flag=true;
					
					
			    }		
			  
			   
			
			    if($flag)
			    {
			    	Yii::app()->clientScript->scriptMap['jquery.js']=false;
					$this->renderPartial('_composeProfile',array('model'=>$model),false,true);
			    }
		
	}
	

	//Reception des messages
	public function actionReceived()
	{
			$dataProvider=Messages::model()->loadReceived();

			//Inbox
			$demandesContacts=ContactMembres::model()->getDemandesCount();
			$nouveauMessages=Messages::model()->getNewMessagesCount();
			$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
			
			$this->render('received',array(
											'received'=>$dataProvider,
											'demandesContacts'=>$demandesContacts,
											'nouveauMessages'=>$nouveauMessages,
											'recommandationRequest'=>$recommandationRequest
										));
	}


	//Effacer un message		
	public function actionDelete()
	{
		
		if(isset(Yii::app()->request->isPostrequest))
		{
		
			if(isset($_POST['id']))
			{
				$id=$_POST['id'];
				$messages=$this->loadModel($id);	
				$id=$messages->expediteur_id;	
				$messages->delete();
			
				if($id===Yii::app()->user->getID())
				
					$this->redirect(Messages::model()->getSentUrl());
				
				else 
					$this->redirect(Messages::model()->getReceivedUrl());
			}
			else 	
				throw new CHttpException(400,'Requête invalide.');
		}
		
	}
	
	
	//Pour lire un message
	public function actionRead($id){
	
		//Reply to a message
		if(isset($_POST['Messages']) and Yii::app()->request->isPostRequest)
		{	
			$new= new Messages();
			$this->performAjaxValidation($new);
			$new->date_envoie=Date('Y-m-d H:i:s');
			$new->contenu=$_POST['Messages']['contenu'];
			$new->objet=$_POST['Messages']['objet'];
			$new->destinataire_id=$_POST['Messages']['destinataire_id'];
			$new->expediteur_id=Yii::app()->user->getID();

			
			if($new->validate() and $new->save())
			{
				$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
				$destinataire=Membres::model()->getViewIdInfo($new->destinataire_id);
				
					//Send an email to notify $destinataire
					$mail= new YiiMailMessage;
					$mail->view='message';
					$mail->setBody(array('nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
					$mail->addTo($destinataire['email']);
					$mail->from=$expediteur['email'];
					$mail->subject=$model->objet;
					Yii::app()->mail->send($mail);

				Yii::app()->user->setFlash('messageSent',"Message envoyé" );
				$this->redirect(Messages::model()->getReceivedUrl());
			}
		}
		
		if(isset($id) and is_numeric($id))
		{
			$reply= new Messages();
			$message=$this->loadModel($id);
			
			if($message->date_lecture == "" and $message->destinataire_id == Yii::app()->user->getID())
			{
				$message->date_lecture=date('Y-m-d H:i:s');
				$message->save();
			}
			
			$reply->objet=$message->objet;
			
			//If you request from Sent items or items received
			if($message->expediteur_id == Yii::app()->user->getID())
			$reply->destinataire_id=$message->destinataire_id;
			else
			$reply->destinataire_id=$message->expediteur_id;
			
			$reply->expediteur_id=Yii::app()->user->getID();

			//Loading conversation
			$objet=$message->objet;
			
			//If you request from Sent items or items received
			if($message->expediteur_id == Yii::app()->user->getID())
			$membre_id=$message->destinataire_id;
			else
			$membre_id=$message->expediteur_id;
			
			$conversationArray=Messages::model()->loadConversation($membre_id, $objet);
			
				
			//Making sure date_lecture is properly set for each message in the conversation.
			foreach ($conversationArray as $value)
			{
				$msgUpdate=$this->loadModel($value['messages_id']);
				
				if($msgUpdate->date_lecture == "" and $value['destinataire_id']== Yii::app()->user->getID())
				{
					$msgUpdate->date_lecture=date('Y-m-d H:i:s');
					$msgUpdate->save();
				}
			}
			
			$conversation = new CArrayDataProvider($conversationArray, array('keyField'=>'messages_id',));
			
			//Setting pager to last message received in the conversation.
			$pager=$conversation->pagination;
                $pager->itemCount=$conversation->totalItemCount;
                if(!Yii::app()->request->isAjaxRequest)
                        $pager->currentPage=$pager->pageCount;
			
			
		}
				
			//Inbox
			$demandesContacts=ContactMembres::model()->getDemandesCount();
			$nouveauMessages=Messages::model()->getNewMessagesCount();
			$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
			
			
			$this->render('read',array(
											'message'=>$message,
											'reply'=>$reply,
											'conversation'=>$conversation,
											'demandesContacts'=>$demandesContacts,
											'nouveauMessages'=>$nouveauMessages,
											'recommandationRequest'=>$recommandationRequest
										));
	}
	
	//Messages envoyees
	public function actionSent(){
		
		
		$dataProvider=Messages::model()->loadSent();
		
		//Inbox
		$demandesContacts=ContactMembres::model()->getDemandesCount();
		$nouveauMessages=Messages::model()->getNewMessagesCount();
		$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
		
		$this->render('sent',array(
									'sent'=>$dataProvider,
									'demandesContacts'=>$demandesContacts,
									'nouveauMessages'=>$nouveauMessages,
									'recommandationRequest'=>$recommandationRequest
								  ));
	}
	
	
	
	
		//Auto complete when create messages	
		public function actionAutocomplete(){
		
			$res=array();
			$term = Yii::app()->getRequest()->getParam('q', false);
			 
   	  	 if (isset($term))
      	 {
         	$sql = 'SELECT id, nom,prenom FROM b_membres where (LCASE(nom) LIKE :term) OR (LCASE(prenom) LIKE :term)';
         	$cmd = Yii::app()->db->createCommand($sql);
         	$cmd->bindValue(":term","%".strtolower($term)."%", PDO::PARAM_STR);
         	$res = $cmd->queryAll();
         	$returnVal='';
         	
         	foreach($res as $value){
         		 
         		$returnVal .=$value['nom'].' '.$value['prenom'].'|'.$value['id']."\n";
          		
         	}
         	
         	echo $returnVal;
     	 }		
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Messages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandée  n\'existe pas.');
		else if($model->destinataire_id===Yii::app()->user->getID() || $model->expediteur_id===Yii::app()->user->getID())
				return $model;
			else 
				throw new CHttpException(404,'L\'element demandée  n\'existe pas.');
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='messages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
