<?php

class DefaultController extends Controller
{
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('index','news','view','autocomplete','update','search','connect','advancedSearch','invite','chatroom','searchCv','cvcomplete','skillview'),
				'users'=>array('?'),
			),
			array('allow',  
				'actions'=>array('cv'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('index','news','autocomplete','view','update','cv','search','connect','advancedSearch','invite','chatroom','searchCv','cvcomplete','skillview'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actions(){
		
		return array(

					'view'=>'application.modules.profil.controllers.default.ViewAction',
									
							
					'search'=>array('class'=>'application.modules.profil.controllers.default.SearchAction',
									'searchMode'=>'normal',
						),
										
		);
		
	}
	
	//Chat room
	public function actionChatroom()
	{
		//Suggest group
	 	$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
		 	foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		$this->render('chatroom',array(
										 'suggestGroups'=>$suggestGroups,
										 'suggestMembres'=>$suggestContacts
					 ));
	}
	
	//Invite
	public function actionInvite()
	{
		$this->render('invite');
	}
	
	// Index
	public function actionIndex(){
		
		$this->viewProfile(Yii::app()->user->getID(),'index');
	}
	
	
	
	//Update
	public function actionUpdate(){
		
		$this->viewProfile(Yii::app()->user->getID(),'update');
	}
	
	//Cv
	public function actionCv($id){
		
		$this->viewProfile($id, 'cv');
	}
	
	//News feed
	public function getNews(){
			//Creating news feed
			$newsFeed=array();
			//Emp feed
			$newsFeed=OffreEmp::model()->getIndex();
			//Articles feed
			$articlesFeed=Articles::model()->getArticlesIndex();
			foreach ($articlesFeed as $value)
			$newsFeed[]=$value;
			//Statuts feed
			$statutsFeed=Statuts::model()->getStatutsIndex();
			foreach ($statutsFeed as $value)
			$newsFeed[]=$value;
			
			$dataProvider=new CArrayDataProvider($newsFeed,array('sort'=>array( 'attributes'=>array('date')),'pagination'=>array('pageSize'=>10)));
			$dataProvider->sort->defaultOrder='date DESC';
			return $dataProvider;
	}
	
	//Render partial
	public function actionNews(){
		
			//Creating news feed
			$dataProvider=$this->getNews();
			$this->renderPartial('_news',array('articlesFeed'=>$dataProvider),false,true);
	}
	
	
	
	//viewProfile($id) cette fonction affiche le profil pour le membre les contacts et les RH ou membres avec un compte Premium
	public function viewProfile($id,$view){
		
		$demandeContactsArray=array();
		$contactNombre=0;
		$visiteNombre=0;
				
		//Calculer le nombre de visite du profil		
		$visiteNombre=VisiteMembres::model()->count('b_membres_id='.$id);
				
 		//Calculer le nombre de contacts du profil
		$contactNombre=ContactMembres::model()->count('(destinataire_id='.$id.' AND etat=\'accepte\')
																 OR (expediteur_id='.$id.' AND etat=\'accepte\')');
		//Afficher les derniers 3 visiteurs
		$lastVisitors=VisiteMembres::model()->getLastVistors();
			
		//Informations du membre
		$membre=array();
		$membre=Membres::model()->membreById($id);
		
		if($membre===false)
			throw new CHttpException(400,'Requête invalide.');
		
		
		//View index
		if($view==='index')
		{
			//new model for statut
			$statut=new Statuts();
			
			//Creating news feed
			$dataProvider=$this->getNews();
			
										//Notifications
										$demandesContacts=ContactMembres::model()->getDemandesCount();
										$nouveauMessages=Messages::model()->getNewMessagesCount();
										$recommandationRequest=Recommandations::model()->getRecommandationRequestCount();
										$recommandationConfirm=Recommandations::model()->getRecommandationConfirmCount();
										
										//Suggest group
										$suggestGroups=ContactGroupes::model()->getSuggest();

										//Suggest contacts to user
										$suggestContacts=array();
 										$suggestId=array();
 										$suggestId=ContactMembres::model()->getSuggestIndex();

 										if($suggestId!=false)
 										{	
 											
											 foreach ($suggestId as $value)
											 {
													$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
											 }
										}
										
								
										
								
			$this->render($view,array(
										'membre'=>$membre,
										'contactNombre'=>$contactNombre,
										'visiteNombre'=>$visiteNombre,		
										'articlesFeed'=>$dataProvider,
										'demandesContacts'=>$demandesContacts,
										'nouveauMessages'=>$nouveauMessages,
										'recommandationConfirm'=>$recommandationConfirm,	
										'recommandationRequest'=>$recommandationRequest,
										'suggestMembres'=>$suggestContacts,	
										'suggestGroups'=>$suggestGroups,
										'lastVisitors'=>$lastVisitors,
										'statut'=>$statut,
									));	
		}
		else 
		{
		
		
			//Educations	
			$educations=Educations::model()->getByMembreId($id);
		
			//Experiences
			$experiences=Experiences::model()->getByMembreId($id);
		
			//Recommandations confirmer
			if($view!='cv')
			$recommandations=Recommandations::model()->getByMembreId($id);
		
		
		
								if($view==='update')
								{
										//Suggest group
										$suggestGroups=ContactGroupes::model()->getSuggest();

										//Suggest contacts to user
										$suggestContacts=array();
 										$suggestId=array();
 										$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 										if($suggestId!=array())
 										{	
 											
											 foreach ($suggestId as $value)
											 {
													$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
											 }
										}
									
										$this->render($view,array(
																	'membre'=>$membre,
																	'educations'=>$educations,
																	'experiences'=>$experiences,
																	'contactNombre'=>$contactNombre,
																	'visiteNombre'=>$visiteNombre,	
																	'recommandations'=>$recommandations,
																	'suggestGroups'=>$suggestGroups,
																	'suggestMembres'=>$suggestContacts,	
															));	
									
									
								}
								else
								{	
										if($view=='cv')
										{
											//Suggest contacts and groups
											$suggestContacts=array();
											$suggestGroups=array();
											
											if(!Yii::app()->user->isGuest)
											{
												//Suggest groups
												$suggestGroups=ContactGroupes::model()->getSuggest();

												//Suggest contacts to user
												$suggestContacts=ContactMembres::model()->getSuggestMembers();
											}
											
											//Groupes 
											$groupes=Groupes::model()->getGroupesByID($id);
								
											//Entreprises
											$entreprises=AdminEntreprises::model()->getEntreprises($id);
											
											
											$this->render($view,array(
																		'membre'=>$membre,
																		'educations'=>$educations,
																		'experiences'=>$experiences,
																		'contactNombre'=>$contactNombre,
																		'visiteNombre'=>$visiteNombre,		
																		'groupes'=>$groupes,
																		'entreprises'=>$entreprises,
																		'suggestMembres'=>$suggestContacts,	
																		'suggestGroups'=>$suggestGroups			
																));	
											
										}
										else 
										{
											//Suggest group
											$suggestGroups=ContactGroupes::model()->getSuggest();

											//Suggest contacts to user
											$suggestContacts=array();
 											$suggestId=array();
 											$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 											if($suggestId!=array())
 											{	
 											
											 	foreach ($suggestId as $value)
											 	{
													$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
											 	}
											}
											
											//Groupes 
											$groupes=Groupes::model()->getGroupesByID($id);
								
											//Entreprises
											$entreprises=AdminEntreprises::model()->getEntreprises($id);
									
								
										

											$this->render($view,array(
																		'membre'=>$membre,
																		'educations'=>$educations,
																		'experiences'=>$experiences,
																		'contactNombre'=>$contactNombre,
																		'visiteNombre'=>$visiteNombre,	
																		'recommandations'=>$recommandations,	
																		'groupes'=>$groupes,
																		'entreprises'=>$entreprises,
																		'suggestMembres'=>$suggestContacts,	
																		'suggestGroups'=>$suggestGroups				
																));	
										}
								}

		}
	}
	
	
	//Recherche avance.
	public function actionAdvancedSearch(){
		
		
		 	$model=new Membres('search');
			$model->unsetAttributes();  // clear any default values
			
			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();
			
			//Suggest contacts to user
			$suggestContacts=array();
 			$suggestId=array();
 			$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 			if($suggestId!=array())
 			{	
 				foreach ($suggestId as $value)
				{
					$suggestContacts[$value]=ContactMembres::model()->queryContact($value);							
				 }
			}
			
			if(isset($_GET['Membres'])){
				$model->attributes=$_GET['Membres'];
				
				$dataProvider=$model->search();
				
				$this->render('search',array(
												'dataProvider'=>$dataProvider,
											));
				
			
			}
			else
				$this->render('_search',array(
												'model'=>$model,
												'suggestMembres'=>$suggestContacts,	
												'suggestGroups'=>$suggestGroups	
											 ));
		
	}
	
	//connect action to be used with ajax	
	public function actionConnect($id){
		
		if(isset($id) and is_numeric($id))
		{	
			if(Yii::app()->request->isAjaxRequest)
			{
				
				$model=new ContactMembres();
				$model->expediteur_id=Yii::app()->user->getID();
				$model->destinataire_id=$id;
				$model->save();
				
				$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
				$destinataire=Membres::model()->getViewIdInfo($id);
				
				//Send an email to notify $destinataire
				$mail= new YiiMailMessage;
				$mail->view='demandeContacts';
				$mail->setBody(array('nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
				$mail->addTo($destinataire['email']);
				$mail->from=$expediteur['email'];
				$mail->subject='Demande de contact sur branchew';
				Yii::app()->mail->send($mail);
					
				
				echo 'Demande de contact envoyée.';
				Yii::app()->end();			
			}
			else 
				 throw new CHttpException(400,'Requête invalide.');
					
		}
		
	}
	
	
	public function actionSearchCv()
	{
			$model=new SearchForm;
		  	if(isset($_GET['cvcomplete']))
		  	{
		  		$model->category="membre";
		  		$model->search=$_GET['cvcomplete'];
		  		
		  		if($model->validate())
			 	 {
					  $membreArray=Yii::app()->db->createCommand()
						->selectDistinct('b_membres.id,b_experiences.id exp_id,b_membres_id, position, description,chemin_avatar,nom,prenom,domaine_activite')
						->from('b_experiences')
						->where(array('or',array('like','position','%'.$model->search.'%'),
								   array('like','description','%'.$model->search.'%'),
								   array('like','position','%'.$model->search),
								   array('like','description','%'.$model->search),
								   array('like','position',$model->search.'%'),
								   array('like','description','%'.$model->search.'%'),
								))
						->join('b_membres','b_membres.id=b_experiences.b_membres_id')
						->queryAll();
		
						$dataProvider = new CArrayDataProvider($membreArray, array('pagination'=>array('pageSize'=>10)));
			
					$this->render('searchCv',array(	'dataProvider'=>$dataProvider));
			  }  		  
		  	}
		
	   }
	   
	/**
	 * 
	 * Auto Complete action
	 * 
	 */	
	  public function actionCvcomplete(){
		
		$res=array();
		$term = Yii::app()->getRequest()->getParam('q', false);
			 
   	  	 if (isset($term))
      	 {
         	$sql = 'SELECT id,description,position FROM b_experiences where (LCASE(description) LIKE :term)';
         	$cmd = Yii::app()->db->createCommand($sql);
         	$cmd->bindValue(":term","%".strtolower($term)."%", PDO::PARAM_STR);
         	$res = $cmd->queryAll();
         	$returnVal='';
         	
         	foreach($res as $value){
         		 
         		$returnVal .=strip_tags($value['description'].'|'.$value['id']."\n");
          		
         	}
         	
         	echo $returnVal;
     	 }		
	 }
	 
	 //Render a nice view to search skills
	 public function actionSkillview()
	 {
	 	//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();
 		
 		if($suggestId!=array())
 		 {	
 			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
	 	$this->render('skillview', array('suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups	));
	 }
	
	//Auto Complete function
 	public function actionAutocomplete()
 	{
		
		$res=array();
		$term = Yii::app()->getRequest()->getParam('q', false);
			 
   	  	 if (isset($term))
      	 {
         	$sql = 'SELECT id,nom,prenom FROM b_membres WHERE (LCASE(nom) LIKE :term) OR (LCASE(prenom) LIKE :term)';
         	$cmd = Yii::app()->db->createCommand($sql);
         	$cmd->bindValue(":term","%".strtolower($term)."%", PDO::PARAM_STR);
         	$res = $cmd->queryAll();
         	$returnVal='';
         	
         	foreach($res as $value){
         		 
         		$returnVal .=$value['nom'].' '.$value['prenom']."\n";
          		
         	}
         	
         	echo $returnVal;
     	 }		
	}
	
}