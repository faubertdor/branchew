<?php

class EducationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', 
				'actions'=>array('create','view','update','delete'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('create','view','update','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Educations;

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Educations']))
		{
			$model->attributes=$_POST['Educations'];
			$model->competence_specifique=$_POST['competence_specifique'];
			$model->date_debut=$model->anneeDebut.'-'.$model->moisDebut.'-01';
			$model->date_fin=$model->anneeFin.'-'.$model->moisFin.'-28';
			
			if($model->save())
				$this->redirect(ContactMembres::model()->getUpdateUrl());
		}
		
		//Suggest group
	 	$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
		 	foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}

		$this->render('create',array(
										'model'=>$model,
										'suggestGroups'=>$suggestGroups,
										'suggestMembres'=>$suggestContacts
									));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);

		if(isset($_POST['Educations']))
		{
			$model->attributes=$_POST['Educations'];
			$model->competence_specifique=$_POST['competence_specifique'];
			$model->date_debut=$model->anneeDebut.'-'.$model->moisDebut.'-01';
			$model->date_fin=$model->anneeFin.'-'.$model->moisFin.'-28';
			
			if($model->save())
				$this->redirect(ContactMembres::model()->getUpdateUrl());
		}
		
		if($model->date_debut!=NULL){
			
			$model->anneeDebut=substr($model->date_debut,0,4);
			$model->moisDebut=substr($model->date_debut,-5,2);
		}
		
		if($model->date_fin!=NULL){
			
			$model->anneeFin=substr($model->date_fin,0,4);
			$model->moisFin=substr($model->date_fin,-5,2);
		}

		//Suggest group
	 	$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
		 	foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		$this->render('update',array(
										'model'=>$model,
										'suggestGroups'=>$suggestGroups,
										'suggestMembres'=>$suggestContacts
									));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']))
			{
				$id=$_POST['id'];
				
				if($this->loadModel($id)->delete())
					$this->redirect(ContactMembres::model()->getUpdateUrl());
			}
			else 
				throw new CHttpException(400,'Requête invalide.');
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Educations::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandee n\'existe pas.');
		else if($model->b_membres_id===Yii::app()->user->getID())
				return $model;
			else 
			throw new CHttpException(404,'L\'element demandée n\'existe pas.');
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='educations-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
