<?php

class StatutsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', 
				'actions'=>array('index','create','update','delete','comment'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('index','view','create','update','delete','comment'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	//Commentaires
	public function actionComment($id)
	{
		if(isset($id) and is_numeric($id))
		{
			if(Yii::app()->request->isAjaxRequest)
				$this->renderPartial('application.modules.profil.views.statuts._comment',array('id'=>$id),false,true);
				Yii::app()->end();	
		}
		
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$setTitle=true;
		$this->render('_view',array(
										'data'=>$this->loadModel($id),
										'setTitle'=>$setTitle,
									));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Statuts;

		// Uncomment the following line if AJAX validation is needed
		 //

		if(isset($_POST['Statuts']))
		{
			$this->performAjaxValidation($model);
			$model->attributes=$_POST['Statuts'];
			$model->date=date('Y-m-d H:i:s');
			$model->b_membres_id=Yii::app()->user->getID();
			
			if($model->save())
			{
				//$this->redirect(array('view','id'=>$model->id));
				$this->redirect(ContactMembres::model()->getIndexUrl());
			}
			
			$this->redirect(ContactMembres::model()->getIndexUrl());
		}

		//$this->render('create',array('model'=>$model,));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Statuts']))
		{
			$model->attributes=$_POST['Statuts'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']) and is_numeric($_POST['id']))
			{
				$id=$_POST['id'];
				// we only allow deletion via POST request
				$this->loadModelAdmin($id)->delete();
				$this->redirect(ContactMembres::model()->getIndexUrl());
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Statuts');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		 $statutArray=Yii::app()->db->createCommand()
										->select('b_statuts.id,statut,b_membres_id,date,newsId,
												  b_membres.id m_id,b_membres.chemin_avatar m_avatar,nom,prenom,domaine_activite')
										->from('b_statuts')
										->where('b_statuts.id='.$id)
										->join('b_membres','b_membres_id=b_membres.id')
										->order('date DESC')
										->queryRow();		
		if($statutArray===null)
			throw new CHttpException(404,'Requête invalide.');
		return $statutArray;
	}
	
	//Load model admin
	public function loadModelAdmin($id)
	{
		$model=Statuts::model()->findByPk($id);
		if($model===null or $model->b_membres_id!=Yii::app()->user->getID())
			throw new CHttpException(404,'Requête invalide.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='statuts-form')
		{
			CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
