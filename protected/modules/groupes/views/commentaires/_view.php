<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_creation')); ?>:</b>
	<?php echo CHtml::encode($data->date_creation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_membres_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_membres_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_groupes_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_groupes_id); ?>
	<br />


</div>