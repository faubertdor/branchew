<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commentaires-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Les champs avec <span class="required">*</span> sont obligatoire.</p>

	<?php echo $form->errorSummary($model); ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>3, 'cols'=>65)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Ajouter',array('class'=>'button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->