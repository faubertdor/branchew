<?php
$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Commentaires', 'url'=>array('index')),
	array('label'=>'Manage Commentaires', 'url'=>array('admin')),
);
?>

<h1>Create Commentaires</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>