<?php
$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Commentaires', 'url'=>array('index')),
	array('label'=>'Create Commentaires', 'url'=>array('create')),
	array('label'=>'View Commentaires', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Commentaires', 'url'=>array('admin')),
);
?>

<h1>Update Commentaires <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>