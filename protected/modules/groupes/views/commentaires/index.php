<?php
$this->breadcrumbs=array(
	'Commentaires',
);

$this->menu=array(
	array('label'=>'Create Commentaires', 'url'=>array('create')),
	array('label'=>'Manage Commentaires', 'url'=>array('admin')),
);
?>

<h1>Commentaires</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
