<?php
$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Commentaires', 'url'=>array('index')),
	array('label'=>'Create Commentaires', 'url'=>array('create')),
	array('label'=>'Update Commentaires', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Commentaires', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Commentaires', 'url'=>array('admin')),
);
?>

<h1>View Commentaires #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date_creation',
		'message',
		'b_membres_id',
		'b_groupes_id',
	),
)); ?>
