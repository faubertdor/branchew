<div class="wide form">

<?php 
		$form=$this->beginWidget('CActiveForm', array(
														'id'=>'groupes-form',
														'enableAjaxValidation'=>true,
								)); 
?>
<div class="span-13" align="left">
	
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoire</p>
	<?php echo $form->errorSummary($model); ?>
	
	
</div>


<div class="span-16" align="left">


	<div class="span-16">
	<?php echo $form->error($model,'nom'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'nom'); ?>
		<?php echo $form->textField($model,'nom',array('size'=>25,'maxlength'=>25)); ?>
		
	</div>





	<div class="span-16">
	<?php echo $form->error($model,'domaine'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'domaine'); ?>
		<?php echo $form->dropDownList($model,'domaine', DomaineActivites::loadItems(),array('prompt'=>'Sélectionnez:')); ?>
	</div>
	
	
	
	
	
	<div class="span-16">
	<?php echo $form->error($model,'groupes_statut'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'groupes_statut'); ?>
		<?php echo $form->dropDownList($model,'groupes_statut', $model->groupesStatut); ?>
	</div>





	<div class="span-16">
	<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="span-16">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('cols'=>55,'rows'=>8));?>
	</div>

</div>
<div class="span-13" align="right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Créer ' : 'Sauvegarder',array('class'=>'button')); 
				echo '  ou  '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl(Yii::app()->user->Id));
		 
		?>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->