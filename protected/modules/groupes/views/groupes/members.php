<?php $this->pageTitle=Yii::app()->name.' | Groupe '.$model->nom;?>
<div class="span-23" align="left">
<div class="span-16">
<h2><?php echo 'Membres du groupe '.$model->nom; ?></h2>
</div>

<div class="span-16">
<?php
		$this->widget('zii.widgets.CListView',
					 							array(
  					  									'dataProvider'=>$members,
   					  									'itemView'=>'_members', 
					 	     						  )
					 );
			
	
?>
</div>
</div>