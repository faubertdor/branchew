<div class="span-12 vcard" align="left">
<div class="span-7" align="left">
<?php echo Groupes::model()->getViewID($data['g_id'], $data['g_avatar'], $data['g_nom'], $data['g_domaine']);?>
</div>
<div class="span-2" align="center">
<strong>
<?php 	

	if($data['g_admin']===Yii::app()->user->Id)
		echo CHtml::link('Administrer',Groupes::model()->getViewAdminUrl($data['g_id']));	
?>
</strong>
</div>
<div class="span-2" align="right">
<?php echo CHtml::encode(Helper::statutViewFormat($data['g_statut']));?>
</div>
</div>