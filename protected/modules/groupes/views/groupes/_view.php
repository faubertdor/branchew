<div class="span-16" align="left">
<div id="join-group" class="span-16" align = "left">
	<?php if(!Yii::app()->user->isGuest) 
			{
				$this->renderPartial('_joinGroup', array('group_id'=>$model->id)); 
			}
	?>
	<?php if($model->isCreator and $model->statut == 'ferme') 
	  		{
	  			if($model->memberRequestCount==0)
	  			echo CHtml::link('Demandes d\'adhésion  ('.$model->memberRequestCount.')',$this->createUrl('groupes/memberRequests',array('group_id'=>$model->id)));
	  			else 
	  			echo CHtml::link('Demandes d\'adhésion  (<strong class="notification">'.$model->memberRequestCount.'</strong>)',$this->createUrl('groupes/memberRequests',array('group_id'=>$model->id)));
	  			
	  			
	  		}
	?>
	<br>
	<?php 
			if($model->isCreator) 
	  		{
	  			echo CHtml::link('Changer l\'image',$this->createUrl('groupes/uploadImage',array('group_id'=>$model->id)));
	  		}
	?>
<br>
</div>
<!-- Groupes info -->
<?php $this->renderPartial('_groupeinfo',array('model'=>$model));?>

<?php if($model->commentCount >= 0) : ?>
<div class="span-16">
<br>
<h5>
<?php 
echo $model->commentCount == 0 ? 'Commentaire (0)' : 'Commentaires ('.$model->commentCount.')'; 
?>
</h5>
<br>
</div>
<?php endif;?>
<div id="add-comment" class="span-15" align="left">	
	<?php $this->renderPartial('_addComment',array(
														'model'=>$model,
														'comment'=>$comment		
														));
		  						
	 ?>
</div>
<div class="span-15" align="left">
<br>
<?php
	

		$this->widget('zii.widgets.CListView',
					 	array(
  					  		'dataProvider'=>$model->comments,
   					  		'itemView'=>'_comments', 
					 	)
					 );
			
?>
</div>
</div>