<?php 
if(isset($groupe))
{
	$this->pageTitle=Yii::app()->name.' | Groupe '.$groupe->nom;
	$this->renderPartial('application.modules.groupes.views.groupes._groupeinfo',array('model'=>$groupe));
	echo '<div class="span-15"><br><br></div>';
}
?>
<!-- Groupe info -->

<div class="span-15 comments">
	<div class="span-2" align="left">
	<?php 
	 echo CHtml::link(CHtml::image($data['chemin_avatar'],$data['prenom'].' '.$data['nom'],array(
																			'width'=>70,
																			'height'=>70,
																		)),ContactMembres::model()->getViewUrl($data['m_id']));
	
	?>
	</div>
	<div class="span-12" align="left">
	<div class="span-6" align="left">
	<h4>
	<?php 
	echo CHtml::link(ucwords($data['prenom']).' '.strtoupper($data['nom']),ContactMembres::model()->getViewUrl($data['m_id']));
	?>
	</h4>
	</div>
	<div class="span-5" align="right">
	<?php echo Yii::app()->dateFormatter->format("dd MMMM y, HH:mm", $data['date_creation']); ?>
	</div>
	<div class="span-12" align="left">
	<?php echo CHtml::encode($data['message']); ?>
	</div>
	
	<div class="span-12" align="right">
	<br>
	<?php 
	
		if($data['m_id']===Yii::app()->user->getID())
			echo CHtml::link('Effacer','#',array('submit'=>array('commentaires/delete'),
												 'params'=>array('id'=>$data['c_id']),
												 'confirm'=>'Voulez-vous vraiment supprimer cet élément ?'));
	?>
	</div>
	</div>
	
	<div class="span-8" align="left">
	<br>
	
	</div>
</div>
<!-- commentaire -->
<?php Yii::app()->clientScript->registerScript($data['c_id'],'FB.XFBML.parse()',CClientScript::POS_END);?>
