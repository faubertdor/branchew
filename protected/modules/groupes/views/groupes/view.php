<?php $this->pageTitle=Yii::app()->name.' | Groupe '.$model->nom;?>
<div class="span-23">
<div class="span-16">
<?php 
$this->renderPartial('_view',array(
											'model'=>$model,
											'comment'=>$comment,
											'suggestGroups'=>$suggestGroups
								  ));

?>
</div>
<!-- end group view -->


<?php 	
		if (!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>