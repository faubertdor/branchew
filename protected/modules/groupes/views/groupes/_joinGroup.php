<?php
$_state = Groupes::model()->isGroupMember($group_id);
if(!$_state) {
 echo CHtml::ajaxButton('Rejoindre le groupe', ContactGroupes::model()->getCreateUrl($group_id),array('update'=>'#join-group'),array('class'=>'button'));
}
else {
	if($_state['etat'] === 'envoye') {
		echo 'Demande en cours de confirmation...';
	}}
?>
