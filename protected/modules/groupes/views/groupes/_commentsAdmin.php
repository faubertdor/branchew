<div class="span-15">
	<div class="span-6" align="left">
	<?php echo Membres::model()->getViewID($data['m_id'], $data['chemin_avatar'], $data['nom'],$data['prenom'],$data['domaine_activite']) ?> 
	</div>
	<div class="span-8" align="left">
	<div class="span-3">
	<br>
	<strong><?php echo Helper::dateViewFormat($data['date_creation']); ?></strong>
	</div>
	<div class="span-3" align="right">
	<br>
	<?php 
	
		if($data['m_id']===Yii::app()->user->getID())
			echo CHtml::link('Effacer','#',array('submit'=>array('commentaires/delete'),
												 'params'=>array('id'=>$data['c_id']),
												 'confirm'=>'Voulez-vous vraiment supprimer cet élément ?'));
	?>
	</div>
	</div>
	
	<div class="span-8" align="left">
	<br>
	<?php echo CHtml::encode($data['message']); ?>
	</div>
		
	<hr>
</div>
<!-- commentaire -->