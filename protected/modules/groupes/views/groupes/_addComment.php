<?php 
$members = $model->members;
$isMember = false;

foreach ($members as $member) {
	if($member['m_id']=== Yii::app()->user->id) {
		$isMember = true;
		break;
	}
}
if($isMember) :
?>

<br><br><br>
<strong>Ajouter un commentaire</strong>

<?php if(Yii::app()->user->hasFlash('commentSubmited')) : ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('commentSubmited'); ?>
</div>
<?php endif; ?>
<?php $this->renderPartial('/commentaires/_form',array(
'model'=>$comment,
)); 
endif;?>