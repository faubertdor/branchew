<div class="span-16">
<div class="span-12 vcard" align="left">
<div class="span-7" align="left">
<?php echo Groupes::model()->getViewID($data['g_id'], $data['g_avatar'], $data['g_nom'], $data['g_domaine']);?>
</div>

<div class="span-4" align="center">
	<?php echo 'Groupe '.CHtml::encode(Helper::statutViewFormat($data['g_statut']));?>
</div>
</div>
</div>