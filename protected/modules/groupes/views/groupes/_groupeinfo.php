<div class="span-15 card">
<div class="span-3" align="left">
<?php if($model->chemin_avatar !== null) 
	echo CHtml::image($model->chemin_avatar,$model->nom,array('width'=>100, 'height'=>80));
?>
</div>

<div class="span-10" align="left">
<?php echo '<h3>'.$model->nom.'</h3>';?>
</div>
<br>
<div class="span-6" align="left">
<?php echo DomaineActivites::itemByInteger($model->domaine).' | '.
		   Helper::statutViewFormat($model->statut);?>
</div>
<div class="fb-like span-2" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-action="recommend" data-font="arial"></div>

<div class="span-10" align="left">
<i>
<?php $owner = $model->owner;
	echo 'Proprietaire : '.CHtml::link($owner['owner_prenom'].' '.$owner['owner_nom'],Yii::app()->createUrl('profil/default/view',array('id'=>$owner['owner_id'])));
?>
</i>
</div>

<div class="span-10" align="left">
<i>
<?php
		if($model->memberCount > 1) echo CHtml::link($model->memberCount.' membres', $this->createUrl('groupes/members', array('group_id'=>$model->id)));
		else echo CHtml::link($model->memberCount.' membre',$this->createUrl('groupes/members', array('group_id'=>$model->id)));
?>
</i>
</div>
</div>

