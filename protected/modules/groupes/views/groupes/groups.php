<?php $this->pageTitle=Yii::app()->name.' | Groupes de discussions';?>
<div class="span-23" align="left">
<div class="span-16">
<div class="span-16">
<h2>Les groupes de discussions</h2>
<?php echo CHtml::link('Creer un groupe',Groupes::model()->getCreateUrl(),array('class'=>'button'));?>
</div>
<div class="span-16" align="left">
<?php
		$this->widget('zii.widgets.CListView',
					 	array(
  					  			'dataProvider'=>$groups,
   					  			'itemView'=>'_itemViewGroups', 
					 	     )
					 );
			
	
?>

</div>
</div>

<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
<!-- end right menu -->
</div>