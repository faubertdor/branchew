<?php

/**
 * This is the model class for table "{{commentaires}}".
 *
 * The followings are the available columns in table '{{commentaires}}':
 * @property string $id
 * @property string $date_creation
 * @property string $message
 * @property string $b_membres_id
 * @property string $b_groupes_id
 */
class Commentaires extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Commentaires the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{commentaires}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message', 'required'),
			);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'auteur' => array(self::BELONGS_TO, 'Membres', 'b_membres_id'),
		);
	}

	
	//Delete commentaire url
	public function getDeleteUrl($id)
	{
		return Yii::app()->createUrl('groupes/commentaires/delete',array('id'=>$id));
	}
	
	public function getComment($id)
	{
		$comment = Yii::app()->db
								->createCommand()
								->select('b_commentaires.id c_id, b_membres.id m_id, date_creation, message,b_groupes_id g_id, nom, prenom, chemin_avatar, domaine_activite')
								->from('b_commentaires')
								->where('b_commentaires.id = '.$id)
								->join('b_membres', 'b_membres.id = b_commentaires.b_membres_id')
								->queryRow();
		return $comment;
	}
	
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		
	}
}