<?php

/**
 * This is the model class for table "{{groupes}}".
 *
 * The followings are the available columns in table '{{groupes}}':
 * @property string $id
 * @property string $nom
 * @property integer $domaine
 * @property string $description
 * @property string $chemin_avatar
 * @property string $date_creation
 * @property string $groupes_statut
 * @property string $groupes_actif
 * @property string $b_membres_id
 */
class Groupes extends CActiveRecord
{
	const STATUT_OUVERT = 'ouvert';
	const STATUT_FERME = 'ferme';
	
	public function getGroupesStatut() {
		
		return array(
			self::STATUT_OUVERT => 'Ouvert',
			self::STATUT_FERME => 'Fermé',
		);
	}
	public function getStatut()
	{
		if($this->groupes_statut == 'ouvert') return 'ouvert';
		else return 'ferme';
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Groupes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{groupes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nom, domaine, groupes_statut', 'required'),
			array('nom', 'unique'),
			array('domaine', 'numerical', 'integerOnly'=>true),
			array('nom, chemin_avatar', 'length', 'max'=>60),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nom, domaine, description, chemin_avatar, date_creation, groupes_statut, groupes_actif, b_membres_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}
	
	//Static function IsAdmin by groupes ID
	public static function getIsAdmin($id)
	{
		$result = Yii::app()->db
								->createCommand()
								->select('id','b_membres_id')
								->from('b_groupes')
								->where(array('and','id ='.$id,'b_membres_id='.Yii::app()->user->getID()))
								->queryRow();
		if($result===false)
			return false;
		else 
			return true;
	}
	
	
	
	public function getIsCreator() 
	{
	 
	 if(Yii::app()->user->id === $this->b_membres_id)
	 	return true;
	 else 
	 	return false;
	}
	
	public function getCommentCount() 
	{
	 	$count = Commentaires::model()->count('b_groupes_id = '.$this->id);
	 	return $count;
	}
	/*
	 *  groups of the current user
	 */
	public static function getMyGroups()
	{
		$result = Yii::app()->db
								->createCommand()
								->select('b_groupes.id g_id, b_groupes.nom g_nom, b_groupes.domaine g_domaine,b_groupes.b_membres_id g_admin, 
										  b_groupes.chemin_avatar g_avatar,b_groupes.groupes_statut g_statut')
								->from('b_contact_groupes')
								->where(array('and','b_contact_groupes.b_membres_id = '.Yii::app()->user->id, 'etat = \'accepte\''))
								->join('b_groupes', 'b_groupes.id = b_contact_groupes.b_groupes_id')
								->queryAll();
								
		return new CArrayDataProvider($result,array('keyField'=>'g_id', 'pagination'=> array('pageSize'=>10)));
	}
	
	public function getGroupesByID($id){
		
		$result = Yii::app()->db
								->createCommand()
								->select('b_groupes.id g_id, b_groupes.nom g_nom, b_groupes.domaine g_domaine, b_groupes.chemin_avatar g_avatar')
								->from('b_contact_groupes')
								->where(array('and','b_contact_groupes.b_membres_id = '.$id, 'etat = \'accepte\''))
								->join('b_groupes', 'b_groupes.id = b_contact_groupes.b_groupes_id')
								->queryAll();
		return $result;								
	}
	
	//Get all the groups available
	public function getGroups()
	{
		$groups=Yii::app()->db->createCommand()
			->select('b_groupes.id g_id, b_groupes.nom g_nom, b_groupes.domaine g_domaine, b_groupes.chemin_avatar g_avatar, b_groupes.groupes_statut g_statut')
			->from('b_groupes')
			->order('g_nom ASC')
			->queryAll();
		
		return $groups;
	}
	
	/*
	 *  add a comment
	 */
	public function addComment($comment) 
	{
		$comment->b_groupes_id = $this->id;
		$comment->b_membres_id = Yii::app()->user->id;
		$comment->date_creation = date('Y-m-d H:i:s');
		
		return $comment->save();
	}
	
	//get owner by group id
	public function getOwnerByGroupId($id)
	{
		$result = Yii::app()->db
								->createCommand()
								->select('b_groupes.nom g_nom, b_membres.id owner_id, b_membres.nom owner_nom, b_membres.prenom owner_prenom, b_membres.email owner_email')
								->from('b_groupes')
								->where('b_groupes.id = '.$id)
								->join('b_membres', 'b_membres.id = b_groupes.b_membres_id')
								->queryRow();
		return $result;
	}
	/*
	 *  owner of the group
	 */
	public function getOwner()
	{
		$result = Yii::app()->db
								->createCommand()
								->select('b_membres.id owner_id, b_membres.nom owner_nom, b_membres.prenom owner_prenom')
								->from('b_groupes')
								->where('b_groupes.id = '.$this->id)
								->join('b_membres', 'b_membres.id = b_groupes.b_membres_id')
								->queryRow();
		return $result;
	}
	/*
	 *  comments of a group
	 */
	public function getComments()
	 {
		
		$comments = Yii::app()->db
								->createCommand()
								->select('b_commentaires.id c_id, b_membres.id m_id, date_creation, message,b_groupes_id, nom, prenom, chemin_avatar, domaine_activite')
								->from('b_commentaires')
								->where('b_groupes_id = '.$this->id)
								->join('b_membres', 'b_membres.id = b_commentaires.b_membres_id')
								->order('date_creation DESC')
								->queryAll();
			
			$dataProvider= new CArrayDataProvider($comments, array('keyField'=>'c_id','pagination'=>array('pageSize'=>10)));
			$dataProvider->sort->defaultOrder='date_creation DESC';
			return $dataProvider;
	}
	/*
	 *  is a group member?
	 */
	public function isGroupMember($id) {
		
		$member = array();
		
		if(!Yii::app()->user->isGuest)
		{
			$userId=Yii::app()->user->getID();
			$member = Yii::app()->db
								->createCommand()
								->select('etat')
								->from('b_contact_groupes')
								->where(array('and','b_groupes_id = '.$id,' b_membres_id = '.$userId))
								->queryRow();
								
		}
		
		return $member;	
	}
	/*
	 *  total members of this group
	 */
	public function getMemberCount() {
		return ContactGroupes::model()->count('b_groupes_id = '.$this->id.' and etat = \'accepte\'');
	}
	/*
	 *  total member's requets for this group 
	 */
	public function getMemberRequestCount()
	{
		return ContactGroupes::model()->count('b_groupes_id = '.$this->id.' and etat = \'envoye\'');
	}
	/*
	 *  member's requets for this group 
	 */
	public function getMemberRequests() 
	{
		$memberRequest = array();
		
		$memberRequest = Yii::app()->db
								->createCommand()
								->select('b_membres.id m_id, b_contact_groupes.b_groupes_id g_id, etat, nom, prenom,chemin_avatar,domaine_activite')
								->from('b_contact_groupes')
								->where(array('and','b_groupes_id = '.$this->id,'etat = \'envoye\''))
								->join('b_membres','b_membres.id = b_contact_groupes.b_membres_id')
								->queryAll();
								
		
		
		return $memberRequest;	
	}
	/*
	 *  members of this group
	 */
	public function getMembers() {
		$members = array();
		
		$members = Yii::app()->db->createCommand()
								->select('b_membres.id m_id, nom, prenom,chemin_avatar,domaine_activite')
								->from('b_contact_groupes')
								->where(array('and','b_groupes_id = '.$this->id,'etat = \'accepte\''))
								->join('b_membres','b_membres.id = b_contact_groupes.b_membres_id')
								->queryAll();
								
		
		
		return $members;			
	}
	
		
	//members email
	public function getMembersEmail(){
		
		$members = array();
		
		$members = Yii::app()->db->createCommand()
								->select('b_membres.id m_id,email')
								->from('b_contact_groupes')
								->where(array('and','b_groupes_id = '.$this->id,'etat = \'accepte\''))
								->join('b_membres','b_membres.id = b_contact_groupes.b_membres_id')
								->queryAll();
								
		
		
		return $members;	
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(		
			'nom' => 'Nom du groupe',
			'domaine' => 'Secteur d\'activite',
			'description' => 'Description',			
			'date_creation' => 'Date Creation',
			'groupes_statut' => 'Type de groupe',
			'b_membres_id' => 'Administrateur',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('domaine',$this->domaine);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('chemin_avatar',$this->chemin_avatar,true);
		$criteria->compare('date_creation',$this->date_creation,true);
		$criteria->compare('groupes_statut',$this->groupes_statut,true);
		$criteria->compare('groupes_actif',$this->groupes_actif,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/*
	 *  @override CActiveRecord::beforeSave()
	 */
	protected function beforeSave() {
		if($this->isNewRecord) {
			$this->date_creation = date('Y-m-j');
			$this->b_membres_id = Yii::app()->user->id;
		
		}
		return parent::beforeSave();
	}

	public function getUpdateUrl($id){
		
		return Yii::app()->createUrl('groupes/groupes/update',array('id'=>$id));
	}
	
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('groupes/groupes/view',array('id'=>$id));
	}
	
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('groupes/groupes/create',array());
	}
	
	public function getMyGroupsUrl(){
		
		return Yii::app()->createUrl('groupes/groupes/myGroups',array());
	}
	
	public function getMemberRequestsUrl($group_id){
		
		return Yii::app()->createUrl('groupes/groupes/memberRequests',array('group_id'=>$group_id));
	}
	
	//admin view url
	public function getViewAdminUrl($id){
		
		return Yii::app()->createUrl('groupes/groupes/viewAdmin',array('id'=>$id));
	}
	
	//Upload image url
	public function getUploadImageUrl($group_id){
		
		return Yii::app()->createUrl('groupes/groupes/uploadImage',array('group_id'=>$group_id));
	}
	
	//Index group
	public function getIndexUrl(){
		
		return Yii::app()->createUrl('groupes/groupes/Index',array());
	}

	//Get info for one group
	public function getInfo($id)
	{
		$groupArray=Yii::app()->db->createCommand()
			->select('id,chemin_avatar,nom,domaine')
			->from('b_groupes')
			->where('id='.$id)
			->queryRow();
	
		if($groupArray==false)
		return array();
		else
		return $groupArray;
	}
	
	//Afficher une vue info pour un groupe
	public function getViewID($id,$avatar,$nom,$domaine){
		
		return 
			   '<div class="span-2" align="right">'.
				CHtml::link(CHtml::image($avatar,CHtml::encode($nom),array(
																			'width'=>65,
																			'height'=>55,
																		)),Groupes::model()->getViewUrl($id)).
																		
				'</div>'.
				'<div class="span-4" align="left">'.
				'<div class="span-4" align="left"><b>'.
				 CHtml::link(CHtml::encode($nom),Groupes::model()->getViewUrl($id)).
				'</b></div>'.
				'<div class="span-4" align="left"><i>'.
				 CHtml::encode(DomaineActivites::itemByInteger($domaine)).
				'</i></div>'.
				'</div>';
				
		
	}
}