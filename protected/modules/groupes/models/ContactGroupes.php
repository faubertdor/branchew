<?php

/**
 * This is the model class for table "{{contact_groupes}}".
 *
 * The followings are the available columns in table '{{contact_groupes}}':
 * @property string $id
 * @property string $b_membres_id
 * @property string $b_groupes_id
 * @property string $etat
 */
class ContactGroupes extends CActiveRecord
{
	
	public function getCreateUrl($id){
		
		return Yii::app()->createUrl('groupes/contactGroupes/create',array('id'=>$id));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @return ContactGroupes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contact_groupes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}
	
	// Suggest group to users
	public function getSuggest()
	{   
		$suggest=array();
		$suggestIndex=array();
		
		$idArray=Yii::app()->db->createCommand()
			->select('id')
			->from('b_groupes')
			->queryAll();
		
		if($idArray!=array())
		{
			$count=Groupes::model()->count();
			$i=0;
			
			foreach($idArray as $value)
			{
				$i++;
				$suggestIndex[$i]=$value['id'];
			}	
			
			$max1=$count/3;
			$x1=rand(1,$max1);
			$x2=rand($max1 + 1,$max1*2);
			$x3=rand($max1*2 + 1,$max1*3);
			
			$suggest[]=$suggestIndex[$x1];
			$suggest[]=$suggestIndex[$x2];
			$suggest[]=$suggestIndex[$x3];
			
			
			return $suggest;
		}
		else
			return array();
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
	}
}