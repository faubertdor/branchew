<?php

class GroupesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','index','update','uploadImage','acceptNewMember','view','members','memberRequests','declineNewMember','myGroups','viewAdmin'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	//Will display all the groups available order by the most popular
	
	public function actionIndex(){

		$groupsArray=Groupes::model()->getGroups();
		
		
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		$groups = new CArrayDataProvider($groupsArray,
	 													array(
	 															'keyField'=>'g_id',
	 														    'pagination'=>array(
	 																 				'pageSize'=>10
	 															   				    )
	 														 )
	 								  );
	 
		 $this->render('groups', array('groups'=>$groups,'suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups));
	}
	
	//Create a new comment
	protected function createComment($group) {
		$comment = new Commentaires;
		
		if(isset($_POST['Commentaires']))
		{
			$comment->attributes = $_POST['Commentaires'];
			
			if($group->addComment($comment))
			{	
				//Send an email to the members of the group
				$members=$group->getMembersEmail();
				$author=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
				
				foreach ($members as $value) 
				{
					
					//Send an email to notify $destinataire
					if($value['email']!=$author['email'])
					{
						$mail= new YiiMailMessage;
						$mail->view='commentaires';
						$mail->setBody(array('nom'=>$author['nom'],'prenom'=>$author['prenom'],'id'=>$author['id'],'group'=>$group->nom,'message'=>$comment->message),'text/html');
						$mail->addTo($value['email']);
						$mail->from='branchew-notification@branchew.com';
						$mail->subject=$group->nom;
						Yii::app()->mail->send($mail);						
					}
				}
				
				Yii::app()->user->setFlash('commentSubmited',"Ton commentaire a été ajouté avec succès." );
				$this->refresh();
			}		
		}
		
		return $comment;
	}
		

	//administration by the owner
	public function actionViewAdmin($id){
		
		if(isset($id) and is_numeric($id)){
			
			$group=$this->loadModel($id);
			$_SESSION['group_owner']=$group->b_membres_id;
			
			if($group->isCreator){
				
				$comment = $this->createComment($group);
				//Suggest group
				$suggestGroups=ContactGroupes::model()->getSuggest();
				
				$this->render('viewAdmin',array(
												 'model'=>$group,
												 'comment'=>$comment,
												 'suggestGroups'=>$suggestGroups
											   ));
			}
			else 
				throw new CHttpException(400,'Requête invalide.');
		}
		else
			throw new CHttpException(404,'La page demandée n’existe pas.');
		
	}
	
	//Decline member in the groupe
	public function actionDeclineNewMember($group_id, $member_id)
	{
		$model = ContactGroupes::model()->find('b_groupes_id ='.$group_id.' and b_membres_id ='.$member_id);
		$model->etat = 'refuse';
		
		if($model->save())
			$this->redirect(Groupes::model()->getMemberRequestsUrl($group_id));
		
	}
	
	//Accept member in the group
	public function actionAcceptNewMember($group_id, $member_id)
	{
		$model = ContactGroupes::model()->find('b_groupes_id ='.$group_id.' and b_membres_id ='.$member_id);
		$model->etat = 'accepte';
		
		if($model->save())
		{
			
		 			$owner=Groupes::model()->getOwnerByGroupId($group_id);
					$requestor=Membres::model()->getViewIdInfo($member_id);
				
					//Send an email to notify $destinataire
					$mail= new YiiMailMessage;
					$mail->view='adhesionAccepte';
					$mail->setBody(array('g_nom'=>$owner['g_nom'],'nom'=>$owner['owner_nom'],'prenom'=>$owner['owner_prenom'],'id'=>$owner['owner_id']),'text/html');
					$mail->addTo($requestor['email']);
					$mail->from=$owner['owner_email'];
					$mail->subject='Adhésion au groupe '.$owner['g_nom'];
					Yii::app()->mail->send($mail);
		 			
			$this->redirect(Groupes::model()->getMemberRequestsUrl($group_id));
		}
		
	}
	
	
	//Accept member request
	public function actionMemberRequests($group_id)
	{
		$model = $this->loadModel($group_id);
		$memberRequests = new CArrayDataProvider($model->memberRequests,
	 																	array(
	 																			'keyField'=>'m_id',
	 																			'pagination'=>array(
	 																								 'pageSize'=>10
	 																								)
	 										 								 )
	 								  			);
	 								  			
		$this->render('memberRequests',array('memberRequests'=>$memberRequests,'model'=>$model));
	}
	
	
	//Get the members from the group
	public function actionMembers($group_id)  
	{
	 $model = $this->loadModel($group_id);
	 $members = new CArrayDataProvider($model->members,
	 									array('keyField'=>'m_id',
	 											'pagination'=>array(
	 																 'pageSize'=>10
	 															   )
	 										 )
	 								  );
	 
	 $this->render('members', array('members'=>$members, 'model'=>$model));
	}
	
	
	//My groups request
	public function actionMyGroups()
	{
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		
		$my_groups = Groupes::model()->getMyGroups();
		$this->render('myGroups',array('my_groups'=>$my_groups,'suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups));
	}
	
	//View the group
	public function actionView($id)
	{
		
		$group = $this->loadModel($id);
		$comment = $this->createComment($group);
		$_SESSION['group_owner']=$group->b_membres_id;
		
		
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		
		
		$this->render('view',array(
									'model'=>$group,
									'comment'=>$comment,
									'suggestGroups'=>$suggestGroups,
									'suggestMembres'=>$suggestContacts,
								  ));
		
		
	}
    /**
     * Upload a picture for the group
     */
	public function actionUploadImage($group_id)
	{
		$group = Groupes::model()->findByPk($group_id);
		$dir=Yii::getPathOfAlias('application.modules.groupes.assets.images');
		
		$newImage=new UploadImages();
		
		if(isset($_POST['UploadImages'])){
			
			$newImage->attributes=$_POST['UploadImages'];
			$this->performAjaxValidation($newImage);
			
			$file=CUploadedFile::getInstance($newImage,'image');
			
			if($newImage->validate()){				
				
				$file->saveAs($dir.'/'.$group->id);
				$group->chemin_avatar=Yii::app()->assetManager->publish($dir.'/'.$group->id);
				$group->save();
				
				$this->redirect(array('view','id'=>$group->id));
			}
			
		}
		
		$this->render('uploadImage',array(
			'imageProfil'=>$newImage,
			'group_id'=>$group_id
		));
		
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Groupes;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Groupes']))
		{
			$model->attributes=$_POST['Groupes'];
			$dir=Yii::getPathOfAlias('application.modules.groupes.assets.images');
			$model->chemin_avatar=Yii::app()->assetManager->publish($dir.'/0');
		 		
			if($model->save()) 
			{
				$ownerGroup = new ContactGroupes;
				$ownerGroup->b_membres_id = Yii::app()->user->id;
		 		$ownerGroup->etat = 'accepte';
				$ownerGroup->b_groupes_id = $model->id;
				$ownerGroup->save();
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();

		//Suggest contacts to user
		$suggestContacts=array();
 		$suggestId=array();
 		$suggestId=ContactMembres::model()->getSuggestIndex();

 		if($suggestId!=false)
 		{	
 											
			foreach ($suggestId as $value)
			{
				$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
			}
		}
		$this->render('create',array(
										'model'=>$model,
										'suggestGroups'=>$suggestGroups,
										'suggestMembres'=>$suggestContacts,
									));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
	
		if($model->isCreator)
		{
			// Uncomment the following line if AJAX validation is needed
		 	$this->performAjaxValidation($model);

			if(isset($_POST['Groupes']))
			{
				$model->attributes=$_POST['Groupes'];
				$model->description = $_POST['description'];
				if($model->save())
					$this->redirect(array('viewAdmin','id'=>$model->id));
			}

			$this->render('update',array(
											'model'=>$model,
										));
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	
	}
	
	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Groupes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandée n’existe pas.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='groupes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
