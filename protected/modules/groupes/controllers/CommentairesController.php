<?php

class CommentairesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'actions'=>array('delete'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('delete','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
			
		if(Yii::app()->request->isPostRequest)
		{	
			if(isset($_POST['id']))
			{
				$id=$_POST['id'];
				$model=$this->loadModel($id);
			
				if($model->b_membres_id===Yii::app()->user->getID() or Groupes::getIsAdmin($model->b_groupes_id))
				{
					$group_id=$model->b_groupes_id;
					$model->delete();
					$this->redirect(Groupes::model()->getViewUrl($group_id));
				
				}
				else 
					throw new CHttpException(400,'Requête invalide.');
			}
				
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	//Public view for single comment because of Facebook like.	
	public function actionView($id)
	{
		$comment=Commentaires::model()->getComment($id);
		$groupe=Groupes::model()->findByPk($comment['g_id']);

		$this->render('application.modules.groupes.views.groupes._comments',array('data'=>$comment,'groupe'=>$groupe));
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Commentaires::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandée n’existe pas.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='commentaires-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
