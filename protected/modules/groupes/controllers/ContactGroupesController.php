<?php

class ContactGroupesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('view','create'),
				'users'=>array('?'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	private function isOpenGroup($id) {
		
		$groupe = Groupes::model()->findByPk($id);
		
		if($groupe->groupes_statut === 'ouvert')
			return true;
		else 
			return false;
	}
	
	public function actionCreate($id)
	{
		if(isset($id) and is_numeric($id)) {
		
			if(Yii::app()->request->isAjaxRequest){
		 		$model=new ContactGroupes;
		 		$model->b_membres_id = Yii::app()->user->id;
		 		$model->b_groupes_id = $id;
		 		
		 		if($this->isOpenGroup($id)) {
		 			$model->etat = 'accepte';
		 		}
		 		
		 		
		 		if($model->save())
		 		{
		 			
		 			$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
					$owner=Groupes::model()->getOwnerByGroupId($id);
				
					//Send an email to notify $destinataire
					$mail= new YiiMailMessage;
					$mail->view='demandeAdhesion';
					$mail->setBody(array('g_nom'=>$owner['g_nom'],'nom'=>$expediteur['nom'],'prenom'=>$expediteur['prenom'],'id'=>$expediteur['id']),'text/html');
					$mail->addTo($owner['owner_email']);
					$mail->from=$expediteur['email'];
					$mail->subject='Demande d\'adhésion au groupe '.$owner['g_nom'];
					Yii::app()->mail->send($mail);
		 			
		 		  }
		 		  
		 		
		 		echo $this->isOpenGroup($id) ?  'OK. Vous êtes  membre du groupe.' : 'Votre demande a été envoyée!';
		 		Yii::app()->end();
			}
			else 
		 		throw new CHttpException(400,'Requête invalide.');
		
		}
		
	}

	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ContactGroupes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandée n’existe pas.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='contact-groupes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
