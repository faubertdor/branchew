<div class="span-15 card" align="left">
<div class="span-3" align="right">

<?php echo CHtml::image($entreprise['chemin_avatar'],$entreprise->nom,array(
																								'width'=>100,
																								'height'=>90
						));
?>
</div>
<h3><?php echo CHtml::encode($entreprise->nom)?></h3>
<?php echo CHtml::encode(Pays::itemByInteger($entreprise->pays)).', '.Villes::itemByInteger(CHtml::encode($entreprise->ville));?>
<br>
<?php echo CHtml::encode(Helper::secteurViewFormat($entreprise->type_entreprise));?>
</div>

<div class="span-16" align="left">
<br>
<br>
<br>
<div class="span-10" align="left">
<h2>DESCRIPTION</h2>
</div>
</div>
<div class="span-16" align="left">
<?php echo $entreprise->description;?>
</div>

<div class="span-16" align="left">
<div class="span-10" align="left">
<h2>INFORMATIONS ADDITIONNELLES</h2>
</div>
</div>
<div class="span-15" align="left">
<div class="span-4" align="right">
<strong>Nombre d’employée </strong>	
</div>
<div class="span-10" align="left">
<?php echo CHtml::encode($entreprise->nombre_employe);?>
</div>

<div class="span-4" align="right">
<strong>Adresse </strong>	
</div>
<div class="span-10" align="left">
<?php echo CHtml::encode($entreprise->adresse);?>
</div>

<div class="span-4" align="right">
<strong>Telephone </strong>	
</div>
<div class="span-10" align="left">
<?php echo CHtml::encode($entreprise->telephone1);?>
</div>

<div class="span-4" align="right">
<strong>Site web </strong>	
</div>
<div class="span-10" align="left">
<?php echo CHtml::link(CHtml::encode($entreprise->siteweb),$entreprise->siteweb);?>
</div>
</div>