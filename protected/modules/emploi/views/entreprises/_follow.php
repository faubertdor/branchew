<?php

	if(!SuivreEntreprises::model()->isFollowing($id))
		echo ' '.CHtml::ajaxButton('Suivre',SuivreEntreprises::model()->getFollowUrl($id),array('update'=>'#follow'));
	else 	
		echo 'Vous suivez cette entreprise.';