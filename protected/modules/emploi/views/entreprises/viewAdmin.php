<div class="span-23">
<div class="span-18" align="left">

<?php $this->renderPartial('_entreprise',array('entreprise'=>$entreprise)); ?>

</div>

<div class="span-4 vmenu" align="left">
<div id="infobox" class="span-5">
<div class="title">
Administration
</div>
<div class="span-4 element" align="left">
<?php echo CHtml::link('Voir le profil',Entreprises::model()->getViewUrl($entreprise->id));?>
</div>
<div class="span-4 element" align="left">
<?php echo CHtml::link('Voir les admin',AdminEntreprises::model()->getViewUrl($entreprise->id));?>
</div>
<div class="span-4 element" align="left">
<?php echo CHtml::link('Modifier le logo',Entreprises::model()->getUploadLogoUrl($entreprise->id));?>
</div>
<div class="span-4 element" align="left">
<?php echo CHtml::link('Modifier les info',Entreprises::model()->getUpdate($entreprise->id));?>
</div>
<div class="span-4 element" align="left">
<?php echo CHtml::link('Ajouter un admin',AdminEntreprises::model()->getAddUrl($entreprise->id));?>
</div>
<div class="span-4 element" align="left">
<?php 
	
	if($entreprise->actif==='true')
		echo CHtml::link('Desactiver profil',Entreprises::model()->getDisableUrl($entreprise->id),array('confirm'=>'Voulez-vous vraiment désactiver ce profil ?'));
	else 
		echo CHtml::link('Activer profil',Entreprises::model()->getEnableUrl($entreprise->id),array('confirm'=>'Voulez-vous vraiment activer ce profil ?'));
?>

</div>
</div>
</div>
<!-- end vmenu -->

</div>

