<div class="span-23">
<div class="form">
<div class="span-18"><h2>Charger un logo</h2></div>
<div class="span-18">
	<?php echo CHtml::beginForm('','post',array('enctype'=>'multipart/form-data'))?>
	<?php echo CHtml::error($logo, 'image')?>
	<?php echo CHtml::activeFileField($logo, 'image')?>
</div>
<div class="span-18">
<div class="span-2">
	<?php echo CHtml::submitButton('Charger')?>
	<?php echo CHtml::endForm()?>
</div>
<div class="span-2">
<?php echo 'ou '.CHtml::link('Annuler',Entreprises::model()->getViewAdminUrl($id));?>
</div>
</div>
</div>
</div>