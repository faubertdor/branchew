<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'entreprises-form',
														'enableAjaxValidation'=>true,
													));
?>

<div class="span-18" align="left">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
</div>

<div class="span-18" align="left">
	
		
	<div class="span-18">
	<?php echo $form->error($model,'nom'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'nom'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->textField($model,'nom',array('size'=>25,'maxlength'=>25)); ?>
	</div>
	
	
	
	
	
	<div class="span-18">
		<?php echo $form->error($model,'type_entreprise'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'type_entreprise'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->dropDownList($model,'type_entreprise',array('prive'=>'Privé','public'=>'Public','ong'=>'ONG'),array('prompt'=>'Selectionnez :')); ?>
	</div>





	
	<div class="span-18">
	<?php echo $form->error($model,'nombre_employe'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'nombre_employe'); ?>
	</div>
	
	<div class="span-8">
		<?php echo $form->dropDownList($model,'nombre_employe',array(
																	 '5-10'=>'5-10',
																	 '10-50'=>'10-50',
																	 '50-200'=>'50-200',
																	 '500'=>'500 +'
																    ),
																array('prompt'=>'Selectionnez :')); 
		?>	
	</div>







	
	<div class="span-18">
	<?php echo $form->error($model,'pays'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'pays'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->dropDownList($model,'pays',
									   Pays::loadItems(),
									  	 array(
									  	 		'prompt'=>'Selectionnez un pays:',
												'ajax' => array(
												'type'=>'POST', //request type
												'url'=>Controller::createUrl('requestCities'), //url to call.
												
												'update'=>'#'.CHtml::activeId($model,'ville') //selector to update
												
										))); 
		?>
	</div>
	
	
	
	
	
	
	
	<div class="span-18">
	<?php echo $form->error($model,'ville'); ?>
	</div>
	<div class="span-4">
		
		<?php
				 //Ville
				$ville=array();
				$ville=array($model->ville=>Villes::itemByInteger($model->ville));
		
				echo $form->labelEx($model,'ville');
		?>
	</div>
	
	<div class="span-8">
		<?php echo $form->dropDownList($model,'ville',$ville); ?>
	</div>	
		
	
	
	
	
	
	
	<div class="span-18">
	<?php echo $form->error($model,'adresse'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'adresse'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->textArea($model,'adresse',array('rows'=>3, 'cols'=>40)); ?>
	</div>
	
	
	
	
	
	<div class="span-18">
	<?php echo $form->error($model,'telephone1'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'telephone1'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->textField($model,'telephone1',array('size'=>25,'maxlength'=>15)); ?>
		
	</div>




	<div class="span-18">
	<?php echo $form->error($model,'siteweb'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'siteweb'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->textField($model,'siteweb',array('size'=>25,'maxlength'=>60)); ?>	
	</div>







	<div class="span-18">
	<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'email'); ?>
	</div>
	<div class="span-8">
		<?php echo $form->textField($model,'email',array('size'=>25,'maxlength'=>60)); ?>
	</div>






	<div class="span-18">
	<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="span-4">
		<?php echo $form->labelEx($model,'description'); ?>
	</div>
	<div class="span-10">
		<textarea id="description" name="description" ><?php echo $model->description;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('description',
				{
					toolbar : 'Basic'
				} );
			</script>
		<?php echo $form->error($model,'info_additionel'); ?>

	</div>
</div>
	
<div class="span-14" align="right">
		<?php 
			  echo CHtml::submitButton($model->isNewRecord ? 'Créer':'Sauvegarder',array('class'=>'button')); 
			  echo '  ou  '.CHtml::link('Annuler',ContactMembres::model()->getIndexUrl());
		 ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->