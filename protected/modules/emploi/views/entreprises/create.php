<?php $this->pageTitle=Yii::app()->name.' | Créer le profil d \'une entreprise'; ?>
<div class="span-23">
<div class="span-16" align="left">
<h2>Créer le profil d'une entreprise</h2>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
<!-- end form -->


<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>