<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom')); ?>:</b>
	<?php echo CHtml::encode($data->nom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chemin_avatar')); ?>:</b>
	<?php echo CHtml::encode($data->chemin_avatar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_entreprise')); ?>:</b>
	<?php echo CHtml::encode($data->type_entreprise); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domaine_activite')); ?>:</b>
	<?php echo CHtml::encode($data->domaine_activite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_employe')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_employe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pays')); ?>:</b>
	<?php echo CHtml::encode($data->pays); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ville')); ?>:</b>
	<?php echo CHtml::encode($data->ville); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adresse')); ?>:</b>
	<?php echo CHtml::encode($data->adresse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephone1')); ?>:</b>
	<?php echo CHtml::encode($data->telephone1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephone2')); ?>:</b>
	<?php echo CHtml::encode($data->telephone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_creation')); ?>:</b>
	<?php echo CHtml::encode($data->date_creation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actif')); ?>:</b>
	<?php echo CHtml::encode($data->actif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profil_type')); ?>:</b>
	<?php echo CHtml::encode($data->profil_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('siteweb')); ?>:</b>
	<?php echo CHtml::encode($data->siteweb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_membres_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_membres_id); ?>
	<br />

	*/ ?>

</div>