<?php $this->pageTitle=Yii::app()->name.' | Modifier le profil d\'une entreprise'; ?>
<div class="span-23">
<div class="span-16" align="left">
<h2>Modifier le profil d'une entreprise</h2>
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>


<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>