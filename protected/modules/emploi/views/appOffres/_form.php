<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'app-offres-form',
	'enableAjaxValidation'=>true,
)); ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'lettre_motivation'); ?>
		<textarea id="lettre_motivation" name="lettre_motivation" ><?php echo $model->lettre_motivation;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('lettre_motivation',
				{
					toolbar : 'Basic'
				} );
			</script>
		<?php echo $form->error($model,'lettre_motivation'); ?>
	</div>

	<?php echo $form->hiddenField($model,'b_offre_emp_id');?>
		
	<div class="row buttons">
		<?php echo CHtml::submitButton('Envoyer'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->