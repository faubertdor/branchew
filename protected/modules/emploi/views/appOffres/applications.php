<?php $this->pageTitle=Yii::app()->name.' | Liste des applications'?>
<div class="span-23">
<div class="span-5" align="left">
<?php $this->renderPartial('application.modules.emploi.views.offreEmp._left_menu');?>
</div>
<div class="span-16">
<h2>Liste des applications : <?php echo $offre_emp->titre.' - '.Pays::itemByInteger($offre_emp->pays).', '.Villes::itemByInteger($offre_emp->ville);?> </h2>
</div>
<div class="span-16">
	<?php 
			
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$applications,
   					  'itemView'=>'_itemView',   // refers to the partial view named '_itemView'
   
					 ));
	
	
	?>
</div>
</div>