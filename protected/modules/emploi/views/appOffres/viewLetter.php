<?php $this->pageTitle=Yii::app()->name.' - Lettre de motivation'?>
<div class="container" align="left">
<div class="span-22">
<div class="span-5" align="left">
<strong class="large">
<u>
<?php echo CHtml::link('Retour',AppOffres::model()->getApplicationsUrl($letter['offre_emp_id']));?>
</u>
</strong>
<?php $this->renderPartial('application.modules.emploi.views.offreEmp._left_menu');?>
</div>

<div class="span-15">
<strong class="medium-green">Lettre de motivation pour le poste :  </strong>
<strong class="medium"><u>
<?php 
echo CHtml::link($letter['titre'].' - '.Pays::itemByInteger($letter['pays']).', '.Villes::itemByInteger($letter['ville']),
				OffreEmp::model()->getViewUrl($letter['offre_emp_id'])); 
?>
</u></strong>
<hr>

</div>

<div class="span-15">
	<?php 
			
		
	echo $letter['lettre_motivation'];
	
	?>
</div>
</div>
<div class="span-22" align="right">
<br>
<strong class="large">
<u>
<?php echo CHtml::link('Retour',AppOffres::model()->getApplicationsUrl($letter['offre_emp_id']));?>
</u>
</strong>
</div>
</div>