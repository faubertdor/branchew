<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_membres_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_membres_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('b_offre_emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->b_offre_emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lettre_chemin')); ?>:</b>
	<?php echo CHtml::encode($data->lettre_chemin); ?>
	<br />


</div>