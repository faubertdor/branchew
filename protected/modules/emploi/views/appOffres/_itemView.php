<div class="span-16 vcard">
<div class="span-7">
<?php 
	
	echo Membres::model()->getViewID($data['membres_id'], $data['chemin_avatar'], $data['nom'], $data['prenom'], $data['domaine_activite']);

?>
</div>
<div class="span-3">
<br>
<?php	echo 'Le '.Helper::dateViewFormat($data['date']);?>
</div>
<div class="span-3">
<br>
<?php echo CHtml::link('Lettre de motivation',AppOffres::model()->getViewLetterUrl($data['app_id']));?>
</div>
</div>