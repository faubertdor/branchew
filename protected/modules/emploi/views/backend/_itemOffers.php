<div class="container">
<div class="span-22">

<div class="span-8">
<div class="span-8">
<strong class="medium">
<?php 
echo CHtml::link('<u>'.$data['titre'].'</u>',OffreEmp::model()->getViewUrl($data['id']));
?>
</strong>
</div>

<div class="span-8">
<strong class="medium-green">
<?php 
if($data['b_entreprises_id']!=0)
{
	$entreprise=Entreprises::model()->entrepriseInfoById($data['b_entreprises_id']);
	echo $entreprise['nom'];
}
else 
	echo $data['entreprise'];
?>
</strong>
<?php 
echo ' - ';
echo Pays::itemByInteger($data['pays']).', '.Villes::itemByInteger($data['ville']);
?>
</div>
</div>


<div class="span-5">
<div class="span-4">
<?php 
echo '<strong>date de publication : </strong>'.Helper::dateViewFormat($data['date_debut']);
?>
</div>

<div class="span-4">
<?php 
echo '<strong>date limite : </strong>'.Helper::dateViewFormat($data['date_fin']);
?>
</div>
</div>

<div class="span-4">
<strong>
<?php
echo CHtml::link('Valider','#',array('submit'=>array('backend/validate'),'params'=>array('id'=>$data['id']))); 
?>
</strong>
</div>

<div class="span-4">
<strong>
<?php
echo CHtml::link('Rejeter','#',array('submit'=>array('backend/reject'),'params'=>array('id'=>$data['id']))); 
?>
</strong>
</div>

<hr>
</div>
</div>