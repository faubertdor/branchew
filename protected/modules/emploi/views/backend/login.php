<?php
$this->pageTitle='Backend - Vérification de votre identité - Backend';
?>
<div class="container">
<div class="span-22" align="center">
<h2>Backend - Vérification de votre identité - Backend</h2><hr>
</div>

<div class="span-18" >
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton('Authentifier'); ?>
	</div>
	

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>