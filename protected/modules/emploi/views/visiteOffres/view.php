<?php $this->pageTitle=Yii::app()->name.' | Visites offre d\'emploi'?>

<div class="span-23">
<div class="span-5" align="left">
<br>
<?php $this->renderPartial('application.modules.emploi.views.offreEmp._left_menu');?>
</div>

<div class="span-16">
<h2>Liste des visites : <?php echo $offreArray['titre'].' - '.Pays::itemByInteger($offreArray['pays']).', '.Villes::itemByInteger($offreArray['ville']);?></h2>
</div>

<div class="span-16">
	<?php 
			
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$dataProvider,
   					  'itemView'=>'_itemView',   // refers to the partial view named '_itemView'
   
					 ));
	
	
	?>
</div>
</div>