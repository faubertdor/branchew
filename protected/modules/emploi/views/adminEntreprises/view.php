<?php $this->pageTitle=Yii::app()->name.' | Liste des administrateurs'; ?>
<div class="span-23" align="left">
<div class="span-18" align="left">
</div>
<div class="span-18" align="left">
<h2><?php echo $entreprise['nom'].', liste des administrateurs ';?></h2>
</div>
<div class="span-18">	
<?php echo CHtml::link('Ajouter',AdminEntreprises::model()->getAddUrl($entreprise['entrepriseId']),array('class'=>'button'));?>
</div>
<div class="span-18" align="left">	

	<?php
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$adminData,
   					  'itemView'=>'_itemView',   // refers to the partial view named '_itemView'
   
					 ));
	?>
</div>
</div>