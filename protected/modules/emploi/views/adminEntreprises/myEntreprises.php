<?php $this->pageTitle=Yii::app()->name.' | Mes entreprises'; ?>
<div class="span-23" align="left">
<div class="span-16" align="left">
<h2>Mes entreprises</h2>
	<?php
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$myEntreprises,
   					  'itemView'=>'_itemViewMyEntreprises',   // refers to the partial view named _itemViewMyEntreprises
   
					 ));
			
	
	?>
</div>
<!-- end listing -->

<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>