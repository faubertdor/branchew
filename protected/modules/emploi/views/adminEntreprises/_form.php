<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
													 'id'=>'admin-entreprises-form',
													 'enableAjaxValidation'=>true,
												   )); 

?>
<div class="span-18">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
</div>
	
	<div class="span-18">
	<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="span-2">
		<?php echo $form->labelEx($model,'email'); ?>
	</div>
	<div class="span-12">
		<?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>40)); ?>
	</div>
	<div class="span-10">
	<div class="span-9"  align="right">
		<?php echo CHtml::submitButton('Ajouter',array('class'=>'button')); ?>
	
		<?php echo ' ou '.CHtml::link('Annuler',AdminEntreprises::model()->getViewUrl($id));?>
	</div>
	</div>
	

<?php $this->endWidget(); ?>

</div><!-- form -->