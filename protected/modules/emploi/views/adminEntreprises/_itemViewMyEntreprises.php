<div class="span-13 vcard" align="left">
<div class="span-7" align="left">

<?php 

	echo Entreprises::model()->getViewInfo($data['entrepriseId'], $data['chemin_avatar'], $data['nom'],$data['type_entreprise']);									 
?>

</div>
<div class="span-3" align="left">

<?php echo CHtml::link('Administrer',Entreprises::model()->getViewAdminUrl($data['entrepriseId']))?>
</div>
<div class="span-2" align="left">
<?php 

	if($data['actif']==='true')
		echo 'Actif';
	else 	
		echo 'Non Actif';


?>
</div>
</div>
