<div class="span-15 newscard" align="left">
<div class="span-14">
<strong class="medium">
<?php 
echo CHtml::link('<u>'.$data['titre'].'</u>',OffreEmp::model()->getViewUrl($data['id']));
?>
</strong>
</div>

<div class="span-10">
<strong class="medium-green">
<?php 
if($data['b_entreprises_id']!=0)
{
	$entreprise=Entreprises::model()->entrepriseInfoById($data['b_entreprises_id']);
	echo $entreprise['nom'];
}
else 
	echo $data['entreprise'];
?>
</strong>
<?php 
echo ' - ';
echo Pays::itemByInteger($data['pays']).', '.Villes::itemByInteger($data['ville']);
?>
</div>


<div class="span-10">
<?php 
echo '<strong>date limite : </strong>'.Yii::app()->dateFormatter->format("dd MMMM y", $data['date']);
?>
</div>
</div>