<?php $this->pageTitle=Yii::app()->name . ' | Offre d\'emploi';?>
<div class="span-23">
<div class="span-7" align="center">
<span class="button">
<?php 
echo CHtml::link('Soumettre a l\'equipe de Branchew','#',array('submit'=>array('offreEmp/submit'),'params'=>array('id'=>$model->id)));
?>
</span>
</div>

<div class="span-5" align="center">
<span class="button">
<?php 
echo CHtml::link('Sauvegarder dans brouillons','#',array('submit'=>array('offreEmp/saveToDraft'),'params'=>array('id'=>$model->id)));
?>
</span>
</div>
<div class="span-15">
<br>
<hr>
</div>

<div class="span-16">
<?php 
$this->renderPartial('_view',array('model'=>$model));
?>
</div>
<!-- end content -->


<?php $this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));?>
</div>
