<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offre-emp-form',
	'enableAjaxValidation'=>true,
)); ?>

	<div class="span-18" align="left">
	<p class="note">Les champs avec <span class="required">*</span> sont obligatoires.</p>
	<?php echo $form->errorSummary($model); ?>
	</div>

<div class="span-18" align="left">
	<div class="span-18">
	<?php echo $form->error($model,'titre'); ?>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'titre'); ?>
		<?php echo $form->textField($model,'titre',array('size'=>35,'maxlength'=>60)); ?>
	</div>
	
	<div class="span-18">
	<?php echo $form->error($model,'entreprise'); ?>
	</div>
	<div class="span-7" align="right">
	<strong class="medium">
	<i>
		<?php 
		echo CHtml::link('<u>Créer d’abord le profil de votre entreprise</u>',Entreprises::model()->getCreateUrl());
		?>
	</i>
	</strong>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'entreprise'); ?>
	
		<?php 
	/*
			$this->widget('CAutoComplete', array(
													'name'=>'autocomplete',
													'url'=>$this->createUrl('offreEmp/suggestEntreprise'),
													'htmlOptions'=>array('size'=>20),
													'delay'=>500,
													'max'=>10,
													'methodChain'=>".result(function(event,item){\$(\"#".CHtml::activeId($model,'b_entreprises_id')."\").val(item[1]);})",
											
										)
   
				 );
	
	
		
		
		*/
		
		$this->widget('CAutoComplete', array(
												'model'=>$model,
												'attribute'=>'entreprise',
												'url'=>$this->createUrl('offreEmp/suggestEntreprise'),
												'htmlOptions'=>array('size'=>20),
												'delay'=>500,
												'max'=>10,
												'methodChain'=>".result(function(event,item)
												{
													
													if(item[1]!=0)
													{
														$(\"#".CHtml::activeId($model,'b_entreprises_id')."\").val(item[1]);
													}
													else
													{
														a=0;
														$(\"#".CHtml::activeId($model,'b_entreprises_id')."\").val(a);
														
													}
													
												})",											
											)
   
				 );
				 
		echo $form->hiddenField($model,'b_entreprises_id');
	?>	

	</div>

	<div class="span-18">
	<?php echo $form->error($model,'type'); ?>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array('temps_plein'=>'Temps Plein','temps_partiel'=>'Temps Partiel'),array('prompt'=>'Selectionnez:')); ?>
	</div>
	
	
	<div class="span-18">
	<?php echo $form->error($model,'domaine_activite'); ?>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'domaine_activite'); ?>
		<?php echo $form->dropDownList($model,'domaine_activite',DomaineActivites::loadItems(),array('prompt'=>'Selectionnez:')); ?>
	</div>



	<div class="span-18">
	<?php echo $form->error($model,'pays'); ?>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'pays'); ?>
		<?php echo $form->dropDownList($model,'pays',
									   Pays::loadItems(),
									  	 array(
									  	 		'prompt'=>'Selectionnez un pays:',
												'ajax' => array(
												'type'=>'POST', //request type
												'url'=>Controller::createUrl('requestCities'), //url to call.
												
												'update'=>'#'.CHtml::activeId($model,'ville') //selector to update
												//'data'=>'js:javascript statement' 
												//leave out the data key to pass all form values through
										))); 
		?>
	</div>

 	<div class="span-18">
 	<?php echo $form->error($model,'ville'); ?>
 	</div>
 	<div class="span-18">
		<?php
				 //Ville
				$ville=array();
				$ville=array($model->ville=>Villes::itemByInteger($model->ville));
				echo $form->labelEx($model,'ville');
		 ?>
		<?php echo $form->dropDownList($model,'ville',$ville); ?>
	</div>
	
	
	
	<div class="span-18">
	<?php echo $form->error($model,'description'); ?>
	</div>
	<div class="span-3">
		<?php echo $form->labelEx($model,'description'); ?>
	</div>
	<div class="span-10">
	<textarea id="description" name="description" ><?php echo $model->description;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('description',
				{
					toolbar : 'Basic'
				} );
			</script>
		<br>
	</div>
	
	
	<div class="span-18">
	<?php echo $form->error($model,'qualification'); ?>
	</div>
	<div class="span-3">
		<?php echo $form->labelEx($model,'qualification'); ?>
	</div>
	<div class="span-10">
		<textarea id="qualification" name="qualification" ><?php echo $model->qualification;?></textarea>
			<script type="text/javascript">
				CKEDITOR.replace('qualification',
				{
					toolbar : 'Basic'
				} );
			</script>
	<br>
	</div>


	<div class="span-18">
	<?php echo $form->error($model,'application'); ?>
	</div>
	<div class="span-18">
		<?php echo $form->labelEx($model,'application'); ?>
	<?php echo $form->dropDownList($model,'application',array('email'=>'Email','siteweb'=>'Site web'),array('prompt'=>'Selectionnez:'));?>
	</div>
	
	<div class="span-18">
	<hr>
	</div>


	<div class="span-10">
	<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="span-5" align="right">
		<strong><?php echo 'Indiquez une addresse email pour recevoir les notifications'; ?></strong>
	</div>
	<div class="span-6" align="left">
		<?php echo $form->textField($model,'email',array('size'=>25,'maxlength'=>100)); ?>
	</div>
	</div>

	<div class="span-10" align="center">
	<strong class="medium">OU</strong>
	</div>

	<div class="span-10">
	<?php echo $form->error($model,'siteweb_entreprise'); ?>
	</div>
	<div class="span-5" align="right">
		<strong><?php echo 'Indiquez un site web pour recevoir les applications '; ?></strong>
	</div>
	<div class="span-6" align="left">
		<?php echo $form->textField($model,'siteweb_entreprise',array('size'=>25,'maxlength'=>60)); ?>
	</div>
	
	
	
	

	<div class="span-18">
	<hr>
	</div>

	<div class="span-18" align="right">
		<?php 	
				echo CHtml::submitButton('Continuer',array('class'=>'button')); 
				echo '  ou  '.CHtml::link('Annuler',ContactMembres::model()->getUpdateUrl());
		 ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->