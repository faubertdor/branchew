<div class="span-15" align="left">
<div class="span-15" align="left">
<div class="span-11" align="left">
<h1>
<?php echo $model->titre;?>
</h1>
</div>
<div class="fb-like span-3" align="right" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false" data-action="recommend" data-font="arial">
</div>
<div class="span-15" align="left">
<h3>
<?php 
echo $model->entreprise;
?>
</h3>
</div>
<div class="span-10" align="left">
<h5>
<?php 
echo CHtml::encode(DomaineActivites::itemByInteger($model->domaine_activite));
?>
</h5>
</div>
</div>
</div>




<div class="span-15 doc">
<br>
<div class="span-15">
<h2>Description</h2>
</div>
<div class="span-15">
<?php 
echo $model->description;
?>
</div>
</div>



<div class="span-15 doc">
<div class="span-15">
<h2>Qualification</h2>
</div>
<div class="span-15">
<?php 
echo $model->qualification;
?>
</div>
</div>

<div class="span-15 doc" align="left">
<h2>Informations additionnelles</h2>
<div class="span-5"><strong>Type :</strong></div>
<div class="span-9">
<?php 
echo Helper::formatTypeEmploi($model->type);
?>
</div>
<div class="span-5"><strong>Zone :</strong></div>
<div class="span-9">
<?php 
echo CHtml::encode(Pays::itemByInteger($model->pays)).', '.Villes::itemByInteger(CHtml::encode($model->ville));
?> 
</div>
<div class="span-5">
<strong>Date de publication :</strong>
</div>
<div class="span-9">
<?php 
if($model->date_debut===NULL)
	echo 'Aucun<br>';
else 
	echo ' le '.CHtml::encode(Helper::dateViewFormat($model->date_debut)).' <br> ';
?>
</div>
<div class="span-5">
<strong>Date limite : </strong>	
</div>
<div class="span-9">
<?php 
if($model->date_debut===NULL)
	echo 'Aucun<br>';
else 
	echo ' le '.CHtml::encode(Helper::dateViewFormat($model->date_fin));
?>
</div>
</div>