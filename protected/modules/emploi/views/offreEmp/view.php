<?php $this->pageTitle=Yii::app()->name . ' | '.$model->titre;?>
<div class="span-23">
<div class="span-16">
<?php $this->renderPartial('_view',array('model'=>$model));?>
<div class="span-15" align="left">
<br>
<?php 

if(OffreEmp::model()->howToApply($model->id))
{
  if(!OffreEmp::model()->isAdmin($model->id))
  {
	if(AppOffres::model()->etatApplication($model->id))
		echo '<strong class="medium-green">Vous avez postulé pour cette offre d\'emploi !</strong>';
	else
		echo '<strong>'.CHtml::link('<u>Cliquez ici pour Appliquer</u>','#',array('submit'=>array('AppOffres/create'),'params'=>array('id'=>$model->id))).'</strong>';
  }
}
else 
{
	echo '<strong> Pour appliquer rendez-vous sur: '.CHtml::link('<u>'.$model->siteweb_entreprise.'</u>',$model->siteweb_entreprise).'</strong>';
}

?>
</div>
</div>
<!-- end content -->


<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>
