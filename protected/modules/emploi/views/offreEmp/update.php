<?php $this->pageTitle=Yii::app()->name . ' | Modifier une offre d\'emploi';?>
<div class="span-23">
<div class="span-18">
<h2>Modifier une offre d'emploi</h2>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
</div>