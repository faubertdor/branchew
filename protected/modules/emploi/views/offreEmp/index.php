<?php $this->pageTitle=Yii::app()->name . ' | Offres d\'emploi';?>
<div class="span-23" align="left">
<div class="span-16" align="left">	
<h2>Offres d'emploi</h2>

<?php	
		$this->widget('zii.widgets.CListView', array(
  					  'dataProvider'=>$offreEmp,
   					  'itemView'=>'_itemOffreEmp', 
					  'sortableAttributes'=>array('date')
   
					 ));
?>
	
</div>
<!-- end job section -->

<?php 
		if(!Yii::app()->user->isGuest)
		$this->renderPartial('application.modules.profil.views.default._rightMenu',array('id'=>Yii::app()->user->getID(),'suggestMembres'=>$suggestMembres,'suggestGroups'=>$suggestGroups));
?>
</div>