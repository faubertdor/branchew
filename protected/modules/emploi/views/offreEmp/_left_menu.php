<div class="span-5" align="left">
<div id="inbox">
<div class="title">
<strong>Boite d'emploi</strong>
</div>
<div class="body">
<div class="span-5">
<?php 
echo CHtml::link('Créer une offre d’emploi', OffreEmp::model()->getCreateUrl(),array('class'=>'button'));
?>
</div>
<strong><?php echo CHtml::link('Offres ouvertes',OffreEmp::model()->getMyOpenOffersUrl());?></strong>
<br>
<strong><?php echo CHtml::link('Offres ferm&eacute;es',OffreEmp::model()->getMyCloseOffersUrl())?></strong>
<br>
<strong><?php echo CHtml::link('Brouillons',OffreEmp::model()->getDraftsUrl());?></strong>
</div>

</div>
<div id="inbox">
<div class="title">
<strong>Elements envoy&eacute;s</strong>
</div>
<div class="body">
<strong><?php echo CHtml::link('En attente de vérification',OffreEmp::model()->getWaitingUrl());?></strong>
</div>
</div>
</div>