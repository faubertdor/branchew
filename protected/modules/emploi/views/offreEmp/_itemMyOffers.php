<div class="container">
<div class="span-15" align="left">

<div class="span-14">
<strong class="medium">
<?php 
if($data['draft']==='review')
	echo '<u>'.$data['titre'].'</u>';
else
	if ($data['draft']==='false' and $data['actif']==='false')
		echo CHtml::link('<u>'.$data['titre'].'</u>',OffreEmp::model()->getViewUrl($data['id']));
	else 
		if($data['actif']==='true')
			echo CHtml::link('<u>'.$data['titre'].'</u>',OffreEmp::model()->getViewUrl($data['id']));
		else
			if($data['draft']==='true')
				echo CHtml::link('<u>'.$data['titre'].'</u>',OffreEmp::model()->getUpdateUrl($data['id']));
?>
</strong>
</div>

<div class="span-5">
<strong class="medium-green">
<?php 
if($data['b_entreprises_id']!=0)
{
	$entreprise=Entreprises::model()->entrepriseInfoById($data['b_entreprises_id']);
	echo $entreprise['nom'];
										
}
else 
	echo $data['entreprise'];
?>
</strong>
<?php 
echo ' - ';
echo Pays::itemByInteger($data['pays']).', '.Villes::itemByInteger($data['ville']);
?>

</div>
<div class="span-3" align="center">
<?php 
if(($data['draft']==='false' and $data['actif']==='true') or ($data['draft']==='false' and $data['actif']==='false'))
echo CHtml::link('Voir les visites',VisiteOffres::model()->getViewUrl($data['id']));

?>
</div>

<div class="span-3" align="center">
<?php 
if(($data['draft']==='false' and $data['actif']==='true') or ($data['draft']==='false' and $data['actif']==='false'))
echo CHtml::link('Voir les applications',AppOffres::model()->getApplicationsUrl($data['id']));
?>
</div>

<div class="span-3" align="right">
<?php 

if(($data['draft']==='true' and $data['actif']==='false') or ($data['draft']==='false' and $data['actif']==='false'))
echo CHtml::link('Effacer','#',array('submit'=>array('offreEmp/delete'),
		 								   		'confirm'=>'Voulez-vous vraiment supprimer cet élément?',
		 								   		'params'=>array('id'=>$data['id'],'draft'=>$data['draft'],'actif'=>$data['actif'])
				));
				

if($data['draft']==='false' and $data['actif']==='true')
echo CHtml::link('Fermer','#',array('submit'=>array('offreEmp/close'),
												'confirm'=>'Voulez-vous vraiment fermer cet élément?',
												'params'=>array('id'=>$data['id'])
												
				));
?>
</div>


<div class="span-12">
<i>
<?php 
if($data['date_fin']===NULL)
	echo '<strong>date limite :</strong> aucun<br>';
else 
	echo '<strong>date limite :</strong> '.Helper::dateViewFormat($data['date_fin']);
?>
</i>
</div>
<hr>
</div>
</div>