<?php $this->pageTitle=Yii::app()->name . ' | Publier une offre d\'emploi';?>
<div class="span-18">
<h2>Publier une offre d'emploi</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
