<?php

class VisiteOffresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'actions'=>array('view'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('view'),
				'users'=>array('@'),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{	
		//Verifier si vous etes admin de l'entreprise ou de l'offre d'emploi
		
	if(isset($id) and is_numeric($id))
	{
		$offreArray=Yii::app()->db->createCommand()
		->select('id,b_entreprises_id,b_membres_id,titre,pays,ville')
		->from('b_offre_emp')
		->where('id='.$id)
		->queryRow();
		
		if($offreArray!=false){

			if($offreArray['b_entreprises_id']==0)
			{	
				if($offreArray['b_membres_id']==Yii::app()->user->getID())
					$this->afficherVisiteurs($id,$offreArray);
				
			}
			else 
				if(Entreprises::model()->isAdmin($offreArray['b_entreprises_id']))
				{	
					$this->afficherVisiteurs($id,$offreArray);
				}
				else 
				{
					throw new CHttpException(400,'Vous n\'êtes pas un administrateur de cette entreprise ou de cette offre d\'emploi.');
				}
		}
		else 
			throw new CHttpException(404,'La page demandée n\'existe pas');
	
		
	}
	else 
		throw new CHttpException(404,'La page demande n\'existe pas.');
		
		
		
 }
	
	//Afficher les visiteurs
	public function afficherVisiteurs($id,$offreArray){
		
			$visiteOffresArray=Yii::app()->db->createCommand()
		->select('b_visite_offres.id visiteOffresId, b_membres_id,b_offre_emp_id,date,
				  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_visite_offres')
		->where('b_offre_emp_id='.$id)
		->join('b_membres','b_membres.id=b_membres_id')
		->queryAll();
		
		$dataProvider = new CArrayDataProvider($visiteOffresArray, array(
																		'keyField'=>'membres_id',
																		'pagination'=>array(
								 									    'pageSize'=>10,
																							),
														));
		
		$this->render('view',array(
						
							'dataProvider'=>$dataProvider,
							'offreArray'=>$offreArray
						 ));
	}
}
