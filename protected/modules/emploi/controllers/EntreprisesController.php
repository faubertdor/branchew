<?php

class EntreprisesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				  'actions'=>array('create','view','update','disable','enable','viewAdmin','uploadLogo','requestCities'),
				'users'=>array('?'),
			),
			array('allow', 
				  'actions'=>array('create','view','update','disable','enable','viewAdmin','uploadLogo','requestCities'),
				  'users'=>array('@'),
			),
			
			array('deny',  // deny all users
				  'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$entreprise=$this->loadModel($id);
		
		if($entreprise->actif==='true')
		{
			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();
	
			//Suggest members
			$suggestContacts=ContactMembres::model()->getSuggestMembers();
			
			$this->render('view',array(
											'entreprise'=>$entreprise,	
											'suggestMembres'=>$suggestContacts,
											'suggestGroups'=>$suggestGroups
								  ));
		}
		else 
			throw new CHttpException(400,'Le profil demandé n’est pas actif.'); 
	}

	//Display a view with admin links for admin
	
	public function actionViewAdmin($id)
	{
		$this->render('viewAdmin',array(
			'entreprise'=>$this->loadModelAdmin($id),
		));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Entreprises;

		if(isset($_POST['Entreprises']))
		{
			$model->attributes=$_POST['Entreprises'];
			$model->description=$_POST['description'];
			
			
			//Une image par defaut pour tout nouveau membre
			$dir=Yii::getPathOfAlias('application.modules.emploi.assets.logo');
			$model->chemin_avatar=Yii::app()->assetManager->publish($dir.'/0');
			
			$this->performAjaxValidation($model);
			
			if($model->save())
			{
				
					$admin=new AdminEntreprises();
					$admin->b_entreprises_id=$model->id;
					$admin->b_membres_id=Yii::app()->user->getID();
					
					if($admin->save())
						$this->redirect(Entreprises::model()->getViewAdminUrl($model->id));
				
					
			}
				
		}
		
			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();
	
			//Suggest members
			$suggestContacts=ContactMembres::model()->getSuggestMembers();

		$this->render('create',array(
			'model'=>$model,
			'suggestMembres'=>$suggestContacts,
			'suggestGroups'=>$suggestGroups
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
	 if(isset($id) and is_numeric($id))
	 
	 	$model=$this->loadModelAdmin($id);
	 	
	 else 
	 	throw new CHttpException(404,'La page demandée n\'existe pas.');

		// Uncomment the following line if AJAX validation is needed
	    $this->performAjaxValidation($model);

		if(isset($_POST['Entreprises']))
		{
			$model->attributes=$_POST['Entreprises'];
			$model->description=$_POST['description'];
			
			if($model->save())
				$this->redirect(Entreprises::model()->getViewAdminUrl($id));
		}

			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();
	
			//Suggest members
			$suggestContacts=ContactMembres::model()->getSuggestMembers();
			
		$this->render('update',array(
			'model'=>$model,
			'suggestMembres'=>$suggestContacts,
			'suggestGroups'=>$suggestGroups
		));
	}
	
	//Disable le profil d'une entreprise
	
	public function actionDisable($id){
		
		if(isset($id) and is_numeric($id)){

			$entreprise=$this->loadModelAdmin($id);
			
			$entreprise->actif='false';
			$entreprise->save();
			
			$this->redirect(Entreprises::model()->getViewAdminUrl($id));
		}
		else 
	 		 throw new CHttpException(404,'La page demandée n\'existe pas.');
		
		
	}
	
	//Activer le profil d'une entreprise
	public function actionEnable($id){
		
		if(isset($id) and is_numeric($id)){

			$entreprise=$this->loadModelAdmin($id);
			
			$entreprise->actif='true';
			$entreprise->save();
			
			$this->redirect(Entreprises::model()->getViewAdminUrl($id));
		}
		else 
	 		 throw new CHttpException(404,'La page demandée n\'existe pas.');
		
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($id) and is_numeric($id)){
				$this->loadModelAdmin($id)->delete();
				$this->redirect(AdminEntreprises::model()->getMyEntreprisesUrl());
		}
		else 
	 			throw new CHttpException(404,'La page demandée n\'existe pas.');
			
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModelAdmin($id)
	{	
		if(Entreprises::model()->isAdmin($id)){
			$model=Entreprises::model()->findByPk($id);	
			if($model===null)
				throw new CHttpException(404,'La page demandée n\'existe pas.');
			return $model;
		}
		else
			throw new CHttpException(400,'Vous n\'êtes pas administrateur de cette entreprise.');
	}

	public function loadModel($id)
	{	
			$model=Entreprises::model()->findByPk($id);	
			
			if($model===null)
				throw new CHttpException(404,'La page demandée n\'existe pas.');
			return $model;
	}
	
	//Pour ajouter un logo
	
	public function actionUploadLogo($id){
		
		$dir=Yii::getPathOfAlias('application.modules.emploi.assets.logo');

		
		$nouveauImage=new UploadImages($id);
		
		if(isset($id) and is_numeric($id))
		{
		
			$model=$this->loadModelAdmin($id);
			
				if(isset($_POST['UploadImages']))
				{
			
					$nouveauImage->attributes=$_POST['UploadImages'];
					$this->performAjaxValidation($nouveauImage);
			
					$file=CUploadedFile::getInstance($nouveauImage,'image');
				
			
					if($nouveauImage->validate())
					{				
						$file->saveAs($dir.'/'.$model->id);						
						$model->chemin_avatar=Yii::app()->assetManager->publish($dir.'/'.$model->id);
						$model->save();
				
						$this->redirect(Entreprises::model()->getViewAdminUrl($id));
					}
			
			}
			
			
			$this->render('uploadLogo',array(
												'logo'=>$nouveauImage,
												'id'=>$id
											));
		}
	}
	
	//Pour ajouter les villes dans le formulaire
	public function actionRequestCities()
	{
		
	
		$data=Villes::model()->findAll('b_pays_id=:parent_id',
            							array(':parent_id'=>(int)$_POST['Entreprises']['pays']));

        $data=CHtml::listData($data,'id','ville');
		
		
		foreach($data as $key=>$value)
			 echo CHtml::tag('option', array('value'=>$key), CHtml::encode($value), true);
		
	}
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='entreprises-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
