<?php

class BackendController extends Controller
{
/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('index','validate','reject','login','emailJobs','massMail'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('index','validate','reject','login','emailJobs','massMail'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
  public function actionLogin()
  {
	if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
	{	
		$model=new LoginForm();
		
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			$membre=Membres::model()->membreInfoById(Yii::app()->user->getID());
			$model->username=$membre['email'];
			
			if($model->validate())
			{
				$identity=new UserIdentity($model->username,$model->password);
				$identity->authenticate();
		
				if($identity->errorCode===UserIdentity::ERROR_NONE)
				{
					$this->redirect(array('index'));
				}
				else 
				throw new CHttpException(400,'Accès refusé!');
			}
		}

		// display the login form
		$this->render('login',array('model'=>$model));
	}
	else 
		throw new CHttpException(400,'Accès refusé!');		
  }
	
	
	//Index
	public function actionIndex()
	{
		if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
		{
			$waiting=OffreEmp::model()->getSubmitted();
			$en_attentes=new CArrayDataProvider($waiting,array('pagination'=>array('pageSize'=>10)));
		
			$this->render('index',array('en_attentes'=>$en_attentes));
		}
		else
		throw new CHttpException(400,'Accès refusé!');	
	}
	
	
	
	//Activer une offre d'emploi pour etre visible sur le site
	public function actionValidate()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
			{
				if(isset($_POST['id']))
				{
					$id=$_POST['id'];
					
					$offre=OffreEmp::model()->findByPk($id);				
					$offre->date_debut=date('Y-m-d');
					$offre->date_fin=Helper::addDaysToNow(30);
					$offre->draft='false';
					$offre->actif='true';
					
					if($offre->save(false))
					{
						$membre=Membres::model()->findByPk($offre->b_membres_id);
						$membre->rh_expiration=Helper::addDaysToNow(40);
						
						if($membre->save())
						$this->redirect(array('index'));
					}
					else
						throw new CHttpException(404,'Erreur lors de la sauvegarde.');
					
				}
			}
			else 
				throw new CHttpException(400,'Accès refusé!');
		
		}
		else
			throw new CHttpException(400,'Requête Invalide');
	}

	
	
	//Retourner une offre d'emploi dans le dossier brouillons 
	public function actionReject()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
			{
				if(isset($_POST['id']))
				{
					$id=$_POST['id'];
					
					$offre=OffreEmp::model()->findByPk($id);
					$offre->draft='true';
					
					if($offre->save(false)) //to bypass validatio method
					$this->redirect(array('index'));
					else
					throw new CHttpException(404,'Erreur lors de la sauvegarde.');
				
				}
				
			}
			else 
				throw new CHttpException(400,'Accès refusé!');
		}
		else
			throw new CHttpException(400,'Requête Invalide');
	}
	
	//Mass mail
	public function actionMassmail()
	{
		if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
		{	
			$emailArray=Yii::app()->db->createCommand()
			->select('id,email')
			->from('b_membres')
			->queryAll();
			
			
			/*
					$mail= new YiiMailMessage;
					$mail->view='mail';
					$mail->setBody(array(),'text/html');
					$mail->from='branchew-communication@branchew.com';
					$mail->subject='Branchew communication';
					$mail->addTo('faubertdor@gmail.com');
					Yii::app()->mail->send($mail);
					
					$mail= new YiiMailMessage;
					$mail->view='mail';
					$mail->setBody(array(),'text/html');
					$mail->from='branchew-communication@branchew.com';
					$mail->subject='Branchew communication';
					$mail->addTo('faubertdor@hotmail.com');
					Yii::app()->mail->send($mail);
					
			*/
			
			
		
			foreach ($emailArray as $value) 
			{
				
					$mail= new YiiMailMessage;
					$mail->view='mail';
					$mail->setBody(array(),'text/html');
					$mail->from='branchew-communication@branchew.com';
					$mail->subject='Branchew communication';
					$mail->addTo($value['email']);
					Yii::app()->mail->send($mail);
			}
			
		
			$this->redirect(array('index'));
		}
	}
	
	
	//Mass mail to notify everybody about Job offers
	public function actionEmailJobs()
	{
		if(SuperMembres::model()->isSuper(Yii::app()->user->getID()))
		{
			$offreArray=OffreEmp::model()->getIndex();
			$offreEmp = new CArrayDataProvider($offreArray,array('pagination'=>array('pageSize'=>15)));
			
			$emailArray=Yii::app()->db->createCommand()
			->select('id,email')
			->from('b_membres')
			->queryAll();
		
			
		
			$offreEmpArray=$offreEmp->getData();
			
					
			foreach ($emailArray as $value) 
			{
					$mail= new YiiMailMessage;
					$mail->view='emailJobs';
					$mail->setBody(array('offreEmp'=>$offreEmpArray),'text/html');
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Offres d\'emploi sur branchew';
					$mail->addTo($value['email']);
					Yii::app()->mail->send($mail);
			}
		
			$this->redirect(array('index'));
			
		}
		else 
			throw new CHttpException(400,'Accès refusé!');
	}
	 
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}