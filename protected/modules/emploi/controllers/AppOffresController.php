<?php

class AppOffresController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('create','applications','delete','viewLetter'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('create','applications','delete','viewLetter'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	//Lettre de motivation
	public function actionViewLetter($id)
	{
		if(isset($id) and is_numeric($id))
		{
			$letterArray=AppOffres::model()->getLetter($id);
			
			if(OffreEmp::model()->isAdmin($letterArray['b_offre_emp_id']))
			{
				$this->render('viewLetter',array('letter'=>$letterArray));
			}
			else 
				throw new CHttpException(400,'Requête non valide!');		
		}
		else 
			throw new CHttpException(404,'L\'élément demandé n\'existe pas');
	}
	
	
	
	//La liste des applications pour une offre d'emploi
	public function actionApplications($id)
	{
			if(isset($_GET['id']) and is_numeric($_GET['id']))
			{
				$id=$_GET['id'];
				
				if(OffreEmp::model()->isAdmin($id))
				{
					$applicationsArray=AppOffres::model()->getApplications($id);
					$offre_emp=OffreEmp::model()->findByPk($id);
				
					$dataProvider = new CArrayDataProvider($applicationsArray, array(
																					'keyField'=>'app_id',
																					'pagination'=>array(
								 									    			'pageSize'=>10,
																									   ),
														));
				
					$this->render('applications',array('applications'=>$dataProvider,'id'=>$id,'offre_emp'=>$offre_emp));
				}
				else 
					throw new CHttpException(400,'Requête non valide!');
			}
			else 
				throw new CHttpException(404,'L\'élément demandé n\'existe pas');	
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
	  if(Yii::app()->request->isPostRequest)
	  {
	  	 $model=new AppOffres();
	  	 
		 if(isset($_POST['id'])) 		
			$model->b_offre_emp_id=$_POST['id'];
		 
	 	
	 	
	 	
		$this->performAjaxValidation($model);

		if(isset($_POST['lettre_motivation']) and isset($_POST['AppOffres']['b_offre_emp_id']))
		{

			$model->lettre_motivation=$_POST['lettre_motivation'];
			$model->b_offre_emp_id=$_POST['AppOffres']['b_offre_emp_id'];
				
		  if(!AppOffres::model()->etatApplication($model->b_offre_emp_id))
		  {
				$model->b_membres_id=Yii::app()->user->getID();
				$model->date=date('Y-m-d');
			
			if($model->save())
				$this->redirect(OffreEmp::model()->getViewUrl($model->b_offre_emp_id));
		  }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	 	
	  }
	  else 
	 		throw new CHttpException(400,'Requête invalide!');
		
	}

	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']))
			{
				$id=$_POST['id'];
				
				if(OffreEmp::model()->isAdmin($i))
				{
					
					$this->loadModel($id)->delete();
					//need to check for expiration date of the job offer
				}
				else 
					throw new CHttpException(400,'Vous n\'etes l\'administrateur de cette offre d\'emploi.');	
			}
			else
				throw new CHttpException(404,'L\'élément demandé n\'existe pas');
		}
		else
			throw new CHttpException(400,'Requête invalide!');
	}

	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=AppOffres::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='app-offres-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
