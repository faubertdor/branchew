<?php

class AdminEntreprisesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny', 
				'actions'=>array('add','delete','view','myEntreprises'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('add','delete','view','myEntreprises'),
				'users'=>array('@'),
			),

			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	//Voir tout les administrateurs de entreprise $id
	
	public function actionView($id)
	{
		
		if(isset($id) and is_numeric($id)){
			
			if(Entreprises::model()->isAdmin($id)){
				
				$adminArray=Yii::app()->db->createCommand()
				->select('b_admin_entreprises.id adminEntId,b_entreprises_id,b_membres_id, 
						  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
				->from('b_admin_entreprises')
				->where('b_entreprises_id='.$id)
				->join('b_membres','b_membres.id=b_admin_entreprises.b_membres_id')
				->queryAll();
				
				//dataProvider
				$adminData=new CArrayDataProvider($adminArray,array(
																		
																		'keyField'=>'adminEntId',
																		'pagination'=>array(
								 									 						  'pageSize'=>10,
																							),
												 ));
				//Les infos de l'entreprise $id
				
				$entreprise=Entreprises::model()->entrepriseInfoById($id);
				
				$this->render('view',array(
											'adminData'=>$adminData,
											'entreprise'=>$entreprise,
										 ));
				
			}
			else 
	 			throw new CHttpException(400,'Requête invalide. Vous n\'êtes pas administrateur de cette entreprise.');
			
		}
			
	}
	
	//Voir mes entreprises
	
	public function actionMyEntreprises(){
		
		$mesEntArray=AdminEntreprises::model()->mesEntreprises();
		
		$dataProvider = new CArrayDataProvider($mesEntArray, array(
																		'keyField'=>'entrepriseId',
																		'pagination'=>array(
								 									    					 'pageSize'=>10,
																							),
														));
		//Suggest group
		$suggestGroups=ContactGroupes::model()->getSuggest();
	
		//Suggest members
		$suggestContacts=ContactMembres::model()->getSuggestMembers();
		
		$this->render('myEntreprises',array(
						
							'myEntreprises'=>$dataProvider,
							'suggestMembres'=>$suggestContacts,
							'suggestGroups'=>$suggestGroups
						 ));
		
	}

	//Ajouter un nouvel admin pour une entreprise identifier par $id
	public function actionAdd($id)
	{
		$model=new Admin();

		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);
		
	 if(isset($id) and is_numeric($id)){
	 	
	 	if(Entreprises::model()->isAdmin($id)){
	 	
			if(isset($_POST['Admin']))
			{
				$model->attributes=$_POST['Admin'];
				
				if($model->validate()){
					$membre=Membres::model()->findByAttributes(array('email'=>$model->email));

					if(!AdminEntreprises::model()->validateAdmin($membre->id, $id))
					{
						$adminEnt=new AdminEntreprises();
						$adminEnt->b_entreprises_id=$id;
						$adminEnt->b_membres_id=$membre->id;
					}
					else 
						throw new CHttpException(400,'Ce membre est deja administrateur de cette entreprise.');
						
					
					if($adminEnt->save())
						$this->redirect(array('view','id'=>$id));
				}
			}
		}

			$this->render('add',array(
										'model'=>$model,
										'id'=>$id,
									));
	 	}
	 	else 
	 		throw new CHttpException(400,'Requête invalide.');
	 }
	 
	

	
	//Effacer un administrateur avec $id qui represente chaque row dans la table admin_entreprises
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if($_POST['id'])
			{
				$id=$_POST['id'];
				$model=$this->loadModel($id);
				$idEnt=$model->b_entreprises_id;
				
				if($model->delete())
					$this->redirect(AdminEntreprises::model()->getViewUrl($idEnt));
			
			
			}
			else
				throw new CHttpException(400,'Requête invalide.');
		}
		else
			throw new CHttpException(400,'Requête invalide.');
	}

	
	//Charger un row avec ActiveRecord pour effacer un admin
	public function loadModel($id)
	{
		$model=AdminEntreprises::model()->findByPk($id);
		
		
		if($model===null)
			throw new CHttpException(404,'La page demandée n\'existe pas.');
			
		if(Entreprises::model()->isAdmin($model->b_entreprises_id)){		 
			//si vous etes admin de cette entreprise
			$nbre=AdminEntreprises::model()->count('b_entreprises_id='.$model->b_entreprises_id);
			
			if($nbre > 1 and $model->b_membres_id!=Yii::app()->user->getID())
				return $model;
			else 
				throw new CHttpException(400,'Vous ne pouvez pas effacer le seule administrateur de cette entreprise ou vous effacez vous-même.');
		}
		else
			throw new CHttpException(400,'Requete non valide. Vous n\'êtes pas administrateur de cette entreprise.');
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='admin-entreprises-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
