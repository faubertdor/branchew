<?php

class SuivreEntreprisesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('deny',  
				'actions'=>array('follow'),
				'users'=>array('?'),
			),
			array('allow', 
				'actions'=>array('follow'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionFollow($id)
	{
		
		if(isset($id) and is_numeric($id)){
		
			$entreprise=Entreprises::model()->findByPk($id);
			
			if($entreprise===null)
				throw new CHttpException(404,'Cette entreprise n\'existe pas.');
			
			$model=new SuivreEntreprises();
			$model->b_entreprises_id=$id;
			$model->b_membres_id=Yii::app()->user->getID();
			$model->save();
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SuivreEntreprises::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demandée n\'existe pas.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='suivre-entreprises-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
