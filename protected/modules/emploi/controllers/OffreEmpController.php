<?php

class OffreEmpController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  
				'actions'=>array('view','index'),
				'users'=>array('?'),
			),
			array('allow',
				'actions'=>array('create','view','update','requestCities','index','suggestEntreprise','validateView','submit',
				'myOpenOffers','SaveToDraft','drafts','myCloseOffers','waiting','delete','close'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	//Afficher tous les offres d'emplois
	
	public function actionIndex()
	{
		 //Suggest contacts and groups to user
	  	 $suggestContacts=array();
	   	 $suggestGroups=array();
	   	 
		if(!Yii::app()->user->isGuest)
		{
		 //Suggest group
	   	 $suggestGroups=ContactGroupes::model()->getSuggest();
	   	 
	   	 //Suggest contact to user
 	  	 $suggestId=array();
 	   	 $suggestId=ContactMembres::model()->getSuggestIndex();

 	  	 	if($suggestId!=false)
 	   		{	
 											
				foreach ($suggestId as $value)
				{
					$suggestContacts[$value]=ContactMembres::model()->queryContact($value);
												
				}
			}
		}
		
		//Disabled expired offers
		$this->checkstatus();
		
		//Creating job index
		$offreEmp=OffreEmp::model()->getIndex();
		$dataProvider = new CArrayDataProvider($offreEmp,array('pagination'=>array('pageSize'=>10)));
		
		$this->render('index',array('offreEmp'=>$dataProvider,'suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups));
	}
	
	//Check for update which means desiabled expired offer
	public function checkstatus()
	{
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,date_fin,date_debut,actif,draft')
		->from('b_offre_emp')
		->where(array('and','actif=\'true\'','draft=\'false\''))
		->order('date_debut DESC')
		->queryAll();
		
		
		if($offreEmpArray!=array())
		{
			foreach ($offreEmpArray as $value) 
			{
				$now=new DateTime(date('Y-m-d'));
				//$now=$now->format('Ymd');
		
				$dateFin=new DateTime($value['date_fin']);
				//$dateFin=$dateFin->format('Ymd');
		
				if($dateFin < $now)
				{
					$model=$this->loadModel($value['id']);
					$model->actif='false';
					$model->draft='false';
					//Bug to be corrected here!
					$model->save();
					//var_dump($dateFin < $now);
				}
			 }	
		}
	}
	
	
	//envoyer l'offre d'emploi a branchew
	public function actionSubmit()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']) and is_numeric($_POST['id']))
			{
				$model=$this->loadModelAdmin($_POST['id']);
		
				$model->draft='review';
				
				if($model->save())
				{
					$expediteur=Membres::model()->getViewIdInfo(Yii::app()->user->getID());
		
					$mail= new YiiMailMessage;
					$mail->view='offreEmpSuivi';
					$mail->setBody(array(),'text/html');
					$mail->addTo($expediteur['email']);
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Offre d\'emploi sur branchew';
					Yii::app()->mail->send($mail);
		
		
					$mail= new YiiMailMessage;
					$mail->view='nouveauOffreEmp';
					$mail->setBody(array('expediteur'=>$expediteur),'text/html');
					$mail->addTo('faubertdor@gmail.com');
					$mail->from='branchew-notification@branchew.com';
					$mail->subject='Nouveau Offre d\'emploi sur branchew';
					Yii::app()->mail->send($mail);
					
					$this->redirect(OffreEmp::model()->getWaitingUrl());
				}
				else
					throw new CHttpException(404,'La page demandée n\'existe pas.');
			}
			else 
				throw new CHttpException(400,'Requête Invalide');		
		}
		else
			throw new CHttpException(400,'Requête Invalide');
	
    }
    
    //Sauvgarder comme brouillon
    public function actionSaveToDraft()
    {
    	if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']) and is_numeric($_POST['id']))
			{
				$model=$this->loadModelAdmin($_POST['id']);
				$model->actif='false';
				$model->draft='true';
				
				if($model->save())
				{
					$this->redirect(OffreEmp::model()->getDraftsUrl());
				}
				else
					throw new CHttpException(404,'Erreur lors de la sauvegarde!');
				
			}
			else
				throw new CHttpException(404,'La page demandée n\'existe pas.');
		}
		else
			throw new CHttpException(400,'Requête Invalide');
    }
    
   //Tous les offres d'emploi ouvert
   public function actionMyOpenOffers()
   {
   		$myOpenOffers=OffreEmp::model()->getMyOpenOffers();
   		$offresOuvert = new CArrayDataProvider($myOpenOffers,array('pagination'=>array('pageSize'=>10)));
		
		$this->render('myOpenOffers',array('offresOuvert'=>$offresOuvert));
   }
   
  //Tous les offres d'emploi ferme ou expire
   public function actionMyCloseOffers()
   {
   		$myCloseOffers=OffreEmp::model()->getMyCloseOffers();
   		$offresFerme = new CArrayDataProvider($myCloseOffers,array('pagination'=>array('pageSize'=>10)));
   		
		$this->render('myCloseOffers',array('offresFerme'=>$offresFerme));
   }
	
   //Tous les offres d'emploi dans le dossier brouillons
   public function actionDrafts()
   {
   		$drafts=OffreEmp::model()->getDrafts();
		$brouillons=new CArrayDataProvider($drafts,array('pagination'=>array('pageSize'=>10)));
		
		$this->render('drafts',array('brouillons'=>$brouillons));
   }
   
   
 	//Tous les offres d'emploi en attente de verification
   public function actionWaiting()
   {
   		$waiting=OffreEmp::model()->getWaiting();
		$en_attentes=new CArrayDataProvider($waiting,array('pagination'=>array('pageSize'=>10)));
		
		$this->render('waiting',array('en_attentes'=>$en_attentes));
   }
   
	//view to validate 
	public function actionValidateView($id)
	{
		if(isset($id) and is_numeric($id))
		{	
			$model=$this->loadModel($id);
			//Suggest group
			$suggestGroups=ContactGroupes::model()->getSuggest();
		
			//Suggest members
			$suggestContacts=ContactMembres::model()->getSuggestMembers();
			
			
			$model=$this->loadModelAdmin($id);
			$this->render('validateView',array('model'=>$model,'suggestMembres'=>$suggestContacts,'suggestGroups'=>$suggestGroups));
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		
	  if(isset($id) and is_numeric($id))
	  {	
		$model=$this->loadModel($id);
		
		//Suggest contacts and groups to user
		$suggestGroups=array();
		$suggestContacts=array();
		
		if(!Yii::app()->user->isGuest)
		{
			//Suggest groups
			$suggestGroups=ContactGroupes::model()->getSuggest();
		
			//Suggest members
			$suggestContacts=ContactMembres::model()->getSuggestMembers();
		}
		
		if(!OffreEmp::model()->isAdmin($model->id))
			$this->visiteOffreEmp($id);
	
						
				$this->render('view',array(
												'model'=>$model,
												'suggestMembres'=>$suggestContacts,	
												'suggestGroups'=>$suggestGroups
			 					 		  ));
		
	  }
	  else
	  	throw new CHttpException(404,'L\'element demande n\'exite pas.');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
		$model=new OffreEmp;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['OffreEmp']))
		{
			$model->attributes=$_POST['OffreEmp'];
			
			$model->description=$_POST['description'];
			$model->qualification=$_POST['qualification'];
			//$model->description_entreprise=$_POST['description_entreprise'];
			$model->b_membres_id=Yii::app()->user->getID();
																						
			$model->actif='false';
			$model->draft='true';
		
			if(isset($_POST['b_entreprises_id'])) 
				$model->b_entreprises_id=$_POST['b_entreprises_id'];
			
					
			if($model->save())
				$this->redirect(array('validateView','id'=>$model->id));
					
								
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		if(isset($id) and is_numeric($id))
			$model=$this->loadModelAdmin($id);
		else 
			throw new CHttpException(404,'La page demande n\'existe pas.');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['OffreEmp']))
		{
			//On ne peut pas utiliser l'affectation massif pour ne pas modifier la date de fin de publication
			$model->titre=$_POST['OffreEmp']['titre'];
			$model->type=$_POST['OffreEmp']['type'];
			$model->domaine_activite=$_POST['OffreEmp']['domaine_activite'];
			$model->description=$_POST['description'];
			$model->qualification=$_POST['qualification'];
			//$model->description_entreprise=$_POST['description_entreprise'];
			$model->pays=$_POST['OffreEmp']['pays'];
			$model->ville=$_POST['OffreEmp']['ville'];
			$model->siteweb_entreprise=$_POST['OffreEmp']['siteweb_entreprise'];
			$model->email=$_POST['OffreEmp']['email'];
			$model->application=$_POST['OffreEmp']['application'];
			
			$model->entreprise=$_POST['OffreEmp']['entreprise'];
			$model->b_entreprises_id=$_POST['OffreEmp']['b_entreprises_id'];
			
				
			$model->actif='false';
			$model->draft='true';
			
			//if(isset($_POST['b_entreprises_id'])) 
			//	$model->b_entreprises_id=$_POST['b_entreprises_id'];
		
			if($model->save())		
				$this->redirect(array('validateView','id'=>$model->id));
		}
			

		$this->render('update',array(
										'model'=>$model,
									));
	}

	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=OffreEmp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demande n\'existe pas.');
		return $model;
	}
	
	//Effacer une offre d'emploi
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']) and isset($_POST['actif']) and $_POST['draft'])
			{
				if($_POST['draft']==='true' and $_POST['actif']==='false') 
				{
					$emp_id=$_POST['id'];
					$model=$this->loadModelAdmin($emp_id);
					$model->delete();
					$this->redirect(OffreEmp::model()->getDraftsUrl());
				}
				else
				{
					if($_POST['draft']==='false' and $_POST['actif']==='false')
					{
						$emp_id=$_POST['id'];
						
						//Effacer toutes les visites
						$visitesId=VisiteOffres::model()->getVisitesId($emp_id);
						
						if($visitesId!=array())
						{
							foreach ($visitesId as $value) 
							{
								$visite=VisiteOffres::model()->findByPk($value['visite_id']);
								$visite->delete();
							}
						}
						
						//Effacer toutes les applications
						$applicationsId=AppOffres::model()->getApplicationsId($emp_id);
						
						if($applicationsId!=array())
						{
							foreach ($applicationsId as $value) 
							{
								$application=AppOffres::model()->findByPk($value['app_id']);
								$application->delete();
							}
						}
						
						
						//Effacer l'offre en question
						$model=$this->loadModelAdmin($emp_id);
						$model->delete();
						
						//Redirection
						$this->redirect(OffreEmp::model()->getMyCloseOffersUrl());
					}
				}
			}
		}
	}
	
	//Fermer une offre d'emploi
	public function actionClose()
	{
		if(Yii::app()->request->isPostRequest)
		{
			if(isset($_POST['id']))
			{
				$model=$this->loadModelAdmin($_POST['id']);
				$model->actif='false';
				$model->draft='false';
				
				if($model->save())
				{
					$this->redirect(OffreEmp::model()->getMyOpenOffersUrl());
				}
				
			}
		}
	}
    //Load model admin
    public function loadModelAdmin($id)
	{
		$model=OffreEmp::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La page demande n\'existe pas.');
			
			if($model->b_entreprises_id!=0){
				
				if(Entreprises::model()->isAdmin($model->b_entreprises_id))
				{
					return $model;
				}
				else
					throw new CHttpException(400,'Vous n\'etes pas un administrateur de cette entreprise.');
			}
			else
			{
				if($model->b_membres_id==Yii::app()->user->getID())
				{
					return $model;
				}
				else
					throw new CHttpException(400,'Vous n\'etes l\'administrateur de cette offre d\'emploi.');
				
			}
		
	}
	
	//Visite d'une offre d'empoi
	public function visiteOffreEmp($id){
		
		$userId=Yii::app()->user->getID();
		
		if(isset($userId))
		{
			$visiteArray=Yii::app()->db->createCommand()
			->select('b_membres_id,b_offre_emp_id')
			->from('b_visite_offres')
			->where(array('and','b_membres_id='.$userId,'b_offre_emp_id='.$id))
			->queryRow();
		
			if($visiteArray==false)
			{
				$visite=new VisiteOffres();
				$visite->b_membres_id=$userId;
				$visite->b_offre_emp_id=$id;
				$visite->date=date('Y-m-d');
	 			$visite->save();
			}
		}
	}
	
	//Pour ajouter les villes dans le formulaire
	public function actionRequestCities()
	{
		
	
		$data=Villes::model()->findAll('b_pays_id=:parent_id',
            							array(':parent_id'=>(int)$_POST['OffreEmp']['pays']));

        $data=CHtml::listData($data,'id','ville');
		
		
		foreach($data as $key=>$value)
			 echo CHtml::tag('option', array('value'=>$key), CHtml::encode($value), true);
		
	}
	
	
	
		//Auto complete when create messages	
		public function actionSuggestEntreprise()
		{
		
			$res=array();
			$term = Yii::app()->getRequest()->getParam('q', false);
			 
   	  		 if (isset($term))
      		 {
        	 	$sql = 'SELECT id, nom FROM b_entreprises where (LCASE(nom) LIKE :term)';
        	 	$cmd = Yii::app()->db->createCommand($sql);
        	 	$cmd->bindValue(":term","%".strtolower($term)."%", PDO::PARAM_STR);
        	 	$res = $cmd->queryAll();
         		$returnVal='';
         	
         		foreach($res as $value){
         		 
         			$returnVal .=$value['nom'].'|'.$value['id']."\n";
          		
         		}
         	
         		echo $returnVal;
     	 	}		
		}
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offre-emp-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
