<?php
class Admin extends CFormModel{
	
	public $email;
	
	
	public function rules(){
		
		return array(
		
				array('email','required'),
				array('email','email'),
				array('email','validateEmail')
		);
	}
	
	//Validator uniqueEmail
	public function validateEmail($attribute,$params){
		
		$membre=Membres::model()->findByAttributes(array('email'=>$this->email));
		
		if($membre==null){
			$this->addError('validateEmail','Cette adresse email n\'est pas valide.');
		}
		else{ 	
			 if(!ContactMembres::model()->isContact($membre->id))
				$this->addError('validateEmail','Ce membre n\'est pas dans votre reseau');
		}
		
	}
}