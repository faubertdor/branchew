<?php

/**
 * This is the model class for table "{{suivre_entreprises}}".
 *
 * The followings are the available columns in table '{{suivre_entreprises}}':
 * @property string $id
 * @property string $b_entreprises_id
 * @property string $b_membres_id
 *
 * The followings are the available model relations:
 * @property Membres $bMembres
 * @property Entreprises $bEntreprises
 */
class SuivreEntreprises extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return SuivreEntreprises the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{suivre_entreprises}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{

	}
	
	//Follow url
	
	public function getFollowUrl($id){
		
		return Yii::app()->createUrl('emploi/suivreEntreprises/follow',array('id'=>$id));
	}
	
	
	//isFollowing true or false
	public function isFollowing($id){
		
		$followArray=Yii::app()->db->createCommand()
		->select('b_membres_id,b_entreprises_id')
		->from('b_suivre_entreprises')
		->where(array('and','b_membres_id='.Yii::app()->user->getID(),'b_entreprises_id='.$id))
		->queryRow();
		
		if($followArray===false)
		
			return false;
		else 	
			return true;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}