<?php

/**
 * This is the model class for table "{{visite_offres}}".
 *
 * The followings are the available columns in table '{{visite_offres}}':
 * @property string $id
 * @property string $date
 * @property string $b_membres_id
 * @property string $b_offre_emp_id
 */
class VisiteOffres extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return VisiteOffres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{visite_offres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);
		$criteria->compare('b_offre_emp_id',$this->b_offre_emp_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Retourne la liste des visites pour un poste
	public function getVisitesId($id)
	{
		$visitesId=Yii::app()->db->createCommand()
			->select('b_visite_offres.id visite_id,b_offre_emp_id')
			->from('b_visite_offres')
			->where('b_offre_emp_id='.$id)
			->queryAll();
			
		return $visitesId;
	}
	
	
	//View url 
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('emploi/visiteOffres/view',array('id'=>$id));
	}
	
}