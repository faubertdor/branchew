<?php

/**
 * This is the model class for table "{{entreprises}}".
 *
 * The followings are the available columns in table '{{entreprises}}':
 * @property string $id
 * @property string $nom
 * @property string $chemin_avatar
 * @property string $type_entreprise
 * @property integer $domaine_activite
 * @property string $nombre_employe
 * @property string $pays
 * @property string $ville
 * @property string $adresse
 * @property string $telephone1
 * @property string $telephone2
 * @property string $date_creation
 * @property string $actif
 * @property string $profil_type
 * @property string $siteweb
 * @property string $email
 * @property string $description
 * @property string $b_membres_id
 *
 * The followings are the available model relations:
 * @property AdminEntreprises[] $adminEntreprises
 * @property AppelOffres[] $appelOffres
 * @property Membres $bMembres
 * @property OffreEmp[] $offreEmps
 * @property SuivreEntreprises[] $suivreEntreprises
 */
class Entreprises extends CActiveRecord
{
	private $_domaine;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Entreprises the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{entreprises}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nom, type_entreprise,nombre_employe, pays, ville, adresse, telephone1,email, description', 'required'),
			array('pays, ville', 'numerical', 'integerOnly'=>true),
			array('nom', 'length', 'max'=>100),
			array('nom','unique'),
			array('type_entreprise, adresse, siteweb, email', 'length', 'max'=>60),
			array('nombre_employe, telephone1', 'length', 'max'=>15),
			array('email','email'),
			array('siteweb','url'),
			array('email,siteweb','unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nom,type_entreprise, nombre_employe, pays, ville, adresse, telephone1,date_creation, siteweb, email, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
			'nom' => 'Nom de l\' entreprise',
			'type_entreprise' => 'Secteur',
			'nombre_employe' => 'Nombre d\' employé',
			'pays' => 'Pays',
			'ville' => 'Ville',
			'adresse' => 'Adresse',
			'telephone1' => 'Telephone',
			'siteweb' => 'Site web',
			'email' => 'Email',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;


		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('type_entreprise',$this->type_entreprise,true);
		$criteria->compare('nombre_employe',$this->nombre_employe,true);
		$criteria->compare('pays',$this->pays,true);
		$criteria->compare('ville',$this->ville,true);
		$criteria->compare('adresse',$this->adresse,true);
		$criteria->compare('telephone1',$this->telephone1,true);
		$criteria->compare('siteweb',$this->siteweb,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('description',$this->description,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Create Url
	
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('emploi/entreprises/create',array());
	}
	
	//Update Url
	
	public function getUpdate($id){
		
		return Yii::app()->createUrl('emploi/entreprises/update',array('id'=>$id));
	}

	//View Url
	
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('emploi/entreprises/view',array('id'=>$id));
	}
	
	//ViewAdmin Url
	
	public function getViewAdminUrl($id){
		
		return Yii::app()->createUrl('emploi/entreprises/viewAdmin',array('id'=>$id));
	}
	
	//Disable Url
	
	public function getDisableUrl($id){
		
		return Yii::app()->createUrl('emploi/entreprises/disable',array('id'=>$id));
	}
	
	//Enable Url
	
	public function getEnableUrl($id){
		
		return Yii::app()->createUrl('emploi/entreprises/enable',array('id'=>$id));
	}
	//Upload logo url
	
	public function getUploadLogoUrl($id){
		
		return Yii::app()->createUrl('emploi/entreprises/uploadLogo',array('id'=>$id));
	}
	//Before save
	
	protected function beforeSave(){
		
		
			if($this->isNewRecord){
				
				
				$this->date_creation=date('Y-m-d');
				$this->profil_type='basic';
				$this->actif='true';
			}
			
		return parent::beforeSave();
		
	}
	
	//Regarde si vous etes un admin de l'entreprise indentifie par $id
	
	public function isAdmin($id){
		
		$userId=Yii::app()->user->getID();
		
		if(isset($userId))
		{
			$adminArray=Yii::app()->db->createCommand()
				->select('*')
				->from('b_admin_entreprises')
				->where(array('and','b_entreprises_id='.$id,'b_membres_id='.$userId))
				->queryRow();
		
			if($adminArray==false)
				return false;
			else 
				return true;
		}

		return false;
			
	}
	
	//Validation pour l'url et l'adresse email de l'entreprise
	
	public function validateUrlEmail($attribute,$params){
		
	if($this->email!=NULL and $this->siteweb!=NULL)
	{
		//pour le domaine du site web
		$lenghUrl=strlen($this->siteweb)-11;
		$domaineUrl=substr($this->siteweb,11,$lenghUrl);
		
		//Pour le domaine de l'email
		$i=0;
		while($this->email[$i]!='@'){
					
			$i++;
		}
		
		$i++;
		$lenghEmail=strlen($this->email)-$i;
		$domaineEmail=substr($this->email,$i,$lenghEmail);
		
		if($domaineEmail!=$domaineUrl)
			$this->addError('validateUrlEmaill','Le domaine indiquer pour l\'email est different du site web!');
		else 
		{
			$domaineArray=Yii::app()->db->createCommand()
			->select('*')
			->from('b_domaine_invalides')
			->where('domaine=\''.$domaineUrl.'\'') //ou $domaineEmail puisque $domaineEmail===$domaineUrl
			->queryRow();
			
			if($domaineArray!=false)
			 $this->addError('validateUrlEmaill','Le domaine indiquer n\'est pas valide!');
			
		}
	}
	else 
		$this->addError('validateUrlEmaill','Le domaine indiquer n\'est pas valide!');
	
}
	
	
	//Afficher une vue info pour une entreprise
	
	public function getViewInfo($id,$avatar,$nom,$type_entreprise){
		
		return '<div class="span-8" align="left">'.
			   '<div class="span-8" align="left">'.
			   '<div class="span-2" align="right">'.
				CHtml::link(CHtml::image($avatar,$nom,array(
																			'width'=>65,
																			'height'=>55,
														   )),Entreprises::model()->getViewUrl($id)).
																		
				'</div>'.
				'<div class="span-4" align="left">'.
				'<div class="span-4" align="left"><b>'.
				 CHtml::link($nom,Entreprises::model()->getViewUrl($id)).
				'</b></div><div class="span-3" align="left">'.
				 CHtml::encode(Helper::secteurViewFormat($type_entreprise)).
				'</div>'.
				'</div>'.
				'</div>'.
				'</div>';
		
	}
	
	
	//Afficher une vue info pour une entreprise qui n'a pas son profil sur le site
	public function getViewFormat($nom_entreprise)
	{
		$dir=Yii::getPathOfAlias('application.modules.emploi.assets.logo');
		$dir=Yii::app()->assetManager->publish($dir.'/0');
		
		return '<div class="span-7" align="left">'.
			   '<div class="span-7" align="left">'.
			   '<div class="span-2" align="left">'.
				CHtml::image($dir,$nom_entreprise,array(
																			'width'=>65,
																			'height'=>55,
														   )).
																		
				'</div>'.
				'<div class="span-4" align="left">'.
				'<div class="span-4" align="left"><br><b>'.
				 CHtml::encode($nom_entreprise).
				'</b></div><div class="span-3" align="left">'.
				
				'</div>'.
				'</div>'.
				'</div>'.
				'</div>';
		
	}
	//Info by Id
	public function entrepriseInfoById($id){
		
		$infoArray=Yii::app()->db->createCommand()
		->select('b_entreprises.id entrepriseId, chemin_avatar,nom,type_entreprise')
		->from('b_entreprises')
		->where('b_entreprises.id='.$id)
		->queryRow();
		
		return $infoArray;
		
	}
	
 
	//Une liste d'entreprise pour dropDownlist
	
	public function idAndName(){
		
		$EntArray=Yii::app()->db->createCommand()
		->select('id,nom')
		->from('b_entreprises')
		->queryAll();
		
		$array=array();
		$array[0]='Aucune';
		
		if($EntArray!=array())
		
			foreach ($EntArray as $value){
			
				$array[$value['id']]=$value['nom'];
			}
		
		return $array;
	}
	
	
}