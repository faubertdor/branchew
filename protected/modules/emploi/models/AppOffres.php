<?php

/**
 * This is the model class for table "{{app_offres}}".
 *
 * The followings are the available columns in table '{{app_offres}}':
 * @property string $id
 * @property string $b_membres_id
 * @property string $b_offre_emp_id
 * @property string $date
 * @property string $lettre_chemin
 */
class AppOffres extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return AppOffres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{app_offres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('lettre_motivation','required'),
			array('b_membres_id,b_offre_emp_id,date','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, b_membres_id, b_offre_emp_id, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		
		'lettre_motivation'=>'Rédigez votre lettre de motivation',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);
		$criteria->compare('b_offre_emp_id',$this->b_offre_emp_id,true);
		$criteria->compare('date',$this->date,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Si vous avez deja appliquer pour cette offre
	public function etatApplication($id){
		
		$userId=Yii::app()->user->getID();
		
		if(isset($userId))
		{
	
			$appArray=Yii::app()->db->createCommand()
			->select('b_membres_id,b_offre_emp_id')
			->from('b_app_offres')
			->where(array('and','b_membres_id='.$userId,'b_offre_emp_id='.$id))
			->queryRow();
		
			if($appArray===false)
			return false;
			else
			return true;
		}
		
		return false;
	}
	
	//Retourner la lettre de motivation
	public function getLetter($id)
	{
		$appArray=Yii::app()->db->createCommand()
			->select('b_app_offres.id app_id,lettre_motivation,b_offre_emp_id,
					  b_offre_emp.id offre_emp_id,titre,pays,ville')
			->from('b_app_offres')
			->where('b_app_offres.id='.$id)
			->join('b_offre_emp','b_app_offres.b_offre_emp_id=b_offre_emp.id')
			->queryRow();
			
		return $appArray;
	}
	
	//Retourne toutes les applications pour un poste
	public function getApplications($id)
	{
		$applicationsArray=array();
		$applicationsArray=Yii::app()->db->createCommand()
		->select('b_app_offres.id app_id,b_membres_id,b_offre_emp_id,date,
		 		  b_membres.id membres_id,chemin_avatar,nom,prenom,domaine_activite')
		->from('b_app_offres')
		->where('b_app_offres.b_offre_emp_id='.$id)
		->join('b_membres','b_membres.id=b_app_offres.b_membres_id')
		->queryAll();
		
		return $applicationsArray;
	}
	
	
	//Retourne la liste des applications ID pour un poste
	public function getApplicationsId($id)
	{
		$applicationsId=Yii::app()->db->createCommand()
			->select('b_app_offres.id app_id, b_offre_emp_id')
			->from('b_app_offres')
			->where('b_offre_emp_id='.$id)
			->queryAll();
			
		return $applicationsId;
	}
	
	
	
	//viewLetter Url
	public function getViewLetterUrl($id)
	{
		return Yii::app()->createUrl('emploi/appOffres/viewLetter',array('id'=>$id));
	}
	
	//Create Url
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('emploi/appOffres/create',array());
	}
	
	//Create Url
	public function getApplicationsUrl($id){
		
		return Yii::app()->createUrl('emploi/appOffres/applications',array('id'=>$id));
	}
}