<?php

/**
 * This is the model class for table "{{admin_entreprises}}".
 *
 * The followings are the available columns in table '{{admin_entreprises}}':
 * @property string $id
 * @property string $b_entreprises_id
 * @property string $b_membres_id
 *
 * The followings are the available model relations:
 * @property Membres $bMembres
 * @property Entreprises $bEntreprises
 */
class AdminEntreprises extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return AdminEntreprises the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{admin_entreprises}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		
	}
	
	//View Url
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('emploi/adminEntreprises/view',array('id'=>$id));
	}
	
	//Add Url
	
	public function getAddUrl($id){
		
		return Yii::app()->createUrl('emploi/adminEntreprises/add',array('id'=>$id));
	
	}
	
	
	//Mes entreprises Url
	
	public function getMyEntreprisesUrl(){
		
		return Yii::app()->createUrl('emploi/adminEntreprises/myEntreprises',array());
	}
	
	//Retourne les entreprises d'un utilisateur par son Id
	public function getEntreprises($id){
		
		$entArray=Yii::app()->db->createCommand()
		->select('b_admin_entreprises.id adminId,b_entreprises_id,b_membres_id,
				  b_entreprises.id entrepriseId, chemin_avatar,nom,type_entreprise,actif')
		->from('b_admin_entreprises')
		->where('b_membres_id='.$id)
		->join('b_entreprises','b_entreprises.id=b_entreprises_id')
		->queryAll();
		
		
		return $entArray;	
	}
	
	//Mes entreprises retourne un array
	public function mesEntreprises(){
		
		$entArray=Yii::app()->db->createCommand()
		->select('b_admin_entreprises.id adminId,b_entreprises_id,b_membres_id,
				  b_entreprises.id entrepriseId, chemin_avatar,nom,type_entreprise,actif')
		->from('b_admin_entreprises')
		->where('b_membres_id='.Yii::app()->user->getID())
		->join('b_entreprises','b_entreprises.id=b_entreprises_id')
		->queryAll();
		
		
		return $entArray;	
	}
	
	//valider un nouvel admin
	
	public function validateAdmin($membreId,$entId){
		
		
		$admin=Yii::app()->db->createCommand()
		->select('b_membres_id,b_entreprises_id')
		->from('b_admin_entreprises')
		->where(array('and','b_membres_id='.$membreId,'b_entreprises_id='.$entId))
		->queryRow();
		
		
			return $admin;
		}
}