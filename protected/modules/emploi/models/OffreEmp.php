<?php

/**
 * This is the model class for table "{{offre_emp}}".
 *
 * The followings are the available columns in table '{{offre_emp}}':
 * @property string $id
 * @property string $titre
 * @property string $type
 * @property integer $domaine_activite
 * @property string $description
 * @property string $qualification
 * @property string $description_entreprise
 * @property integer $pays
 * @property integer $ville
 * @property string $siteweb_entreprise
 * @property string $date_debut
 * @property string $date_fin
 * @property string $siteweb_application
 * @property string $b_membres_id
 * @property string $b_entreprises_id
 */
class OffreEmp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return OffreEmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{offre_emp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titre, type, domaine_activite, description, qualification, pays, ville,entreprise,application', 'required'),
			array('domaine_activite, pays, ville', 'numerical', 'integerOnly'=>true),
			array('titre, siteweb_entreprise', 'length', 'max'=>100),
			array('siteweb_entreprise','url'),
			array('siteweb_entreprise','appSiteweb'),
			array('email','email'),
			array('email','appEmail'),
			array('type', 'length', 'max'=>13),
			array('b_entreprises_id','validateAdmin'),
			array('description_entreprise,b_entreprises_id,actif,draft,date_debut,date_fin', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('titre, type, domaine_activite, description, qualification, description_entreprise, pays, ville,date_debut, date_fin', 'safe', 'on'=>'search'),
		);
	}
	
	
	//Validate b_entreprises_id	
	public function validateAdmin($attribute,$params){
		
		if($this->b_entreprises_id!=null and $this->b_entreprises_id!=0)
			if(!Entreprises::model()->isAdmin($this->b_entreprises_id))
				$this->addError('validateAdmin', 'Vous n\' êtes pas administrateur de cette entreprise.');
	}
	
	//Validate email pour application
	public function appEmail($attribute,$params){
	
		if($this->application=='email')
			if($this->email==null)
				$this->addError('appEmail', 'Email ne peut être  vide.');
		
	}

	//Validate siteweb pour application
	public function appSiteweb($attribute,$params){
	
		if($this->application=='siteweb')
			if($this->siteweb_entreprise==null)
				$this->addError('appSiteweb', 'Site web pour application ne peut être  vide.');
		
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	//Retourner tous les offres d'emploi actifs
	public function getIndex()
	{	
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin date,date_debut,actif,draft,entreprise,b_entreprises_id,newsId')
		->from('b_offre_emp')
		->where(array('and','actif=\'true\'','draft=\'false\''))
		->order('date_debut DESC')
		->queryAll();
	 
	 	return $offreEmpArray;
	}
	
	//Tous les offres d'emploi actifs 
	public function getMyOpenOffers()
	{	
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin,actif,entreprise,b_entreprises_id,b_membres_id,draft')
		->from('b_offre_emp')
		->where(array('and','actif=\'true\'','b_membres_id='.Yii::app()->user->getID()))
		->order('date_debut DESC')
		->queryAll();
		
		return $offreEmpArray;
	}
	
	//Tous les offres d'emploi ferme ou expire 
	public function getMyCloseOffers()
	{	
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin,actif,entreprise,b_entreprises_id,b_membres_id,draft')
		->from('b_offre_emp')
		->where(array('and','actif=\'false\'','draft=\'false\'','b_membres_id='.Yii::app()->user->getID()))
		->order('date_debut DESC')
		->queryAll();
		
		return $offreEmpArray;
	}
	
	//Les offres d'emploi dans  le dossier brouillons
	public function getDrafts()
	{
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin,actif,entreprise,b_entreprises_id,b_membres_id,draft')
		->from('b_offre_emp')
		->where(array('and','draft=\'true\'','b_membres_id='.Yii::app()->user->getID()))
		->order('date_debut DESC')
		->queryAll();
		
		return $offreEmpArray;
	}
	
	//Les offres d'emploi en attente de verification
	public function getWaiting()
	{
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin,actif,entreprise,b_entreprises_id,b_membres_id,draft')
		->from('b_offre_emp')
		->where(array('and','draft=\'review\'','b_membres_id='.Yii::app()->user->getID()))
		->order('date_debut DESC')
		->queryAll();
		
		return $offreEmpArray;
	}
	
	//Tout les offres d'emploi envoyees a branchew!
	public function getSubmitted()
	{
		$offreEmpArray=array();
		$offreEmpArray=Yii::app()->db->createCommand()
		->select('id,titre,pays,domaine_activite,ville,date_fin,date_debut,actif,entreprise,b_entreprises_id,b_membres_id,draft')
		->from('b_offre_emp')
		->where('draft=\'review\'')
		->order('date_debut DESC')
		->queryAll();
		
		return $offreEmpArray;
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'titre' => 'Titre du poste',
			'type' => 'Type',
			'domaine_activite' => 'Domaine',
			'description' => 'Description',
			'qualification' => 'Qualification',
			'description_entreprise' => 'Description de l\'entreprise',
			'pays' => 'Pays',
			'ville' => 'Ville',
			'siteweb_entreprise' => 'Site web pour application',
			'email'=>'Email pour recevoir les dossiers',
			'date_debut' => 'Date de debut',
			'date_fin' => 'Date de fin',
			'entreprise'=>'Entreprise',
			'b_entreprises_id'=>'Entreprise',
			'application'=>'Méthode pour application'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('titre',$this->titre,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('domaine_activite',$this->domaine_activite);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('description_entreprise',$this->description_entreprise,true);
		$criteria->compare('pays',$this->pays);
		$criteria->compare('ville',$this->ville);
		$criteria->compare('siteweb_entreprise',$this->siteweb_entreprise,true);
		$criteria->compare('date_debut',$this->date_debut,true);
		$criteria->compare('date_fin',$this->date_fin,true);
		$criteria->compare('siteweb_application',$this->siteweb_application,true);
		$criteria->compare('b_membres_id',$this->b_membres_id,true);
		$criteria->compare('b_entreprises_id',$this->b_entreprises_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	//Authentifier administrateur pour chaque offre d'emploi
	public function isAdmin($id)
	{
		$offreArray=Yii::app()->db->CreateCommand()
		->select('id,b_membres_id,b_entreprises_id')
		->from('b_offre_emp')
		->where('id='.$id)
		->queryRow();
		
		if($offreArray['b_entreprises_id']==0)
		{
			if($offreArray['b_membres_id']==Yii::app()->user->getID())
				return true;
		}
		else
		{
			if(Entreprises::model()->isAdmin($offreArray['b_entreprises_id']))
				return true;
		}	
	}
	
	//View url
	public function getViewUrl($id){
		
		return Yii::app()->createUrl('emploi/offreEmp/view',array('id'=>$id));
		
	}
	
	//Create url
	
	public function getCreateUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/create',array());
	}
	
	//Index url
	public function getIndexUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/index',array());
	}
	
	//My waiting offers url
	public function getWaitingUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/waiting',array());
	}
	
	//My open offers url
	public function getMyOpenOffersUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/myOpenOffers',array());
	}
	
	//My closed offers url
	public function getMyCloseOffersUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/myCloseOffers',array());
	}
	
	//comment appliquer, Si l'entreprise veut utiliser notre systeme 
	public function howToApply($id)
	{
			$application=Yii::app()->db->createCommand()
			->select('id,application')
			->from('b_offre_emp')
			->where('id='.$id)
			->queryRow();
			
			if($application['application']=='email')
			return true;
			else 
			return false;
	}
	
	
	//drafts url
	public function getDraftsUrl(){
		
		return Yii::app()->createUrl('emploi/offreEmp/drafts',array());
	}
	//Update url
	public function getUpdateUrl($id){
		
		return Yii::app()->createUrl('emploi/offreEmp/update',array('id'=>$id));
	}
	
	//Afficher une vue info pour une offre d'emploi
	public function getViewInfo($id,$titre,$domaine,$dateFin){
		
		return  '<div class="row" align="left">'.
				'<div class="column" align="left"><b>'.
				 CHtml::link($titre,OffreEmp::model()->getViewUrl($id)).
				'</b></div>'.
				'<div class="column" align="left">'.
				 DomaineActivites::itemByInteger($domaine).' '.$dateFin.
				'</div>'.
				 
				'</div>';
		
	}
}